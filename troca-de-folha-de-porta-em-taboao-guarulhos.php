<?php
$title       = "Troca de folha de porta em Taboão - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As portas e janelas de correr são as principais peças que usam folhas, podendo ter 2 folhas, 3 folhas, 4 folhas e até 6 folhas. A porta de 1 folha consiste no modelo mais comum do mercado, dando referência a porta comum de abrir e fechar. Já a porta de 2 folhas ou mais folhas, pode ser uma porta fixa, uma porta correndo ou duas portas correndo sobre uma outra porta fixa. Troca de folha de porta em Taboão - Guarulhos é com a Interporta.</p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de Fabricante de Porta, a Interporta oferece a confiança e a qualidade que você procura quando falamos de Manutenção de porta camarão, Porta de madeira com espelho, Porta de madeira com aplique de veneziana superior ou inferior, Porta de embutir sistema duplo linear ou 45 graus e Porta de madeira laqueada pivotante. Ainda, com o mais acessível custo x benefício para quem busca Troca de folha de porta em Taboão - Guarulhos, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>