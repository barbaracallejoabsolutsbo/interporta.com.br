<?php
$title       = "Manutenção de porta embutida no Ipiranga";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para realizar a Manutenção de porta embutida no Ipiranga os profissionais da nossa empresa utilizam um sistema que permite a retirada da porta sem que seja necessário quebrar a parede novamente. Além disso, nossa empresa consegue tirar a roldana do amortecimento e até mesmo do trilho sem danificar a porta, podendo efetuar reparos e manutenções preventivas. Entre em contato com a gente e saiba mais.</p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de Fabricante de Porta, a Interporta oferece a confiança e a qualidade que você procura quando falamos de Manutenção de porta camarão, Porta de madeira com aplicação de vidro, Porta de madeira laqueada embutida, Loja de fabrica de porta embutida e Porta de madeira de correr. Ainda, com o mais acessível custo x benefício para quem busca Manutenção de porta embutida no Ipiranga, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>