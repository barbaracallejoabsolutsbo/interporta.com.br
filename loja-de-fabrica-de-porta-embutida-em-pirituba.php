<?php
$title       = "Loja de fabrica de porta embutida em Pirituba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A porta embutida de correr é uma das melhores soluções para melhorar a circulação do ambiente e para aproveitar ao máximo o espaço. Além disso, a porta embutida de correr agrega mais design e estilo para os ambientes. A Interporta é uma loja de fábrica de porta embutida. </p>
<p>Com a Interporta proporcionando de forma excelente Porta de correr trilho simples, Porta de madeira embutida na lateral da parede, Loja de fabrica de porta laqueada, Manutenção de porta pivotante e Manutenção de porta de giro conseguindo manter a alta qualidade e credibilidade no ramo de Fabricante de Porta, assim, consequentemente, proporcionando o que se tem de melhor em resultados para você. Possibilitando diversas escolhas para os melhores resultados, a nossa empresa torna-se referência com Loja de fabrica de porta embutida em Pirituba.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>