<?php
$title       = "Porta de madeira de parede de drywall em São Caetano do Sul";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta trabalha com Porta de madeira de parede de drywall em São Caetano do Sul com entregas para todo o Brasil. Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente.  Todos nossos produtos e serviços são feitos com os melhores materiais do mercado e com profissionais experientes e capacitados para realizar todos os serviços da melhor forma possível.</p>
<p>A Interporta, como uma empresa em constante desenvolvimento quando se trata do mercado de Fabricante de Porta visa trazer o melhor resultado em Porta de madeira de parede de drywall em São Caetano do Sul para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Manutenção de porta camarão, Manutenção de porta com roldana, Loja de fabrica de porta laqueada, Porta de madeira com friso e Porta de madeira personalizada para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>