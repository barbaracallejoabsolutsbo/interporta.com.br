<?php
$title       = "Porta de madeira personalizada em Santo Amaro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta esta situada na cidade de São Paulo, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil. Operamos e fornecemos Porta de madeira personalizada em Santo Amaro para todos os estados do nosso país. Enquanto uma porta tradicional pode ocupar quase um metro quadrado, uma porta personalizada pode ser adaptada a espaços menores otimizando o lugar.</p>
<p>Com a Interporta proporcionando o que se tem de melhor e mais moderno no segmento de Fabricante de Porta consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Porta de madeira personalizada, Porta de embutir sistema duplo linear ou 45 graus, Porta de madeira de parede de drywall, Loja de fabrica de porta celeiro e Porta de madeira com espelho nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Porta de madeira personalizada em Santo Amaro.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>