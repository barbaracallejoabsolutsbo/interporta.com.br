<?php
$title       = "Porta de madeira laqueada pivotante em Sorocaba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de Porta de madeira laqueada pivotante em Sorocaba com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos. A laqueação deve ser feita por profissionais experientes no assunto, de preferência, por um bom pintor também. Essa porta está na moda, já que se tornaram sinônimo de elegância nas residências.</p>
<p>Especialista no mercado, a Interporta é uma empresa que ganha visibilidade quando se trata de Porta de madeira laqueada pivotante em Sorocaba, já que possui mão de obra especializada em Porta de vidro, Porta de madeira de parede de drywall, Manutenção de porta camarão, Manutenção de porta pivotante e Porta de correr trilho simples. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de Fabricante de Porta, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>