<?php
$title       = "Porta de madeira com aplicação de vidro em Bom Clima - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Porta de madeira com aplicação de vidro em Bom Clima - Guarulhos foi criada a partir dos boxes dos banheiros, lavanderias, jardim, e outros ambientes que poderiam ser molhados ou enfrentar mudanças do tempo ou temperaturas. Com o passar dos anos e com a melhoria da tecnologia, foi criado Porta de madeira com aplicação de vidro em Bom Clima - Guarulhos para todos os tipos de ambientes com diversos modelos e soluções.</p>
<p>Se você está em busca por Porta de madeira com aplicação de vidro em Bom Clima - Guarulhos conheça a empresa Interporta, pois somos especializados em Fabricante de Porta e trabalhamos com variadas opções de produtos e/ou serviços para oferecer, como Porta de madeira laqueada, Manutenção de porta com roldana, Porta de correr trilho simples, Porta de madeira celeiro e Porta de madeira de parede de drywall. Sabendo disso, entre em contato conosco e faça uma cotação, temos competentes profissionais para dar o melhor atendimento possível para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>