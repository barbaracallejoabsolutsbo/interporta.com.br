<?php
$title       = "Porta de madeira celeiro em Cabuçu de Cima - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo para todos os estados do Brasil Porta de madeira celeiro em Cabuçu de Cima - Guarulhos. A Porta de madeira celeiro em Cabuçu de Cima - Guarulhos não é uma novidade no mercado, mas sempre será uma boa escolha para a sua residência. A cada dia que passa surge novos modelos de portas, com materiais diferenciados e tecnologia avançada.</p>
<p>Sendo referência no ramo Fabricante de Porta, garante o melhor em Porta de madeira celeiro em Cabuçu de Cima - Guarulhos, a empresa Interporta trabalha com os profissionais mais qualificados do mercado em que atua, com experiências em Porta de madeira de correr, Porta de madeira de roldana aparente, Troca de folha de porta, Porta de embutir sistema duplo linear ou 45 graus e Manutenção de porta embutida para assim atender as reais necessidades de nossos clientes e parceiros. Venha conhecer a qualidade de nosso trabalho e nosso atendimento diferenciado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>