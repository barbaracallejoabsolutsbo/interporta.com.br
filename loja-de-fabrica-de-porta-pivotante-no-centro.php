<?php
$title       = "Loja de fabrica de porta pivotante no Centro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Por ser uma loja de fábrica de porta pivotante, nossa empresa consegue oferecer portas pivotantes de diversos tamanhos e podendo ser feitas sob medidas. Além disso, fazemos portas pivotante sob medida e oferecemos preços e condições diferenciadas para todos os nossos clientes. As portas pivotantes geralmente são mais largas do que as portas comuns.</p>
<p>Se está procurando por Loja de fabrica de porta pivotante no Centro e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Interporta é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Fabricante de Porta conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Porta de madeira laqueada roldana aparente, Porta de vidro fosco, Porta de madeira com aplicação de vidro, Manutenção de porta embutida e Porta de madeira laqueada embutida.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>