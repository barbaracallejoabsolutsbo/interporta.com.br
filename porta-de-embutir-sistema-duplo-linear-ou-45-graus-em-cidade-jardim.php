<?php
$title       = "Porta de embutir sistema duplo linear ou 45 graus em Cidade Jardim";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossa empresa é uma fabricante de Porta de embutir sistema duplo linear ou 45 graus em Cidade Jardim. Produzimos esquadrias sob medidas e com diversos tipos de matérias com a finalidade de atender todos os gostos e necessidades dos nossos clientes. Com profissionais experientes e capacitados para orientar você da melhor forma e realizar todos os serviços necessários, somos referência no mercado.</p>
<p>Se está procurando por Porta de embutir sistema duplo linear ou 45 graus em Cidade Jardim e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Interporta é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Fabricante de Porta conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Loja de fabrica de porta roldana aparente, Porta de madeira com passagem para pet, Porta de vidro, Troca de ferragens de porta e Manutenção de porta com roldana.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>