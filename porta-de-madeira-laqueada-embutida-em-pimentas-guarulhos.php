<?php
$title       = "Porta de madeira laqueada embutida em Pimentas - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sempre se atualizando e trazendo as novidades do segmento, oferecemos os melhores serviços e produtos como Porta de madeira laqueada embutida em Pimentas - Guarulhos com materiais de primeira linha e profissionais competentes e atualizados que buscam a máxima qualidade dos nossos serviços e dos nossos produtos. Entre em contato com a gente e saiba mais sobre nossos produtos e serviços!</p>
<p>Além de sermos uma empresa especializada em Porta de madeira laqueada embutida em Pimentas - Guarulhos disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Porta de madeira com aplicação de vidro, Porta de madeira embutida na parede, Manutenção de porta com roldana, Porta de madeira laqueada e Porta de vidro. Com a ampla experiência que a equipe Interporta possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Fabricante de Porta.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>