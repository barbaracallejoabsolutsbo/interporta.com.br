<?php
$title       = "Loja de fabrica de porta celeiro em Monte Carmelo - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossa loja de fábrica de porta celeiro produz portas em diversos tamanhos e também sob medidas. As portas celeiro estão sendo usadas em quartos, despensas, cozinhas, banheiros, e até escritórios que procuram um design mais rústico. Elas também são usadas como porta de entrada das residências. Essas portas costumam deixas expostas as ferragens e as corrediças como forma de decoração.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Interporta ganha destaque quando o assunto é Fabricante de Porta. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Loja de fabrica de porta celeiro em Monte Carmelo - Guarulhos até Porta de madeira de parede de alvenaria, Porta de madeira com friso, Loja de fabrica de porta pivotante, Porta de madeira com passagem para pet e Porta de madeira com aplique de veneziana superior ou inferior com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>