<?php
$title       = "Porta de madeira de parede de drywall na Vila Galvão - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Drywall é uma expressão que vem do inglês, que traduzida significa “parede seca”. O sistema utilizado na parede de drywall é formado por estruturas de perfis de aço e chapas de gesso. Portas de madeira de parede de drywall suportam colocação de portas e oferecem muito mais opções, já que as tecnologias do drywall foram feitas para aplicação de paredes e também de divisórias.</p>
<p>Tendo como especialidade Porta de vidro, Porta de madeira laqueada roldana aparente, Porta de madeira de roldana aparente, Troca de ferragens de porta e Porta de madeira de parede de alvenaria, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de Fabricante de Porta. Por isso, quando falamos de Porta de madeira de parede de drywall na Vila Galvão - Guarulhos, buscar pelos membros da empresa Interporta é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>