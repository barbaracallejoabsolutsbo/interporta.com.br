<?php
$title       = "Porta de embutir sistema duplo linear ou 45 graus em Guarujá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossa empresa é uma fabricante de Porta de embutir sistema duplo linear ou 45 graus em Guarujá. Produzimos esquadrias sob medidas e com diversos tipos de matérias com a finalidade de atender todos os gostos e necessidades dos nossos clientes. Com profissionais experientes e capacitados para orientar você da melhor forma e realizar todos os serviços necessários, somos referência no mercado.</p>
<p>Como uma empresa de confiança no mercado de Fabricante de Porta, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Porta de embutir sistema duplo linear ou 45 graus em Guarujá. A Interporta vem crescendo e mostrando seu potencial através de Manutenção de porta pivotante, Porta de madeira com passagem para pet, Porta de madeira telescópica, Porta de madeira com aplicação de vidro e Porta de correr trilho simples, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>