<?php
$title       = "Manutenção de porta pivotante no Bom Retiro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se a porta pivotante for instalada em um ambiente externo, é necessário sempre notar as condições climáticas do local, já que essas variações podem levar a necessidade da Manutenção de porta pivotante no Bom Retiro. Mesmo tendo muitas vantagens, a porta pivotante é um pouco mais frágil que as outras portas, necessitando de uma estrutura mais rígida para garantir a segurança da porta.</p>
<p>Como uma empresa especializada em Fabricante de Porta proporcionamos sempre o melhor quando falamos de Porta de madeira com aplicação de vidro, Porta de embutir sistema duplo linear ou 45 graus, Troca de folha de porta, Porta de madeira com passagem para pet e Manutenção de porta de giro. Com potencial necessário para garantir qualidade e excelência em Manutenção de porta pivotante no Bom Retiro com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Interporta trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>