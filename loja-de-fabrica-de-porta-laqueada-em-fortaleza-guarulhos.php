<?php
$title       = "Loja de fabrica de porta laqueada em Fortaleza - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A laca é um tipo de acabamento feito com pintura automotiva em móveis e portas. O laqueamento tem como função selar as superfícies, eliminando riscos, imperfeições, emendas, ranhuras, entre outras coisas. A resistência e a durabilidade das peças são inquestionáveis, pois a tinta utilizada é a mesma que utilizam nos automóveis. Loja de fábrica de porta laqueada é com a Interporta!</p>
<p>Entre em contato com a Interporta se você busca por Loja de fabrica de porta laqueada em Fortaleza - Guarulhos. Somos uma empresa especializada com foco em Manutenção de porta camarão, Porta de madeira convencional, Loja de fabrica de porta celeiro, Porta de madeira celeiro laqueada e Porta de madeira com friso onde garantimos o melhor para nossos clientes, uma vez que, contamos com o conhecimento adequado para o ramo de Fabricante de Porta. Entre em contato e faça um orçamento com um de nossos especialistas e garanta o melhor custo x benefício do mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>