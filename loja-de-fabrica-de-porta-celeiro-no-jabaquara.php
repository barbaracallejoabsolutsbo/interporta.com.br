<?php
$title       = "Loja de fabrica de porta celeiro no Jabaquara";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. A porta celeiro é uma porta de correr com um design inspirado na rusticidade das portas de celeiro tradicionais encontradas nos campos. Essas portas viraram opções em diversas casas do mundo como peças de decoração de interiores. Loja de fábrica de porta celeiro é com a gente.</p>
<p>Se deseja comprar Loja de fabrica de porta celeiro no Jabaquara e procura por uma empresa séria e competente, a Interporta é a melhor opção. Com uma equipe formada por profissionais experientes e qualificados, dos quais trabalham para oferecer soluções diferenciadas para o projeto de cada cliente. Porta de madeira com aplique de veneziana superior ou inferior, Porta de madeira com aplicação de vidro, Porta de correr trilho simples, Porta de embutir sistema duplo linear ou 45 graus e Manutenção de porta pivotante. Entre em contato e saiba tudo sobre Fabricante de Porta com os melhores profissionais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>