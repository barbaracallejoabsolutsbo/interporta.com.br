<?php
$title       = "Porta de madeira de correr em Grajaú";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Porta de madeira de correr em Grajaú é uma ótima opção para destacar a porta e deixar o ambiente mais bonito, já que a porta de madeira tem um visual robusto valorizando o ambiente e tornando-se parte da decoração do ambiente. A Interporta é a única loja especializada em portas de correr embutidas na parede do Brasil! Entre em contato com a nossa empresa e saiba mais.</p>
<p>Com uma ampla atuação no segmento, a Interporta oferece o melhor quando falamos de Porta de madeira de correr em Grajaú proporcionando aos seus clientes a máxima qualidade e desempenho em Troca de ferragens de porta, Manutenção de porta camarão, Manutenção de porta com roldana, Porta de madeira de parede de alvenaria e Porta de madeira celeiro laqueada, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de Fabricante de Porta.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>