<?php
$title       = "Porta de madeira bandô em Bela Vista - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossa empresa possui patentes de todos os produtos que são produzidos, sendo que muitos possuem mais de uma patente. Portanto, qualquer semelhança não será mera coincidência, mas sim cópia falsificada. Aperfeiçoe seu espaço, valorizando o ambiente com exclusividade e personalidade com a de Porta de madeira bandô em Bela Vista - Guarulhos. Entre agora em contato com a nossa empresa por telefone ou e-mail e faça um orçamento.</p>
<p>Com a Interporta proporcionando o que se tem de melhor e mais moderno no segmento de Fabricante de Porta consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Porta de madeira com friso, Porta de madeira laqueada embutida, Porta de correr trilho simples, Porta de madeira com boiserie e Porta de madeira de correr nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Porta de madeira bandô em Bela Vista - Guarulhos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>