<?php header("Content-type: text/html; charset=iso-8859-1"); ?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>InterPortas</title>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open Sans">
<link rel="stylesheet" href="http://www.habilitacaonet.com.br/administracao/includes/estilos.css">
<link rel="stylesheet" href="includes/estilos.css" />

<script type="text/javascript" src="includes/jquery.js"></script>
<script type="text/javascript" src="includes/thickbox.js"></script>
<link rel="stylesheet" href="includes/thickbox.css" type="text/css" media="screen">


<style type="text/css">

#menu-vertical{

  position:fixed;
  z-index:999;
  overflow:hidden;
  width:100%;

}

.menu{background:transparent;list-style:none; float:left; }

.menu li{position:relative; float:left; }

.menu li a{color:orange; text-decoration:none; display:block;}

.menu li a:hover{background:transparent; color:orange;}

.menu li   ul{position:fixed; top:80px; left:700px;background:transparent; display:none; }

.menu li:hover ul, .menu li.over ul{display:block;}

.menu li ul li{padding-bottom:10px;height:450px;display:block; width:550px;}


.dropdown {
    position: relative;
    display: inline-block;
}



.dropdown-content {
    display: none;
    position: fixed;
    background-color: #e0e0e0;
    min-width: 100px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    z-index: 1;
    width:200px;

}

.dropdown:hover .dropdown-content {
    display: block;
}

</style>

</head>

<body marginwidth="0" marginheight="0" topmargin="0" leftmargin="0" >

		<Table width="100%" cellspacing="0" cellpadding="0" style="border:0px solid #f0f0f0;" align="center">
		<tR>
			<Td>

			<div id="menu-vertical">

			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<Td style="border-bottom:1px solid #666;background-color: rgba(1,1,1,0.7);">

						<Table width="1020" cellspacing="0" cellpadding="0" style="border:0px solid #f0f0f0;" align="center">
							<Tr>
								<Td width="36"><img src="images/email.png" width="36" height="36"></Td>
								<Td style="color:#ffffff;font-family:Open Sans;font-size:13px;font-family:Open Sans;">vendas@interporta.com.br</Td>
								<Td width="28"><img src="images/horario.png" width="24" height="24"></Td>
								<Td style="color:#ffffff;font-family:Open Sans;font-size:13px;font-family:Open Sans;">Segunda &agrave; Sexta: 08:30 &agrave;s 17:30 | S&aacute;bado: 09:00 &agrave;s 16:00</Td>
								<Td width="36"><img src="images/telefones.png" width="24" height="24"></Td>
								<Td style="color:#ffffff;font-family:Open Sans;font-size:13px;font-family:Open Sans;">11 3427.8000 | 11 3926-2900</Td>
								<Td style="border-left:0px solid #f0f0f0;padding-left:10px;"><img src="images/whatsapp.png" width="24" height="24"></Td>
								<Td style="color:#ffffff;font-family:Open Sans;font-size:13px;font-family:Open Sans;">11 95109-1702 </Td>
								<Td></Td>

							</Tr>
						</Table>

					</Td>
				</tr>
				<TR>
					<Td style="padding-top:5px;padding-bottom:10px;background-color: rgba(1,1,1,0.7);">



					<script type="text/javascript" language="javascript">

						function mostra(menu){

							if (menu == "s"){
								document.getElementById("submenu").style.display = "block";
							}

							if (menu == "p"){
								document.getElementById("submenup").style.display = "block";
							}

							if (menu == "s1"){
								document.getElementById("submenu").style.display = "block";
								document.getElementById("submenu1").style.display = "block";
							}


						}

						function oculta(menu){

							if (menu == "s"){
								document.getElementById("submenu").style.display = "none";
							}

							if (menu == "p"){
								document.getElementById("submenup").style.display = "none";
							}

							if (menu == "s1"){
								document.getElementById("submenu").style.display = "none";
								document.getElementById("submenu1").style.display = "none";
							}

						}

					</script>


						<Table width="1020" cellspacing="0" cellpadding="0" align="center" border="0">
							<tr>
								<Td width="115" style="font-family:Open Sans;font-size:22px;color:#FFF;font-weight: bold;">INTERPORTA</Td>
								<TD width="200"></TD>
								<Td align="center" style="font-weight:bold;font-family:Open Sans;font-size:16px;color:orange;"><a HREF="index.php" class="laranja">HOME</a></Td>
								<Td align="center" style="font-weight:bold;font-family:Open Sans;font-size:16px;color:orange;"><a HREF="quemsomos.php" class="laranja">QUEM SOMOS</a></Td>
								<Td align="center" style="font-weight:bold;font-family:Open Sans;font-size:16px;color:orange;"><a HREF="#" onmouseout="oculta('s')" onmouseover="mostra('s')" class="laranja">PORTAS</a>

									<span name="submenu" onmouseover="mostra('s')" id="submenu" onmouseout="oculta('s')" style="display: none;position: fixed;background-color: #e0e0e0; min-width: 100px; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); padding: 12px 16px; z-index: 1; width:200px;">

										<table>
											<Tr>
												<Td><a href="portas.php?area=1" class="laranja">PORTAS EMBUTIDAS</a></Td>
											</Tr>
											<Tr>
												<Td><a href="portas.php?area=2" class="laranja" onmouseover="mostra('s1')">PORTAS DE CORRER</a></Td>
											</Tr>
											<Tr>
												<Td><a href="portas.php?area=6" class="laranja">PORTAS DE VIDRO</a></Td>
											</Tr>
											<Tr>
												<Td><a href="portas.php?area=7" class="laranja">PORTAS PIVOTANTES</a></Td>
											</Tr>
											<Tr>
												<Td><a href="portas.php?area=8" class="laranja">PORTAS REVESTIDAS</a></Td>
											</Tr>

										</table>

									</span>

									<span name="submenu1" onmouseover="mostra('s1')" id="submenu1" onmouseout="oculta('s1')" style="display:none;left:908px;position: fixed;background-color: #e0e0e0; min-width: 100px; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); padding: 12px 16px; z-index: 1; width:200px;">

										<table>
											<Tr>
												<Td><a href="portas.php?area=3" class="laranja">PORTA BAND&Ocirc;</a></Td>
											</Tr>
											<Tr>
												<Td><a href="portas.php?area=4" class="laranja">ROLDANA APARENTE</a></Td>
											</Tr>
											<Tr>
												<Td><a href="portas.php?area=5" class="laranja">TELESC&Ocirc;PICAS</a></Td>
											</Tr>
										</table>

									</span>



								</Td>
								<Td align="center" style="font-weight:bold;font-family:Open Sans;font-size:16px;color:orange;"><a HREF="#" onmouseout="oculta('p')" onmouseover="mostra('p')" class="laranja">ACESS&Oacute;RIOS</a>

									<span name="submenup" onmouseover="mostra('p')" id="submenup" onmouseout="oculta('p')" style="display: none;position: fixed;background-color: #e0e0e0; min-width: 100px; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); padding: 12px 16px; z-index: 1; width:200px;">

										<table>
											<Tr>
												<Td><a href="acessorios.php?area=9" class="laranja">VEDA&Ccedil;&Atilde;O</a></Td>
											</Tr>
											<Tr>
												<Td><a href="acessorios.php?area=10" class="laranja">FECHADURAS</a></Td>
											</Tr>
											<Tr>
												<Td><a href="acessorios.php?area=11" class="laranja">PUXADORES</a></Td>
											</Tr>
										</table>

									</span>


								</Td>
								<Td align="center" style="font-weight:bold;font-family:Open Sans;font-size:16px;color:orange;"><a HREF="paineis.php?area=12" class="laranja">PAIN&Eacute;IS DECORATIVOS</a></Td>
								<Td align="center" style="font-weight:bold;font-family:Open Sans;font-size:16px;color:orange;"><a HREF="contato.php" class="laranja">CONTATO</A></Td>
							</tr>
						</Table>


					</Td>
				</TR>
			</table>

		</div>









				</Td>
		</tR>
		<TR>
			<Td align="center" style="padding-top:65px"><iframe src="http://www.interporta.com.br/slide/slide.php" name="bottomFrame" width="100%" height="520" scrolling="no" frameborder="0" allowautotransparency="true"/></iframe></Td>
		</TR>
		<TR>
			<Td style="padding-bottom:20px;" valign="top">

				<Table width="1020" cellspacing="0" cellpadding="4" style="border:0px solid #f0f0f0;" align="center">
					<tr>
						<Td align="center" style="font-family:Open Sans;color:orange;font-size:25px;font-weight:bold;">Nossos produtos</Td>
					</tr>
					<TR>
						<Td style="padding-top:10px;padding-bottom:10px;" align="center"><img src="images/imagembaixo.jpg"></Td>
					</TR>
					<tr>
						<Td align="center" style="font-family:Open Sans;font-size:13px;"><b>Todos nossos produtos s&atilde;o patenteados exclusivamente pela Interporta</b><BR>Somos especializados em projetos com madeira, contamos com profissionais capacitados e treinados para dar vida ao seu projeto.</Td>
					</tr>
					<Tr>
						<Td style="padding-top:20px;padding-bottom:10px;">

							<table width="100%" cellspacing="5" cellpadding="10">
								<Tr>
									<td width="25%"><A href="portas.php?area=1"><img style="-webkit-filter: drop-shadow(10px 10px 7px rgba(0, 0, 0, .2));border:4px solid #ffffff;" height="250" width="100%" src="images/portaembutida.jpg"></A></td>
									<td width="25%"><A href="portas.php?area=7"><img style="-webkit-filter: drop-shadow(10px 10px 7px rgba(0, 0, 0, .2));border:6px solid #ffffff;" height="250" width="100%" src="images/portapivotante.jpg"></a></td>
									<td width="25%"><A href="portas.php?area=2"><img style="-webkit-filter: drop-shadow(10px 10px 7px rgba(0, 0, 0, .2));border:4px solid #ffffff;" height="250" width="100%" src="images/portadecorrer.jpg"></A></td>
									<td width="25%"><A href="paineis.php?area=12"><img style="-webkit-filter: drop-shadow(10px 10px 7px rgba(0, 0, 0, .2));border:2px solid #ffffff;" height="250" width="100%" src="images/paineis/painel5.jpg"></A></td>
								</Tr>
								<Tr>
									<td align="center" style="padding-top:5px;padding-bottom:10px;font-family:Open Sans;font-size:16px;font-weight: bold;">Portas de Correr Embutidas</td>
									<td align="center" style="padding-top:5px;padding-bottom:10px;font-family:Open Sans;font-size:16px;font-weight: bold;">Portas Pivotantes</td>
									<td align="center" style="padding-top:5px;padding-bottom:10px;font-family:Open Sans;font-size:16px;font-weight: bold;">Portas de Correr</td>
									<td align="center" style="padding-top:5px;padding-bottom:10px;font-family:Open Sans;font-size:16px;font-weight: bold;">Pain�is Decorativos</td>
								</Tr>
							</table>

						</Td>
					</Tr>
				</Table>

			</Td>
		</TR>
		<Tr>
			<Td bgcolor="#333333">

				<Table width="1020" cellspacing="0" cellpadding="4" style="border:0px solid #f0f0f0;" align="center">
					<TR>
						<td align="center" style="font-family:Open Sans;color:orange;font-size:25px;font-weight:bold;padding-top:30px;padding-bottom:10px;">Projetos realizados</td>
					</TR>
					<tr>
						<Td align="center" style="color:#fff;font-family:Open Sans;font-size:13px;">Firmamos uma longa parceria com nossos clientes, baseada em �tica, respeito e confian�a. Sempre oferecendo um servi�o completo e seguro.</Td>
					</tr>
					<Tr>
						<td style="padding-top:20px;padding-bottom:20px;">

							<table width="100%" cellspacing="5" cellpadding="10">

								<?php
								include("includes/funcoes/abre_conexao.php");
								?>

								<Tr>

									<?php

									$area_pasta = "projetos";

									$sql = "Select area, id_produto from tb_produtos where area = '13' order by data_cadastro desc limit 0,8";
									$resultado = $MySQLi->query($sql) OR trigger_error($MySQLi->error, E_USER_ERROR);
									while ($produto = $resultado->fetch_object()) {

									$area = $produto->area;
									$id_produto = $produto->id_produto;

									//echo $area . "-" . $area_pasta . "-" .$id_produto . "<BR>";

									if (file_exists('images/' . $area_pasta . '/' . $id_produto . '.jpg')) {
									    $foto_produto = 'images/' . $area_pasta . '/' . $id_produto . '.jpg';
									} elseif (file_exists('/images/' . $area_pasta . '/' . $id_produto . '.jpeg')) {
									    $foto_produto = 'images/' . $area_pasta . '/' . $id_produto . '.jpeg';
									} elseif (file_exists('/images/' . $area_pasta . '/' . $id_produto . '.gif')) {
									    $foto_produto = 'images/' . $area_pasta . '/' . $id_produto . '.gif';
								    } elseif (file_exists('/images/' . $area_pasta . '/' . $id_produto . '.png')) {
									    $foto_produto = 'images/' . $area_pasta . '/' . $id_produto . '.png';
									}else{
									//	echo "nao existe " . $area_pasta . "-" .$id_produto . "<BR>";
									}

									//if fsoObject.FileExists(server.mappath("images/" & area_pasta & "/" & id_produto & ".jpg")) Then
									//	foto_produto = "images/" & area_pasta & "/" & id_produto & ".jpg"
									//elseif fsoObject.FileExists(server.mappath("images/" & area_pasta & "/" & id_produto & ".gif")) Then
									//	foto_produto = "images/" & area_pasta & "/" & ".gif"
									//elseif fsoObject.FileExists(server.mappath("images/" & area_pasta & "/" & id_produto & ".png")) Then
									//	foto_produto = "images/" & area_pasta & "/" & ".png"
									//else
									//	foto_produto = "images/produtoSemImagem.jpg"
									//End If

									$x++;

									if ($x == "5" || $x == "9"){
										echo "</tr><td colspan='4' height='5'></td></tr><tr>";
									}

									?>

									<td width="25%"><A href="<?php echo $foto_produto;?>" class="thickbox" rel="fotos"><img  style="-webkit-filter: drop-shadow(10px 10px 7px rgba(0, 0, 0, .2));border:4px solid #f0f0f0;" height="200" width="100%" src="<?php echo $foto_produto;?>"></a></td>

									<?php

									}

									$resultado->free();

									?>

								</Tr>



							</table>

						</td>
					</Tr>
				</Table>

			</Td>
		</TR>



		<?php

		include("includes/rodape.php");

		?>

	</table>

</body>
</html>
