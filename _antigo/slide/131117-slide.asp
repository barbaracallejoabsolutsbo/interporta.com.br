
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">    

  
    <title> Full Width Slider | RoyalSlider</title>
  

    <meta content="Touch-enabled image gallery and content slider plugin, that focuses on providing great user experience on every desktop and mobile device." name="description">
    <meta name="keywords" content="content slider, gallery, plugin, jquery, banner rotator">
    <meta name="viewport" content="width = device-width, initial-scale = 1.0" />
    <meta name="author" content="Dmitry Semenov">

    
    <link href="http://dimsemenov.com/plugins/royal-slider/royalslider/royalslider.css" rel="stylesheet">
    <script type="text/javascript" src="http://gc.kis.v2.scr.kaspersky-labs.com/14EA015C-87CB-A74D-8829-867AFDE2285B/main.js" charset="UTF-8"></script><script src="http://dimsemenov.com/plugins/royal-slider/royalslider/jquery-1.8.3.min.js"></script>
      
        <script src="http://dimsemenov.com/plugins/royal-slider/royalslider/jquery.royalslider.min.js?v=9.3.6"></script>
      
    

    
    <link href="http://dimsemenov.com/plugins/royal-slider/preview-assets/css/reset.css?v=1.0.4" rel="stylesheet">
    
     
        <link href="http://dimsemenov.com/plugins/royal-slider/royalslider/skins/minimal-white/rs-minimal-white.css?v=1.0.4" rel="stylesheet">
     
    

    
    <style>
      #full-width-slider {
  width: 100%;
  color: #000;
}
.royalSlider > .rsContent {
  visibility:hidden;
}
.coloredBlock {
  padding: 12px;
background:transparent;
  /*background: rgba(255,0,0,0.6);*/
  color: #FFF;
   width: 200px;
   left: 20%;
   top: 5%;
}
.infoBlock {
  position: absolute;
  top: 30px;
  right: 30px;
  left: auto;
  max-width: 25%;
  padding-bottom: 0;
  /*background: #FFF;
  background: rgba(255, 255, 255, 0.8);*/
  background:transparent;
  overflow: hidden;
  padding: 20px;
}
.infoBlockLeftBlack {
  color: #FFF;
  background: transparent;  
  /*background: #000;
  background: rgba(0,0,0,0.75);*/
  left: 30px;
  right: auto;
  display:none;
}
.infoBlock h4 {
  font-size: 20px;
  line-height: 1.2;
  margin: 0;
  padding-bottom: 3px;
  display:none;
}
.infoBlock p {
  font-size: 14px;
  margin: 4px 0 0;
  display:none;
}
.infoBlock a {
  color: #FFF;
  text-decoration: underline;
}
.photosBy {
  position: absolute;
  line-height: 24px;
  font-size: 12px;
  background: #FFF;
  color: #000;
  padding: 0px 10px;
  position: absolute;
  left: 12px;
  bottom: 12px;
  top: auto;
  border-radius: 2px;
  z-index: 25; 
} 
.photosBy a {
  color: #000;
}
.fullWidth {
  max-width: 1400px;
  margin: 0 auto 24px;
}

@media screen and (min-width:960px) and (min-height:660px) {
  .heroSlider .rsOverflow,
  .royalSlider.heroSlider {
      height: 520px !important;
  }
}

@media screen and (min-width:960px) and (min-height:1000px) {
    .heroSlider .rsOverflow,
    .royalSlider.heroSlider {
        height: 660px !important;
    }
}
@media screen and (min-width: 0px) and (max-width: 800px) {
  .royalSlider.heroSlider,
  .royalSlider.heroSlider .rsOverflow {
    height: 300px !important;
  }
  .infoBlock {
    padding: 10px;
    height: auto;
    max-height: 100%;
    min-width: 40%;
    left: 5px;
    top: 5px;
    right: auto;
    font-size: 12px;
    display:none;
  }
  .infoBlock h3 {
     font-size: 14px;
     line-height: 17px;

  }
}

    </style>
    
  </head>
  <body >
  <BR>
  <div class="sliderContainer fullWidth clearfix">
<div id="full-width-slider" class="royalSlider heroSlider rsMinW">
  <div class="rsContent">
    <img class="rsImg" src="4.jpg" alt="" />
    <div class="infoBlock infoBlockLeftBlack rsABlock" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4>Titulo da foto</h4>
      <p>Conteúdo da foto</p>
    </div>
  </div>
  <div class="rsContent">
    <img class="rsImg" src="7b.jpg" alt="" />
     <div class="infoBlock  rsAbsoluteEl" style="color:#000;" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4>Titulo da foto</h4>
      <p>Conteúdo da foto</p>
    </div>
  </div>
 <div class="rsContent">
    <img class="rsImg" src="8.jpg" alt="" />
    <div class="infoBlock rsABlock infoBlockLeftBlack" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4>Título da foto</h4>
      <p><a href="#">Leia mais</a></p>
    </div>
    </div>
 <div class="rsContent">

    <img class="rsImg" src="1.jpg" alt="" />
    <div class="infoBlock rsABlock infoBlockLeftBlack" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4>Título da foto</h4>
      <p><a href="#">Leia mais</a></p>
    </div>
    </div>
 <div class="rsContent">

    <img class="rsImg" src="2.jpg" alt="" />
    <div class="infoBlock rsABlock infoBlockLeftBlack" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4>Título da foto</h4>
      <p><a href="#">Leia mais</a></p>
    </div>
    </div>    
 <div class="rsContent">

    <img class="rsImg" src="3.jpg" alt="" />
    <div class="infoBlock rsABlock infoBlockLeftBlack" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4>Título da foto</h4>
      <p><a href="#">Leia mais</a></p>
    </div>
    </div>
 <div class="rsContent">

    <img class="rsImg" src="5.jpg" alt="" />
    <div class="infoBlock rsABlock infoBlockLeftBlack" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4>Título da foto</h4>
      <p><a href="#">Leia mais</a></p>
    </div>
    </div>    

  </div>
  <!--
  <div class="rsContent">
    <img class="rsImg" src="11.jpg" alt="" />
    <span class="photosBy rsAbsoluteEl" data-fade-effect="fa;se" data-move-offset="40" data-move-effect="bottom" data-speed="200">Fotos: <a href="#">Interporta</a></span>
  </div>
-->
<!--
  <div class="rsContent">
    <img class="rsImg" src="5.jpg" alt="" />
    <span class="photosBy rsAbsoluteEl" data-fade-effect="fa;se" data-move-offset="40" data-move-effect="bottom" data-speed="200">Fotos <a href="#">InterPorta</a></span>
  </div>
  <div class="rsContent">
    <img class="rsImg" src="6.jpg" alt="" />
    <span class="photosBy rsAbsoluteEl" data-fade-effect="fa;se" data-move-offset="40" data-move-effect="bottom" data-speed="200">Fotos <a href="#">InterPorta</a></span>
  </div>
  <div class="rsContent">
    <img class="rsImg" src="7.jpg" alt="" />
    <span class="photosBy rsAbsoluteEl" data-fade-effect="fa;se" data-move-offset="40" data-move-effect="bottom" data-speed="200">Fotos <a href="#">InterPorta</a></span>
  </div>
-->
</div>
  </div>
  <div class="wrapper page">
     
  
    <script>
      jQuery(document).ready(function($) {
  $('#full-width-slider').royalSlider({
    arrowsNav: true,
    loop: false,
    keyboardNavEnabled: true,
    controlsInside: false,
    imageScaleMode: 'fill',
    arrowsNavAutoHide: false,
    autoScaleSlider: true, 
    autoScaleSliderWidth: 960,     
    autoScaleSliderHeight: 350,
    controlNavigation: 'bullets',
    thumbsFitInViewport: false,
    navigateByClick: true,
    startSlideId: 0,
    autoPlay: {enabled:true},
    transitionType:'move',
    globalCaption: true,
    deeplinking: {
      enabled: true,
      change: false
    },
    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
    imgWidth: 1400,
    imgHeight: 680
  });
});

    </script>
  

  
  <div style="display:none;"><script type="text/javascript">
  (function(w, c) {
      (w[c] = w[c] || []).push(function() {
          try {
              w.yaCounter11382601 = new Ya.Metrika({id:11382601, enableAll: true, trackHash:false, webvisor:true});
          }
          catch(e) { }
      });
  })(window, "yandex_metrika_callbacks");
  </script></div>
  <script src="http://mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
  <noscript><div><img src="http://mc.yandex.ru/watch/11382601" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-25969065-1']);
    _gaq.push(['_trackPageview']);
  
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    function recordOutboundLink(link, category, action) {
      _gat._getTrackerByName()._trackEvent(category, action);
      setTimeout('document.location = "' + link.href + '"', 100);
    }
</script>
      
  </div>
  </body>
</html>
