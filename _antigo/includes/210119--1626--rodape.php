<tr>
			<Td style="padding-top:10px;padding-bottom:10px;background:#efefef;">

				<Table width="998" cellspacing="0" cellpadding="2" style="border:0px solid #c0c0c0;padding:10px 10px 10px 10px;" align="center">
					<Tr>
						<Td width="50%" valign="top">

							<Table width="100%" cellspacing="0" cellpadding="2" >
								<TR>
									<td style="font-family:Open Sans;font-size:14px;font-weight: bold;color:#666666;">Receba nossa <span style="color:orange;">Newsletter</span></td>
								</TR>
								<TR>
									<td style="font-style:italic;font-family:Open Sans;font-size:12px;font-weight: bold;color:#666666;">E n&atilde;o perca descontos e promo&ccedil;&otilde;es especiais</td>
								</TR>
							</Table>

						</Td>
						<Td width="50%" valign="top">

							<Table width="100%" cellspacing="0" cellpadding="2" >
								<tR>
									<Td><input type="text" placeholder="Digite seu e-mail" style="padding:10px 10px 10px 10px;width:400px;"></Td>
									<Td>

										<Table height="37" width="100%" bgcolor="#868686" cellspacing="0" cellpadding="2" style="padding:4px 4px 4px 4px;border:1px solid #868686;border-radius:3px;">
											<TR>
												<Td style="font-size:12px;font-family:Open SAns;color:#ffffff;">Assinar</Td>
												<Td><img src="images/assinar.jpg"></Td>
											</TR>
										</Table>

									</Td>
								</tR>
							</Table>

						</Td>
					</Tr>
				</Table>

			</Td>
		</tr>
		<tr>
			<Td style="padding-top:20px;">

				<Table width="998" cellspacing="0" cellpadding="2" style="padding:5px 5px 5px 5px;" align="center" border="0">
					<tR>
						<Td width="95%">

							<Table width="100%" cellspacing="0" cellpadding="2" style="padding:5px 5px 5px 5px;" align="center" border="0">
								<Tr>
									<Td width="33%" valign="top">

										<Table width="100%" cellspacing="0" cellpadding="2">
											<Tr>
												<Td align="center"><img src="images/logo.jpg"></Td>
											</Tr>
											<Tr>
												<Td>

													<Table>
														<Tr>
															<Td><img src="images/email.jpg"></Td>
															<Td style="font-family:Open Sans;font-size:11px;"><A href="mailto:vendas@interporta.com.br" style="color:#000;">vendas@interporta.com.br</a></Td>
														</Tr>
													</Table>

												</Td>
											</Tr>
											<Tr>
												<Td>

													<Table border="0">
														<Tr>
															<Td><img src="images/telefonerodape.jpg"></Td>
															<Td style="font-family:Open Sans;font-size:11px;color:#000000;">11 3427.8000 - 11 3926-2900 </Td>
														</Tr>
														<Tr>
															<Td><img src="images/whatsapp.png" width="16" height="16"></Td>
															<Td style="font-family:Open Sans;font-size:11px;color:#000000;">11 95109-1702</Td>
														</Tr>
													</Table>

												</Td>
											</Tr>
											<Tr>
												<Td align="left"><a href="https://www.facebook.com/interporta.portasembutidas" target="blank"><img width="24" height="24" src="images/facebook_rodape.png"></a></Td>
											</Tr>
										</Table>

									</Td>
									<!--<Td width="33%" valign="top">

										<!--<Table width="100%" cellspacing="0" cellpadding="2">
											<TR>
												<Td align="center" style="font-weight:bold;font-family:Open Sans;font-size:13px;color:#555555;">AJUDA</Td>
											</TR>
											<!--<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;padding-top:20px;color:#88797a;"><a class="preto" href="#">Perguntas Frequentes</a></Td>
											</Tr>
											<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;color:#88797a;"><a class="preto" href="#">Como comprar</a></Td>
											</Tr>-->
											<!--
											<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;color:#88797a;"><a class="preto" href="#">Trocas e Devolu&ccedil;&otilde;es</a></Td>
											</Tr>
											<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;color:#88797a;"><a class="preto" href="#">Formas de pagamento</a></Td>
											</Tr>

										</Table>

									</Td>-->
									<Td width="60%" valign="top">

										<Table width="100%" cellspacing="0" cellpadding="2">
											<TR>
												<Td align="left" style="padding-left:40px;font-weight:bold;font-family:Open Sans;font-size:13px;color:#555555;">INFORMA&Ccedil;&Otilde;ES</Td>
											</TR>
											<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;padding-top:20px;color:#88797a;"><a class="preto" href="quemsomos.php">Quem Somos</a></Td>
											</Tr>
											<!--
											<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;color:#88797a;"><a class="preto" href="entrega.php">Informa&ccedil;&otilde;es de Entrega</a></Td>
											</Tr>
										-->
											<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;color:#88797a;"><a class="preto" href="lojafisica.php">Loja F&iacute;sica</a></Td>
											</Tr>
											<!--
											<Tr>
												<Td style="padding-left:40px;font-family:Open Sans;font-size:11px;color:#88797a;"><a class="preto" href="nossosprodutos.php">Nossos Produtos</a></Td>
											</Tr>
										-->
										</Table>

									</Td>
								</Tr>
							</Table>

						</Td>
						<td valign="top" width="2%">

							<table cellspacing="0" cellpadding="4" border="0">
								<Tr>
									<Td align="center" valign="top"><img border="0" src="images/selo_top2.jpg" width="150"></Td>
									<Td valign="top"><img border="0" src="images/seloqualidade2.jpg" width="140"></Td>
								</Tr>
							</table>

						</td>
					</tR>
				</Table>




			</Td>
		</tr>
		<tr>
			<Td style="border-top:1px solid #c0c0c0;">

				<Table width="998" cellspacing="0" cellpadding="2" style="padding:5px 5px 5px 5px;" align="center" border="0">
					<tR>
						<Td style="font-size:11px;font-family:Open SAns;">Desenvolvido por: Estilo Virtual Solu&ccedil;&otilde;es para Internet</Td>
						<Td align="right" style="font-size:11px;font-family:Open SAns;">Parcelamos em at� 12x nos cart�es Visa e Master</Td>
						<td width="30%" align="right"><img src="images/formaspagamento.jpg"></td>
					</tR>
				</Table>

			</Td>
		</tr>