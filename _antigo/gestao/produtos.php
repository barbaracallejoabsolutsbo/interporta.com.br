<?php

$area = $_GET["id_area"];
$area_pasta = $_GET["pasta"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
<title>Gerenciamento de produtos</title>
<link rel="stylesheet" href="includes/estilos.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open Sans">

<script type="text/javascript">

function mostra(){

	document.getElementById("enviar").style.display ="block";


}

var str;
function excluir(pasta,str) {
	if (confirm("Tem certeza que deseja excluir esta imagem?")) {
		var xmlhttp;
		if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else { // code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		//document.getElementById("linha_" + str + "").innerHTML = "";
		//document.getElementById("linha_" + str + "").innerHTML = '<img src="images/indicator.gif" /> <span class="tamanho12 trebuchet">removendo...</span>';

		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("linha_" + str + "").innerHTML = "";
				alert(xmlhttp.responseText);
			}
		}

		window.open("imagens-exclui.php?pasta="+pasta+"&id_produto=" + str + "&acao=excluir")

		xmlhttp.open("POST","imagens-exclui.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("pasta="+pasta+"&id_produto=" + str + "&acao=excluir");
		//window.open("imagens-exclui.asp?pasta="+pasta+"&id_produto="+str)
		window.location.href='produtos.php?id_area=<?php echo $_GET["id_area"];?>&pasta=<?php echo $_GET["pasta"];?>';
	}
}

</script>

<style type="text/css">
<!--
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
.style2 {
	color: #FF3400;
	font-weight: bold;
}
-->
</style></head>

<body marginwidth="0" marginheight="0" topmargin="0" leftmargin="8">

<BR>

	<table width="1000" border="0" style="border:1px solid #c0c0c0;" cellspacing="0" cellpadding="4">
		<tr>
			<td>

				<table width="100%" cellspacing="0" cellpadding="2">
					<tr>
						<td class="tamanho22 trebuchet negrito corazul">Gerenciamento de produtos</td>
					</tr>
					<tr>
						<td class="tamanho13 trebuchet">Selecione abaixo as imagens que deseja enviar</td>
					</tr>
					<!--<tr>
						<td style="padding-top:20px;" class="tamanho13 trebuchet"><a href="#" onclick="mostra();return false;" class="azul">Enviar imagens</a></td>
					</tr>
					<tr>
						<td height="15"></td>
					</tr>
				-->

					<tr>
						<td align="right" style="padding-bottom:20px;padding-top:20px;">

							<span style="display: block;" id="enviar" name="enviar">


							<form action="enviar-imagens-upload.php" method="post" ENCTYPE="multipart/form-data" name="upload">
							<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
							<input type="hidden" name="area_pasta" id="area_pasta" value="<?php echo $area_pasta;?>">
							<input type="hidden" name="id_area" id="id_area" value="<?php echo $area;?>">

							<table cellspacing="0" cellpadding="2">
								<tR>
									<td align="right" style="font-family:Open Sans;font-size:11px;"class="style13">Imagem:</td>
									<td><input type="file" name="arquivo[]" multiple="multiple" /></td>
								</tr>
								<tr>
									<td colspan="2" align="right" style="padding-top:10px;"><input type="submit" value="Enviar imagens" class="botao_azul" style="background:#1059a5;color:#fff;padding:10px 10px 10px 10px;border:1px solid #1059a5;border-radius:1px;"></td>
								</tR>
							</table>

							</form>

							</span>


						</td>
					</tr>
					<tr>
						<td colspan="2">

							<table width="100%" cellspacing="0" style="padding:10px 10px 10px 10px;border:1px solid #e0e0e0;" cellpadding="4">

								<Tr>

								<?php

								include("../includes/funcoes/abre_conexao.php");

								$sql = "Select area, id_produto from tb_produtos where area = '$area' order by data_cadastro desc ";

								$resultado = $MySQLi->query($sql) OR trigger_error($MySQLi->error, E_USER_ERROR);
								while ($produto = $resultado->fetch_object()) {

								$area = $produto->area;
								$id_produto = $produto->id_produto;

								//echo $area_pasta . "--" . $id_produto . "<BR>";

								if (file_exists('../images/' . $area_pasta . '/' . $id_produto . '.jpg')) {
									$foto_produto = '../images/' . $area_pasta . '/' . $id_produto . '.jpg';
								} elseif (file_exists('../images/' . $area_pasta . '/' . $id_produto . '.jpeg')) {
									$foto_produto = '../images/' . $area_pasta . '/' . $id_produto . '.jpeg';
								} elseif (file_exists('../images/' . $area_pasta . '/' . $id_produto . '.gif')) {
									$foto_produto = '../images/' . $area_pasta . '/' . $id_produto . '.gif';
								} elseif (file_exists('../images/' . $area_pasta . '/' . $id_produto . '.png')) {
									$foto_produto = '../images/' . $area_pasta . '/' . $id_produto . '.png';
								}else{
								//	echo "nao existe " . $area_pasta . "-" .$id_produto . "<BR>";
									$foto_produto = "images/produtoSemImagem.jpg";
								}

								$x++;

								if ($x == "5" || $x == "9" || $x == "13" || $x == "17" || $x == "21" || $x == "25" || $x == "29" || $x == "33" || $x == "37" || $x == "41" || $x == "45" || $x == "49" || $x == "53" || $x == "57" || $x == "61" ||$x == "64" || $x == "68" || $x == "72" || $x == "76" || $x == "80" || $x == "84" || $x == "88" ||$x == "92"){
								 echo"</tr><td colspan='4' height='20'></td></tr><tr>";
								}

								?>

									<Td width="25%">

										<table>
											<TR>
												<Td><img style="border:1px solid #e0e0e0;" src="<?php echo $foto_produto;?>" width="250" height="331"></Td>
											</TR>
											<Tr>
												<td align="center" style="padding-top:10px;font-family:Open Sans;font-size:12px;"><A href="#" onclick="excluir('<?php echo $area_pasta;?>','<?php echo $id_produto;?>');return false;" class="azul">Excluir foto</a></td>
											</Tr>
										</table>

									</Td>

								<?php

								}

								$resultado->free();

								?>

								</Tr>

							</table>

						</td>
					</tr>
					<tr>
						<td height="10"></td>
					</tr>

					<tr>
						<td height="5"></td>
					</tr>
					<tr>
						<td align="center">


						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>

</body>
</html>
