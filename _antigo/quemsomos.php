
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Quem Somos - InterPortas</title>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open Sans">
<link rel="stylesheet" href="http://www.habilitacaonet.com.br/administracao/includes/estilos.css">
<link rel="stylesheet" href="includes/demo.css" />
<link rel="stylesheet" href="includes/jquery.fadeshow-0.1.1.min.css" />
<link rel="stylesheet" href="includes/estilos.css" />

</head>

<body marginwidth="0" marginheight="0" topmargin="0" leftmargin="0">

	<Table width="100%" cellspacing="0" cellpadding="0" style="border:0px solid #f0f0f0;" align="center">
		<tR>
			<Td>

			<?php
			include("includes/topo.php");
			?>


			</Td>
		</tR>
		<TR>
			<Td align="center" style="padding-top:65px"><iframe src="slide/slide.php" name="bottomFrame" width="100%" height="520" scrolling="no" frameborder="0" allowautotransparency="true"/></iframe></Td>
		</TR>
		<TR>
			<Td style="padding-bottom:20px;padding-top:20px;">

				<Table width="1020" cellspacing="0" cellpadding="4" style="border:0px solid #f0f0f0;" align="center">
					<tr>
						<Td align="center" style="font-family:Open Sans;color:orange;font-size:25px;font-weight:bold;">QUEM SOMOS</Td>
					</tr>
					<tr>
						<Td style="padding-top:20px;text-align:justify;font-family:Open Sans;font-size:13px;">

						A <b>Interporta</b> é uma loja especializada em portas de correr embutidas na parede. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. Com garantia de 15 anos do sistema deslizante. <BR><BR>

						<!--Trabalhamos com o diferencial de produção sob medida para melhor adequar-se aos projetos dos nossos clientes!<BR>
						As portas de correr embutidas na parede, representam uma praticidade e um ganho em m2 do imóvel, pois você não perde a largura da parede e passa a ter uma parede útil e não mais morta, os tipo podem ser diversos  desde a linha única  a duplas, triplas entre outras opções, pode ser para parede em alvenaria quanto parede em Drywall  e  a marca do produto há diversas no mercado mas disponibilizamos as que podem representar uma maior economia de preço ao consumidor de acordo com a necessidade do mesmo.<BR><BR>

						A <b>INTERPORTA</b> é uma empresa de domínio Brasileiro, constituída em Outubro de 2003, sendo o seu principal objetivo, promover o conceito de aproveitamento de espaço em habitações, lojas e escritórios através dos sistemas de portas de correr embutidas (Sistema exclusivo e <b>Patenteado por INTERPORTA</b>).<BR><BR>
					-->

						A <b>INTERPORTA</b> é fabricante, que oferece uma gama alargada de sistemas de portas de correr embutidas,  trilhos em alumínio e acessórios, batentes de madeira para portas interiores.. <BR><BR>

						Situada na cidade de São Paulo - uma das zonas com maior rendimento per capita e desenvolvimento econômico e industrial é servida por uma das principais vias de comunicação do Brasil, entre a cidade de São Paulo . A <b>INTERPORTA</b>, opera em todo o território brasileiro.<BR><BR>

						A <b>INTERPORTA</b> é a única loja especializada em Portas de Correr Embutidas na Parede do Brasil.<BR><BR>
						Produzimos Sistemas únicos e exclusivos, totalmente feitos sob medida, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais, com alturas e larguras específicas, e para portas com medidas e formatos especiais, inclusive Madeira de Demolição.<BR><BR>
						Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.<BR><BR>
						Com mais de 10 anos de experiência e mais de 8mil clientes satisfeitos, nossos sistemas são extremamente confiáveis, por isso agora oferecemos aos nossos clientes 15 anos de Garantia, oque completa a qualidade dos produtos que produzimos.<BR><BR>
						Somente a <b>INTERPORTA</b> possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente, portanto qualquer semelhança não será mera coincidência, mas sim cópia falsificada.<BR><BR>

						<Table width="20%" align="center" style="border:1px solid #000000;padding:5px 5px 5px 5px;">
							<Tr>
								<Td align="center" style="font-family:Open Sans;font-size:14px;font-weight:bold;">PIRATARIA É CRIME</Td>
							</Tr>
						</Table>

						<BR><BR>

						<center><img src="images/inpi.jpg" width="250"></center>

						<BR><BR>

						Otimize seu espaço, valorizando o ambiente com exclusividade e personalidade.<BR><BR>

						Assista aos vídeos e conheça um pouco mais sobre a <B>INTERPORTA</B>!

					</Td>
					</tr>
					<tr>
						<Td style="padding-top:30px;">

							<TABLE WIDTH="100%" CELLSPACING="0" CELLPADDING="4" BORDER="0">
								<tR>
									<tD align="center"><iframe width="80%" height="315" src="https://www.youtube.com/embed/kxAYrpUJc58" frameborder="0" allowfullscreen></iframe></tD>
								</tR>
								<tR>
									<tD align="center" valign="top" style="padding-top:20px;">

									<iframe width="80%" height="315" src="https://www.youtube.com/embed/LdVKUUWheNI" frameborder="0" allowfullscreen></iframe>

									</tD>
								</tR>
								<tR>
									<tD align="center" valign="top" style="padding-top:20px;">

									<iframe width="80%" height="315" src="https://www.youtube.com/embed/lUseVsnohVY" frameborder="0" allowfullscreen></iframe>

									</tD>
								</tR>

							</TABLE>

						</Td>
					</tr>

				</Table>

			</Td>
		</TR>

		<?php
		include("includes/rodape.php");
		?>

	</table>

</body>
</html>
