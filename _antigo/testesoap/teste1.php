<?php

//21.295.901/0001-28
$ip = $_SERVER["REMOTE_ADDR"];
$data_hora = date("YmdHms");
$autenticacao = "0574566000000000000000001512201700000000000354721295901000128";
$autenticacao = base64_encode($autenticacao);
$data_vencimento= date("Ymd");
$data_emissao= date("Ymd");
$data_juros= date("Ymd");


//echo "autenticacao:==" . $autenticacao . "<BR>";

$param = new stdClass();

$header = new stdClass();
$header->VERSAO = "1.0";
$header->AUTENTICACAO = $autenticacao;
$header->USUARIO_SERVICO = "SGCBS02P";
$header->OPERACAO= "INCLUI_BOLETO";
$header->SISTEMA_ORIGEM="SIGCB";
$header->UNIDADE="0017";
$header->IDENTIFICADOR_ORIGEM=$ip;
$header->DATA_HORA=$data_hora;
$header->ID_PROCESSO="574566";

/**
demais atributos do header
**/

//Adiciona os parāmetros de HEADER
$param->HEADER = $header

$dados = new stdClass();
$inclui_boleto = new stdClass();
$inclui_boleto->CODIGO_BENEFICIARIO= "0574566";

$inclui_boleto->titulo= new stdClass();
//$titulo = new stdClass();

$inclui_boleto->titulo->NOSSO_NUMERO= "00000000000000000";
$inclui_boleto->titulo->NUMERO_DOCUMENTO= "12345678901";
$inclui_boleto->titulo->DATA_VENCIMENTO= $data_vencimento;
$inclui_boleto->titulo->VALOR= "0000000000035.50";
$inclui_boleto->titulo->TITULO_ESPECIE= "99";
$inclui_boleto->titulo->FLAG_ACEITE= "A";
$inclui_boleto->titulo->DATA_EMISSAO= $data_emissao;

$inclui_boleto->titulo->juros_mora = new stdClass();
$inclui_boleto->titulo->juros_mora->TIPO= "VALOR_POR_DIA";
$inclui_boleto->titulo->juros_mora->DATA= $data_juros;
$inclui_boleto->titulo->juros_mora->VALOR= "0000000000001.50";

$inclui_boleto->VALOR_ABATIMENTO= "0000000000000.00";

$inclui_boleto->titulo->pos_vencimento = new stdClass();
$inclui_boleto->titulo->pos_vencimento->ACAO= "PROTESTAR";
$inclui_boleto->titulo->pos_vencimento->NUMERO_DIAS= "30";

$inclui_boleto->CODIGO_MOEDA= "09";

$inclui_boleto->titulo->pagador = new stdClass();
$inclui_boleto->titulo->pagador->CPF= "12345678901";
$inclui_boleto->titulo->pagador->NOME= "NOME DO ALUNO COMPRADOR";

$inclui_boleto->titulo->pagador->endereco = new stdClass();
$inclui_boleto->titulo->pagador->endereco->LOGRADOURO= "TRISTAO GAGO - 07";
$inclui_boleto->titulo->pagador->endereco->BAIRRO= "GUAIANASES";
$inclui_boleto->titulo->pagador->endereco->CIDADE= "SAO PAULO";
$inclui_boleto->titulo->pagador->endereco->UF= "SP";
$inclui_boleto->titulo->pagador->endereco->CEP= "08441320";

$inclui_boleto->titulo->sacador = new stdClass();
$inclui_boleto->titulo->sacador->CNPJ= "21295901000128";
$inclui_boleto->titulo->sacador->RAZAO_SOCIAL= "CURSOS ONLINE EDUCA";

$inclui_boleto->titulo->pagamento = new stdClass();
$inclui_boleto->titulo->pagamento->QUANTIDADE_PERMITIDA= "1";
$inclui_boleto->titulo->pagamento->TIPO= "NAO_ACEITA_VALOR_DIVERGENTE";
$inclui_boleto->titulo->pagamento->VALOR_MINIMO= "0000000000000.00";
$inclui_boleto->titulo->pagamento->VALOR_MAXIMO= "0000000000000.00";

/**
dados do atributo INCLUI_BOLETO
**/

//Adiciona os parāmetros de INCLUI_BOLETO
$dados->INCLUI_BOLETO = $inclui_boleto;

$client = new SoapClient("https://barramento.caixa.gov.br/sibar/ManutencaoCobrancaBancaria/Boleto/Externo?wsdl" , ['trace' => 1]);
$compra = $client->INCLUI_BOLETO($params);

echo "Resultado<BR><BR><hr>";

echo $compra;

?>

