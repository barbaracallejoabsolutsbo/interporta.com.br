<?php
/*
 *	$Id: client3.php,v 1.4 2007/11/06 14:48:24 snichol Exp $
 *
 *	Client sample.
 *
 *	Service: SOAP endpoint
 *	Payload: rpc/encoded
 *	Transport: http
 *	Authentication: none
 */
require_once('lib/nusoap.php');
$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
$useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
//$client = new nusoap_client("https://barramento.caixa.gov.br/sibar/ManutencaoCobrancaBancaria/Boleto/Externo", false,$proxyhost, $proxyport, $proxyusername, $proxypassword);
$client = new nusoap_client("https://barramento.caixa.gov.br/sibar/ManutencaoCobrancaBancaria/Boleto/Externo", false);
$err = $client->getError();
if ($err) {
	echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
	exit();
}
$client->setUseCurl($useCURL);
$client->soap_defencoding = 'UTF-8';

//echo 'You must set your own Google key in the source code to run this client!'; exit();
$params = array(
	'versao'=>'1.0',
	'autenticacao'=>'wIXyzrhD7HjlrpCP1xNnq8vFE7ggAGuz/srw1BOtxDY=',
	'operacao'=>'INCLUI_BOLETO',
	'sistema_origem'=>'SIGCB',
	'data_hora'=>'20171129080311'
);
$result = $client->call("INCLUI_BOLETO", $params, "urn:caixa", "urn:caixa");
if ($client->fault) {
	echo '<h2>Fault</h2><pre>'; print_r($result); echo '</pre>';
} else {
	$err = $client->getError();
	if ($err) {
		echo '<h2>Erro</h2><pre>' . $err . '</pre>';
	} else {
		echo '<h2>Resultado</h2><pre>'; print_r($result); echo '</pre>';
	}
}
echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
?>
