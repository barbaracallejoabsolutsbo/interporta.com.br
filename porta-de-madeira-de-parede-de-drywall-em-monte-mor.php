<?php
$title       = "Porta de madeira de parede de drywall em Monte Mor";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Drywall é uma expressão que vem do inglês, que traduzida significa “parede seca”. O sistema utilizado na parede de drywall é formado por estruturas de perfis de aço e chapas de gesso. Portas de madeira de parede de drywall suportam colocação de portas e oferecem muito mais opções, já que as tecnologias do drywall foram feitas para aplicação de paredes e também de divisórias.</p>
<p>Sendo uma das empresas mais confiáveis no ramo de Fabricante de Porta, a Interporta ganha destaque por ser confiável e idônea quando falamos não só de Porta de madeira de parede de drywall em Monte Mor, mas também quando o assim é Porta de madeira com aplique de veneziana superior ou inferior, Porta de madeira laqueada embutida, Porta de madeira de parede de alvenaria, Porta de madeira laqueada pivotante e Troca de ferragens de porta. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>