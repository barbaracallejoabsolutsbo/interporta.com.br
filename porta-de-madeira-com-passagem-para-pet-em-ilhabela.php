<?php
$title       = "Porta de madeira com passagem para pet em Ilhabela";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A porta de madeira com passagem pet é feita para que o seu pet, seja ele um cachorro ou gato, possa entrar e sair do cômodo sem maiores dificuldades. Essa porta promove a interação dos ambientes para o pet, fazendo com que o animal que a sua família tanto goste tenha mais liberdade e facilidade para passar da área interna para a área externa. Entre em contato com a nossa empresa e saiba mais sobre nossos produtos.</p>
<p>Sendo referência no ramo Fabricante de Porta, garante o melhor em Porta de madeira com passagem para pet em Ilhabela, a empresa Interporta trabalha com os profissionais mais qualificados do mercado em que atua, com experiências em Porta de madeira laqueada pivotante, Loja de fabrica de porta pivotante, Porta de madeira camarão, Porta de madeira com aplique de veneziana superior ou inferior e Porta de madeira com passagem para pet para assim atender as reais necessidades de nossos clientes e parceiros. Venha conhecer a qualidade de nosso trabalho e nosso atendimento diferenciado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>