<?php
$title       = "Porta de madeira convencional em Jardim Presidente Dutra - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta esta situada na cidade de São Paulo, uma das cidades com maior desenvolvimento econômico do Brasil, produzindo e fornecendo para todos os estados do Brasil Porta de madeira convencional em Jardim Presidente Dutra - Guarulhos. Produzimos sistemas únicos e exclusivos, totalmente feitos sob medidas, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais. </p>
<p>Sendo uma das principais empresas do segmento de Fabricante de Porta, a Interporta possui os melhores recursos do mercado com o objetivo de disponibilizar Porta de embutir sistema duplo linear ou 45 graus, Porta de correr trilho simples, Porta de madeira de parede de alvenaria, Porta de madeira com aplique de veneziana superior ou inferior e Porta de madeira telescópica com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Porta de madeira convencional em Jardim Presidente Dutra - Guarulhos com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>