<?php

    $title       = "Contato";
    $description = "Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "contato"
    ));

?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="faixa-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <h1><?php echo $h1; ?></h1>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <div class="text-right">
                            <?php echo $padrao->breadcrumb(array($title)); ?>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo">
            <div class="container">
                <p>Preencha o formulário abaixo para enviar uma mensagem para nós.</p>
                <form class="form-contato">
                    <div class="form-group">
                        <input name="data[Contato][nome]" type="text" placeholder="Nome" class="form-control">                                    
                    </div>
                    <div class="form-group">
                        <input name="data[Contato][email]" type="email" placeholder="E-mail" class="form-control">
                    </div>
                    <div class="form-group">
                        <input name="data[Contato][telefone]" type="text" placeholder="Telefone" class="form-control mask-phone">
                    </div>
                    <div class="form-group">
                        <input name="data[Contato][assunto]" type="text" placeholder="Assunto" class="form-control">                                    
                    </div>
                    <div class="form-group">
                        <select name="data[Contato][como_conheceu]" class="form-control">
                            <option value=""> -- Selecione uma Opção -- </option>
                            <option value="Busca do Google">Busca do Google</option>
                            <option value="Outros Anúncios">Outros Anúncios</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Twitter">Twitter</option>
                            <option value="Indicação">Indicação</option>
                            <option value="Outros">Outros</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea name="data[Contato][mensagem]" placeholder="Mensagem" class="form-control" rows="6"></textarea>
                    </div>
                    <?php if($captcha){ ?>
                        <div class="g-recaptcha" data-sitekey="<?php echo $captcha_key_client_side; ?>"></div>
                    <?php } ?>
                    <div class="text-right">
                        <button type="submit" class="btn btn-default btn-block">Enviar</button>
                    </div>
                </form>
            </div>        
        </div>
        <div class="container">
            <div class="contatos-emp">
                <p><strong>Endereço: </strong>
                    <i class="fas fa-map-marker-alt"></i> <?php echo $unidades[1]["rua"] . " - " . $unidades[1]["bairro"] . " - " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"] . " - " . $unidades[1]["cep"]; ?>
                </p>
                <p><strong>E-mail: </strong>
                    <a title="E-mail" href="mailto:<?php echo $emailContato; ?>"><span itemprop="telephone"> <?php echo $emailContato; ?></span></a>
                </p>
                <p><strong>Tel: </strong>
<!--                     <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span>
                    </a> | 
                     <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone2"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone2"]; ?></span>
                    </a>  -->
                </p>
                <p><strong>WhatsApp: </strong>
                    <a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span>
                    </a>
                </p>
            </div>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7315.575067807578!2d-46.573089!3d-23.540143!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5ee9ec42b8eb%3A0x3483a04e69c8bc09!2sR.%20Visc.%20de%20Itabora%C3%AD%2C%2075%20-%20Vila%20Azevedo%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2003308-050!5e0!3m2!1spt-BR!2sbr!4v1606153225945!5m2!1spt-BR!2sbr"></iframe>
    </main>
    
    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "tools/jquery.fancybox",
        "jquery.padrao.contact"
    )); ?>
    
    <a id="trigger-mensagem-fancybox" href="#modal-mensagem-fancybox" style="display:none"></a>
    <div id="modal-mensagem-fancybox" class="text-center"></div>
    <?php if($captcha){ ?>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <?php } ?>
    <script>
        $(function(){
            var form_identification = ".form-contato";
            $(form_identification).validate({
                errorClass: "control-label",
                validClass: "control-label",
                rules: {
                    "data[Contato][nome]" : {
                        required: true
                    },
                    "data[Contato][email]" : {
                        required: true,
                        email: true
                    },
                    "data[Contato][telefone]" : {
                        required: true
                    },
                    "data[Contato][como_conheceu]" : {
                        required: true
                    },
                    "data[Contato][assunto]" : {
                        required: true
                    },
                    "data[Contato][mensagem]" : {
                        required: true
                    }
                },
                highlight: function (element){
                    $(element).parents("div.form-group").addClass("has-error").removeClass("has-success");
                }, 
                unhighlight: function (element){ 
                    $(element).parents("div.form-group").removeClass("has-error").addClass("has-success"); 
                },
                submitHandler: function(form){
                    var dados = $(form_identification).serialize();
                    $.ajax({
                        type: "POST",
                        url: "includes/dispara-email.php",
                        data: dados,
                        dataType: "json",
                        beforeSend: function(){
                            $(".btn-send").html("Aguarde...").attr("disabled", "disabled");
                            $(".form-control").prop("disabled", true);
                        },
                        success: function(data){
                            if(data.status){
                                window.location = "envia-contato";
                            }else{
                                $("#modal-mensagem-fancybox").html(data.message);
                                $("#trigger-mensagem-fancybox").fancybox().trigger("click");
                            }
                        },
                        error: function(data){
                            $("#modal-mensagem-fancybox");
                            $("#trigger-mensagem-fancybox");
                            if(data.status){
                                window.location = "envia-contato";
                            }else{
                                $("#modal-mensagem-fancybox").html(data.message);
                                $("#trigger-mensagem-fancybox").fancybox().trigger("click");
                            }
                        },
                        complete: function(){
                            $(".btn-send").html("Enviar").removeAttr("disabled");
                            $(".form-control").prop("disabled", false);
                        }
                    });
                }
            });
        });
    </script>
</body>
</html>