<?php
$title       = "Loja de fabrica de porta pivotante em Mogi Guaçu";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Por ser uma loja de fábrica de porta pivotante, nossa empresa consegue oferecer portas pivotantes de diversos tamanhos e podendo ser feitas sob medidas. Além disso, fazemos portas pivotante sob medida e oferecemos preços e condições diferenciadas para todos os nossos clientes. As portas pivotantes geralmente são mais largas do que as portas comuns.</p>
<p>Na busca por uma empresa referência, quando o assunto é Fabricante de Porta, a Interporta será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Porta de madeira com passagem para pet, Porta de madeira telescópica, Porta de madeira de parede de alvenaria, Porta de madeira laqueada e Loja de fabrica de porta pivotante, oferece Loja de fabrica de porta pivotante em Mogi Guaçu com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>