<?php
$title       = "Porta de madeira embutida na parede em Elias Fausto";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O sistema de porta embutida permite que você ganhe espaço em ambos os lados da porta, podendo aproveitar o espaço da melhor forma possível, sem se preocupar em abrir a porta para dentro ou fora ou se preocupar se a porta vai bater em algum móvel. Porta de madeira embutida na parede em Elias Fausto é com a Interporta, a melhor fabricante de portas de correr embutidas do Brasil.</p>
<p>Além de sermos uma empresa especializada em Porta de madeira embutida na parede em Elias Fausto disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Porta de madeira com passagem para pet, Porta de madeira telescópica, Porta de madeira de parede de drywall, Porta de madeira laqueada e Porta de correr trilho simples. Com a ampla experiência que a equipe Interporta possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Fabricante de Porta.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>