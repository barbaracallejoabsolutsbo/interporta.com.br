<?php
$title       = "Porta de madeira com passagem para pet em Cidade Jardim";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A porta de madeira com passagem pet é feita para que o seu pet, seja ele um cachorro ou gato, possa entrar e sair do cômodo sem maiores dificuldades. Essa porta promove a interação dos ambientes para o pet, fazendo com que o animal que a sua família tanto goste tenha mais liberdade e facilidade para passar da área interna para a área externa. Entre em contato com a nossa empresa e saiba mais sobre nossos produtos.</p>
<p>Você procura por Porta de madeira com passagem para pet em Cidade Jardim? Contar com empresas especializadas no segmento de Fabricante de Porta é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Interporta é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Porta de madeira com aplicação de vidro, Loja de fabrica de porta roldana aparente, Porta de madeira de roldana aparente, Loja de fabrica de porta laqueada e Porta de madeira celeiro laqueada e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>