<?php
$title       = "Porta de madeira laqueada embutida na Vila Galvão - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sempre se atualizando e trazendo as novidades do segmento, oferecemos os melhores serviços e produtos como Porta de madeira laqueada embutida na Vila Galvão - Guarulhos com materiais de primeira linha e profissionais competentes e atualizados que buscam a máxima qualidade dos nossos serviços e dos nossos produtos. Entre em contato com a gente e saiba mais sobre nossos produtos e serviços!</p>
<p>Atuando de forma a cumprir os padrões de qualidade do mercado de Fabricante de Porta, a Interporta é uma empresa experiente quando se trata do ramo de Porta de madeira laqueada pivotante, Porta de madeira com friso, Porta de embutir sistema duplo linear ou 45 graus, Porta de madeira laqueada roldana aparente e Loja de fabrica de porta pivotante. Por isso, se você busca o melhor com um custo acessível e vantajoso em Porta de madeira laqueada embutida na Vila Galvão - Guarulhos aqui você encontra o que precisa sem a diminuição da qualidade. Busque sempre o melhor para ter uma real satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>