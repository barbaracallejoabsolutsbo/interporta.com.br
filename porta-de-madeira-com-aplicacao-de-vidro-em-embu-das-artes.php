<?php
$title       = "Porta de madeira com aplicação de vidro em Embu das Artes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Porta de madeira com aplicação de vidro em Embu das Artes foi criada a partir dos boxes dos banheiros, lavanderias, jardim, e outros ambientes que poderiam ser molhados ou enfrentar mudanças do tempo ou temperaturas. Com o passar dos anos e com a melhoria da tecnologia, foi criado Porta de madeira com aplicação de vidro em Embu das Artes para todos os tipos de ambientes com diversos modelos e soluções.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Porta de madeira com aplicação de vidro em Embu das Artes? A Interporta é o que você precisa! Sendo uma das principais empresas do ramo de Fabricante de Porta consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Porta de madeira personalizada, Porta de madeira embutida na lateral da parede, Porta de vidro fosco, Loja de fabrica de porta celeiro e Porta de embutir sistema duplo linear ou 45 graus. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>