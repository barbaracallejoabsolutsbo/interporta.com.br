<?php
$title       = "Porta de madeira embutida na parede em Monte Mor";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você esta procurando por portas de madeira embutida, a Interporta é a melhor solução para você.  Somos uma empresa especializada em portas embutidas. Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de Porta de madeira embutida na parede em Monte Mor com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.</p>
<p>Como uma empresa de confiança no mercado de Fabricante de Porta, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Porta de madeira embutida na parede em Monte Mor. A Interporta vem crescendo e mostrando seu potencial através de Troca de ferragens de porta, Porta de madeira com friso, Porta de madeira celeiro, Porta de embutir sistema duplo linear ou 45 graus e Porta de madeira embutida na lateral da parede, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>