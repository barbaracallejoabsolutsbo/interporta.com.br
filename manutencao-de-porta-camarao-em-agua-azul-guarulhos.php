<?php
$title       = "Manutenção de porta camarão em Água Azul - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A porta camarão, que também é conhecida como porta articulada, permite abertura total ou parcial da porta, dependendo da necessidade do usuário. Com os mesmos efeitos de uma porta de correr, ela possui um funcionamento diferente quando o é relacionado às folhas da porta. Caso tenha dificuldade em abrir a porta, realizamos a Manutenção de porta camarão em Água Azul - Guarulhos.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Manutenção de porta camarão em Água Azul - Guarulhos? A Interporta é o que você precisa! Sendo uma das principais empresas do ramo de Fabricante de Porta consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Porta de madeira bandô, Loja de fabrica de porta laqueada, Manutenção de porta com roldana, Porta de madeira de correr e Porta de madeira com boiserie. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>