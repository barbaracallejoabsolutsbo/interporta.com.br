<?php
$title       = "Porta de madeira laqueada embutida em Riviera de São Lourenço";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante e com materiais de primeira linha. Esse modelo de Porta de madeira laqueada embutida em Riviera de São Lourenço faz muito sucesso entre diversos imóveis, já que há alguns anos ela é tendência no mercado arquitetônico. Porta de madeira laqueada embutida em Riviera de São Lourenço é com a gente.</p>
<p>Com credibilidade no mercado de Fabricante de Porta, a Interporta trabalha dia a dia com foco em proporcionar com qualidade, viabilidade e custo x benefício acessível tanto Manutenção de porta camarão, Porta de madeira pivotante, Loja de fabrica de porta celeiro, Porta de madeira laqueada roldana aparente e Manutenção de porta de giro quanto em Porta de madeira laqueada embutida em Riviera de São Lourenço. Por isso, se você busca pelo melhor para você, conte com a nossa equipe de profissionais altamente capacitados, garantindo assim seu sucesso no mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>