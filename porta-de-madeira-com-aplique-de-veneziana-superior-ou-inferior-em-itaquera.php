<?php
$title       = "Porta de madeira com aplique de veneziana superior ou inferior em Itaquera";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A porta de madeira com veneziana superior ou inferior busca um design diferenciado proporcionando segurança para a casa. A Interporta trabalha com Porta de madeira com aplique de veneziana superior ou inferior em Itaquera feitas sob medidas e com entrega para todo o Brasil!</p>
<p>Desempenhando uma das melhores assessorias do segmento de Fabricante de Porta, a Interporta se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Porta de madeira com aplique de veneziana superior ou inferior em Itaquera com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Manutenção de porta pivotante, Porta de madeira laqueada roldana aparente, Porta de madeira laqueada, Porta de embutir sistema duplo linear ou 45 graus e Porta de madeira com aplique de veneziana superior ou inferior, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>