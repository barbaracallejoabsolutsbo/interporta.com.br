<?php

    $title       = "Vedação";
    $description = "Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "galeria-fotos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="faixa-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <h1><?php echo $h1; ?></h1>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <div class="text-right">
                            <?php echo $padrao->breadcrumb(array($title)); ?>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo">
            <div class="container">
                <p>Confira nossos produtos: Vedação</p>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/1.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/1-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/2.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/2-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/3.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/3-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/4.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/4-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/5.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/5-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/6.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/6-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/7.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/7-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/vedacao/8.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/vedacao/8-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox"
    )); ?>
    
</body>
</html>