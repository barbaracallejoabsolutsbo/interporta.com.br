<ul>
	<li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
	<li><a href="<?php echo $url; ?>empresa" title="Quem Somos">Quem Somos</a></li>
	<li><a href="<?php echo $url; ?>#" title="Portas">Portas</a>
		<ul class="sub-menu">
			<li><a href="<?php echo $url; ?>portas-embutidas" title="Portas Embutidas">Portas Embutidas</a></li>
			<li><a href="<?php echo $url; ?>portas-de-correr" title="Portas de Correr">Portas de Correr</a></li>
			<li><a href="<?php echo $url; ?>portas-de-vidro" title="Portas de Vidro">Portas de Vidro</a></li>
			<li><a href="<?php echo $url; ?>portas-pivotantes" title="Portas Pivotantes">Portas Pivotantes</a></li>
			<li><a href="<?php echo $url; ?>portas-revestidas" title="Portas Revestidas">Portas Revestidas</a></li>
			<li><a href="<?php echo $url; ?>portas-bando" title="Portas Bandô">Portas Bandô</a></li>
			<li><a href="<?php echo $url; ?>roldana-aparente" title="Roldana Aparente">Roldana Aparente</a></li>
			<li><a href="<?php echo $url; ?>telescopicas" title="Telescópicas">Telescópicas</a></li>
		</ul>
	</li>
	<li><a href="<?php echo $url; ?>#" title="Portas">Portas</a>
		<ul class="sub-menu">
			<li><a href="<?php echo $url; ?>vedacao" title="Vedação">Vedação</a></li>
			<li><a href="<?php echo $url; ?>fechaduras" title="Fechaduras">Fechaduras</a></li>
			<li><a href="<?php echo $url; ?>puxadores" title="Puxadores">Puxadores</a></li>
		</ul>
	</li>
	<li><a href="<?php echo $url; ?>paineis-decorativos" title="Painéis Decorativos">Painéis Decorativos</a></li>
	<li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a>
		<ul  class="sub-menu">
			<?php echo $padrao->subMenu($palavras_chave); ?>
		</ul>
	</li>
	<li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
</ul>