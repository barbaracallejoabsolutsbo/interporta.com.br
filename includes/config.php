<?php

    // Principais Dados do Cliente
    $nome_empresa = "Interporta";
    $emailContato = "vendas@interporta.com.br";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => "Interporta",
            "rua" => "Visc. de Itaboraí, 75",
            "bairro" => "Tatuapé",
            "cidade" => "São Paulo",
            "estado" => "São Paulo",
            "uf" => "SP",
            "cep" => "03308-050",
            "latitude_longitude" => "-23.539114715386052, -46.57311915562581", // Consultar no maps.google.com
            "ddd" => "11",
            "telefone" => "3427-8000",
            "telefone2" => "3926-2900",
            "whatsapp" => "97701-0329",
            "whatsapp-link" => "https://api.whatsapp.com/send?phone=5511977010329&text=Ol%C3%A1%2C%20achei%20sua%20empresa%20no%20Google.%20Desejo%20mais%20informa%C3%A7%C3%B5es",
            "link_maps" => "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14631.131033313604!2d-46.57320498630868!3d-23.54031470973517!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5ee9ec42b8eb%3A0x3483a04e69c8bc09!2sR.%20Visc.%20de%20Itabora%C3%AD%2C%2075%20-%20Vila%20Azevedo%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2003308-050!5e0!3m2!1spt-BR!2sbr!4v1633868399619!5m2!1spt-BR!2sbr" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/interporta.com.br/",
        // URL online
        "https://www.interporta.com.br/"
));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
    
    // Parâmetros para Formulário de Contato
    // $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    // $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    // $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
     $smtp_contato            = "162.241.2.49";
     $email_remetente         = "dispara-email@absolutsbo.com.br";
     $senha_remetente         = "DisparaAbsolut123?";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Porta de Madeira Embutida na Parede",
        "Porta de Madeira de Correr",
        "Porta de Madeira de Parede de Drywall",
        "Porta de Madeira de Parede de Alvenaria",
        "Porta de Madeira Camarão",
        "Porta de Madeira Pivotante",
        "Porta de Madeira Convencional",
        "Porta de Madeira Telescopica",
        "Porta de Madeira de Roldana Aparente",
        "Porta de Madeira com Aplicação de Vidro",
        "Porta de Madeira com Espelho",
        "Porta de Madeira Laqueada",
        "Porta de Madeira Laqueada Embutida",
        "Porta de Madeira Laqueada Pivotante",
        "Porta de Madeira Laqueada Roldana Aparente",
        "Porta de Madeira Embutida na Lateral da Parede",
        "Porta de Madeira Celeiro",
        "Porta de Madeira Celeiro Laqueada",
        "Porta de Madeira com Friso",
        "Porta de Vidro",
        "Porta de Vidro Fosco",
        "Porta de Madeira Personalizada",
        "Porta de Madeira com Boiserie",
        "Manutenção de Porta com Roldana",
        "Manutenção de Porta Embutida",
        "Manutenção de Porta Pivotante",
        "Manutenção de Porta Camarão",
        "Manutenção de Porta de Giro",
        "Troca de Folha de Porta",
        "Troca de Ferragens de Porta",
        "Loja de Fabrica de Porta Laqueada",
        "Loja de Fabrica de Porta Pivotante",
        "Loja de Fabrica de Porta Embutida",
        "Loja de Fabrica de Porta Celeiro",
        "Loja de Fabrica de Porta Roldana Aparente",
        "Porta de Embutir Sistema Duplo Linear ou 45 Graus",
        "Porta de Madeira com Aplique de Veneziana Superior ou Inferior",
        "Porta de Madeira com Passagem para Pet",
        "Porta de Madeira Bandô",
        "Porta de Correr Trilho Simples"
    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */