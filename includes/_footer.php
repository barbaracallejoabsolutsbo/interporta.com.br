<footer>
    <?php include "includes/btn-fixos.php"; ?>
    <div class="container">
        <h2><span>Interporta</span></h2>
        <div class="row">
            <div class="col-md-4">
                <h3>Sobre Nós</h3>
                <p>A Interporta é uma loja especializada em portas de correr embutidas na parede. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. Com garantia de 15 anos do sistema deslizante.</p>
            </div>
            <div class="col-md-2">
                <h3>Institucional</h3>
                <ul>
                    <li><a href="<?php echo $url; ?>empresa" title="Empresa">Quem Somos</a></li>
                    <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                    <li><a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">Mapa do Site</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h3>Entre em Contato</h3>
                <ul>
                    <li><a title="E-mail" href="mailto:<?php echo $emailContato; ?>">
                        <i class="far fa-envelope"></i><span itemprop="telephone"> <?php echo $emailContato; ?></span></a>
                    </li>
<!--                     <li><a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>"><i class="fas fa-phone-alt"></i><span itemprop="telephone"> <?php echo $unidades[1]["ddd"]; ?> <?php echo $unidades[1]["telefone"]; ?></span>
                        </a>
                        |
                        <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone2"]; ?>"><span itemprop="telephone"> <?php echo $unidades[1]["ddd"]; ?><?php echo $unidades[1]["telefone2"]; ?></span>
                        </a>
                    </li> -->
                    <li><a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">
                        <i class="fab fa-whatsapp"></i><span itemprop="telephone"> <?php echo $unidades[1]["ddd"]; ?> <?php echo $unidades[1]["whatsapp"]; ?></span>
                        </a>
                    </li>
                </ul>

            </div>
            <div class="col-md-2">
                <h3>Siga-nos</h3>
                <a href="https://www.facebook.com/interporta.portasembutidas" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i> - @iterporta
                </a>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <p class="desenvolvido">Copyright © 2021 <?php echo $nome_empresa; ?></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="baixa-absolut"></div>
                        <p class="absolutsbo"><a href="https://www.absolutsbo.com.br/" target="_blank" title="Absolut SBO">Desenvolvido por <strong>Absolut SBO</strong></a></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                            <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap"></a>
                        <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul class="menu-footer-mobile">
        <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
        <li><a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span></a></li>
        <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
        <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
    </ul>
</footer>
<?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
    <!-- Código do Analytics aqui! -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172848124-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-172848124-5');
</script>
    <?php } ?>