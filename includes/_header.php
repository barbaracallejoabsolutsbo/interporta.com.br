<header itemscope itemtype="http://schema.org/Organization">
    <div class="topo">
        <div class="container">
            <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
                
            </div>
            <div class="col-xs-2 col-sm-8 col-md-8 col-lg-8">
                <div class="tel">
                    <!-- <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
                        <i class="fas fa-phone-alt"></i><span itemprop="telephone"> <?php echo $unidades[1]["ddd"]; ?> <?php echo $unidades[1]["telefone"]; ?></span>
                    </a>
                    |
                    <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone2"]; ?>"><span itemprop="telephone"> <?php echo $unidades[1]["ddd"]; ?><?php echo $unidades[1]["telefone2"]; ?></span>
                    </a> 
                    | -->
                    <a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">
                        <i class="fab fa-whatsapp"></i><span itemprop="telephone"> <?php echo $unidades[1]["ddd"]; ?> <?php echo $unidades[1]["whatsapp"]; ?></span>
                    </a>
                    |
                    <a title="E-mail" href="mailto:<?php echo $emailContato; ?>">
                        <i class="far fa-envelope"></i><span itemprop="telephone"> <?php echo $emailContato; ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>  

    <div class="container header-container-main">
        <div class="logo-info-contato">
            <div class="row">
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <div class="logo">
                        <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                            <span itemprop="image">
                                <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-9 col-md-9 col-lg-9">
                    <nav class="menu">
                        <ul class="menu-list">
                            <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                            <li><a href="<?php echo $url; ?>empresa" title="Quem Somos">Quem Somos</a></li>
                            <li><a href="<?php echo $url; ?>#" title="Portas">Portas</a>
                                <ul class="sub-menu">
                                   <li><a href="<?php echo $url; ?>portas-embutidas" title="Portas Embutidas">Portas Embutidas</a></li>
                                   <li><a href="<?php echo $url; ?>portas-de-correr" title="Portas de Correr">Portas de Correr</a></li>
                                   <li><a href="<?php echo $url; ?>portas-de-vidro" title="Portas de Vidro">Portas de Vidro</a></li>
                                   <li><a href="<?php echo $url; ?>portas-pivotantes" title="Portas Pivotantes">Portas Pivotantes</a></li>
                                   <li><a href="<?php echo $url; ?>portas-revestidas" title="Portas Revestidas">Portas Revestidas</a></li>
                                   <li><a href="<?php echo $url; ?>portas-bando" title="Portas Bandô">Portas Bandô</a></li>
                                   <li><a href="<?php echo $url; ?>roldana-aparente" title="Roldana Aparente">Roldana Aparente</a></li>
                                   <li><a href="<?php echo $url; ?>telescopicas" title="Telescópicas">Telescópicas</a></li>
                                </ul>
                            <li><a href="<?php echo $url; ?>#" title="Portas">Portas</a>
                                <ul class="sub-menu">
                                   <li><a href="<?php echo $url; ?>vedacao" title="Vedação">Vedação</a></li>
                                   <li><a href="<?php echo $url; ?>fechaduras" title="Fechaduras">Fechaduras</a></li>
                                   <li><a href="<?php echo $url; ?>puxadores" title="Puxadores">Puxadores</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo $url; ?>paineis-decorativos" title="Painéis Decorativos">Painéis Decorativos</a></li>
                            <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                            <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>