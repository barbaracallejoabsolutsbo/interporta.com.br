<?php
$title       = "Porta de madeira embutida na parede em Itaquera";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O sistema de porta embutida permite que você ganhe espaço em ambos os lados da porta, podendo aproveitar o espaço da melhor forma possível, sem se preocupar em abrir a porta para dentro ou fora ou se preocupar se a porta vai bater em algum móvel. Porta de madeira embutida na parede em Itaquera é com a Interporta, a melhor fabricante de portas de correr embutidas do Brasil.</p>
<p>Como uma especialista em Fabricante de Porta, a Interporta se destaca dentre as demais empresas quando o assunto é Porta de madeira embutida na parede em Itaquera, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Porta de madeira com passagem para pet, Manutenção de porta de giro, Porta de madeira camarão, Troca de ferragens de porta e Manutenção de porta embutida. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>