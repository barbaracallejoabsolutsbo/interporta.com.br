<?php
$seo = array();

/*
 * Outras páginas
 * -------------------------------------- */
$seo['paginasSite'] = array(
	'/' => array(
		'title' => 'Portas de Correr na Parede',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Home',
		),
	'/informacoes' => array(
		'title' => 'Informações',
		'description' => 'Em nosso página de informações você encontra diversos produtos e serviços, confira nossas categorias e informações de nossos produtos e serviços.',
		'keywords' => 'Informações',
		),
	'/mapa-site' => array(
		'title' => 'Mapa do Site',
		'description' => 'Em nosso mapa do site você encontra todas as páginas do site, confira nossas categorias e informações de nossos produtos e serviços.',
		'keywords' => 'Mapa do Site',
		),
	'/contato' => array(
		'title' => 'Fale Conosco',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Contato',
		),
	'/quem-somos' => array(
		'title' => 'Quem Somos',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Quem Somos',
		),
	'/portas-embutidas' => array(
		'title' => 'Portas Embutidas',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Embutidas',
		),
	'/portas-de-correr' => array(
		'title' => 'Portas de Correr',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas de Correr',
		),
	'/portas-de-vidro' => array(
		'title' => 'Portas de Vidro',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas de Vidro',
		),
	'/portas-pivotantes' => array(
		'title' => 'Portas Pivotantes',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Pivotantes',
		),
	'/portas-revestidas' => array(
		'title' => 'Portas Revestidas',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Revestidas',
		),
	'/portas-bando' => array(
		'title' => 'Portas Bandô',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Bandô',
		),
	'/roldana-aparente' => array(
		'title' => 'Roldana Aparente',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Roldana Aparente',
		),
	'/telescopicas' => array(
		'title' => 'Telescópicas',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Telescópicas',
		),
	'/vedacao' => array(
		'title' => 'Vedação',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Vedação',
		),
	'/fechaduras' => array(
		'title' => 'Fechaduras',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Fechaduras',
		),
	'/puxadores' => array(
		'title' => 'Puxadores',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Puxadores',
		),
	'/paineis-decorativos' => array(
		'title' => 'Painéis Decorativos',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Painéis Decorativos',
		),
	);

/*
 * Páginas Portas
 * -------------------------------------- */
$seo['paginasPort'] = array(
	'/portas-embutidas' => array(
		'title' => 'Portas Embutidas',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Embutidas',
		),
	'/portas-de-correr' => array(
		'title' => 'Portas de Correr',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas de Correr',
		),
	'/portas-de-vidro' => array(
		'title' => 'Portas de Vidro',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas de Vidro',
		),
	'/portas-pivotantes' => array(
		'title' => 'Portas Pivotantes',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Pivotantes',
		),
	'/portas-revestidas' => array(
		'title' => 'Portas Revestidas',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Revestidas',
		),
	'/portas-bando' => array(
		'title' => 'Portas Bandô',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Portas Bandô',
		),
	'/roldana-aparente' => array(
		'title' => 'Roldana Aparente',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Roldana Aparente',
		),
	'/telescopicas' => array(
		'title' => 'Telescópicas',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Telescópicas',
		),
	);
/*
 * Páginas Portas
 * -------------------------------------- */
$seo['paginasAcess'] = array(
	'/vedacao' => array(
		'title' => 'Vedação',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Vedação',
		),
	'/fechaduras' => array(
		'title' => 'Fechaduras',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Fechaduras',
		),
	'/puxadores' => array(
		'title' => 'Puxadores',
		'description' => 'Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco.',
		'keywords' => 'Puxadores',
		),
	);
/*
 * Páginas de SEO
 * -------------------------------------- */
$seo['paginasSeo'] = array(
	'/porta-de-madeira-embutida-na-parede' => array(
		'title' => 'PORTA DE MADEIRA EMBUTIDA NA PAREDE',
		'description' => 'Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de porta de madeira embutida na parede com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.',
		'keywords' => 'PORTA DE MADEIRA EMBUTIDA NA PAREDE',
		),
	'/porta-de-madeira-de-correr' => array(
		'title' => 'PORTA DE MADEIRA DE CORRER',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo para todos os estados do Brasil porta de madeira de correr.',
		'keywords' => 'PORTA DE MADEIRA DE CORRER',
		),
	'/porta-de-madeira-de-parede-de-drywall' => array(
		'title' => 'PORTA DE MADEIRA DE PAREDE DE DRYWALL',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com porta de madeira de parede de drywall.',
		'keywords' => 'PORTA DE MADEIRA DE PAREDE DE DRYWALL',
		),
	'/porta-de-madeira-de-parede-de-alvenaria' => array(
		'title' => 'PORTA DE MADEIRA DE PAREDE DE ALVENARIA',
		'description' => 'A Interporta é uma loja especializada em portas de correr embutidas na parede, com garantia de quinze anos do sistema deslizante. Porta de madeira de parede de alvenaria é com a gente.',
		'keywords' => 'PORTA DE MADEIRA DE PAREDE DE ALVENARIA',
		),
	'/porta-de-madeira-camarao' => array(
		'title' => 'PORTA DE MADEIRA CAMARÃO',
		'description' => 'Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de porta de madeira camarão com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.',
		'keywords' => 'PORTA DE MADEIRA CAMARÃO',
		),
	'/porta-de-madeira-pivotante' => array(
		'title' => 'PORTA DE MADEIRA PIVOTANTE',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de porta de madeira pivotante e outros serviços. ',
		'keywords' => 'PORTA DE MADEIRA PIVOTANTE',
		),
	'/porta-de-madeira-convencional' => array(
		'title' => 'PORTA DE MADEIRA CONVENCIONAL',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo para todos os estados do Brasil porta de madeira convencional.',
		'keywords' => 'PORTA DE MADEIRA CONVENCIONAL',
		),
	'/porta-de-madeira-telescopica' => array(
		'title' => 'PORTA DE MADEIRA TELESCOPICA',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com porta de madeira telescópica.',
		'keywords' => 'PORTA DE MADEIRA TELESCOPICA',
		),
	'/porta-de-madeira-de-roldana-aparente' => array(
		'title' => 'PORTA DE MADEIRA DE ROLDANA APARENTE',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Porta de madeira de parede de alvenaria é com a gente.',
		'keywords' => 'PORTA DE MADEIRA DE ROLDANA APARENTE',
		),
	'/porta-de-madeira-com-aplicacao-de-vidro' => array(
		'title' => 'PORTA DE MADEIRA COM APLICAÇÃO DE VIDRO',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de porta de madeira com aplicação de vidro e outros serviços.',
		'keywords' => 'PORTA DE MADEIRA COM APLICAÇÃO DE VIDRO',
		),
	'/porta-de-madeira-com-espelho' => array(
		'title' => 'PORTA DE MADEIRA COM ESPELHO',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo para todos os estados do Brasil porta de madeira com espelho.',
		'keywords' => 'PORTA DE MADEIRA COM ESPELHO',
		),
	'/porta-de-madeira-laqueada' => array(
		'title' => 'PORTA DE MADEIRA LAQUEADA',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com porta de madeira laqueada.',
		'keywords' => 'PORTA DE MADEIRA LAQUEADA',
		),
	'/porta-de-madeira-laqueada-embutida' => array(
		'title' => 'PORTA DE MADEIRA LAQUEADA EMBUTIDA',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Porta de madeira laqueada embutida é com a gente.',
		'keywords' => 'PORTA DE MADEIRA LAQUEADA EMBUTIDA',
		),
	'/porta-de-madeira-laqueada-pivotante' => array(
		'title' => 'PORTA DE MADEIRA LAQUEADA PIVOTANTE',
		'description' => 'Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de porta de madeira laqueada pivotante com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.',
		'keywords' => 'PORTA DE MADEIRA LAQUEADA PIVOTANTE',
		),
	'/porta-de-madeira-laqueada-roldana-aparente' => array(
		'title' => 'PORTA DE MADEIRA LAQUEADA ROLDANA APARENTE',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de porta de madeira laqueada roldana aparente e outros serviços.',
		'keywords' => 'PORTA DE MADEIRA LAQUEADA ROLDANA APARENTE',
		),
	'/porta-de-madeira-embutida-na-lateral-da-parede' => array(
		'title' => 'PORTA DE MADEIRA EMBUTIDA NA LATERAL DA PAREDE',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de porta de madeira embutida na lateral da parede e outros serviços.',
		'keywords' => 'PORTA DE MADEIRA EMBUTIDA NA LATERAL DA PAREDE',
		),
	'/porta-de-madeira-celeiro' => array(
		'title' => 'PORTA DE MADEIRA CELEIRO',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo para todos os estados do Brasil porta de madeira celeiro.',
		'keywords' => 'PORTA DE MADEIRA CELEIRO',
		),
	'/porta-de-madeira-de-celeiro-laqueada' => array(
		'title' => 'PORTA DE MADEIRA CELEIRO LAQUEADA',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com porta de madeira de celeiro laqueada.',
		'keywords' => 'PORTA DE MADEIRA CELEIRO LAQUEADA',
		),
	'/porta-de-madeira-com-friso' => array(
		'title' => 'PORTA DE MADEIRA COM FRISO',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Porta de madeira com friso é com a gente.',
		'keywords' => 'PORTA DE MADEIRA COM FRISO',
		),
	'/porta-de-vidro' => array(
		'title' => 'PORTA DE VIDRO',
		'description' => 'Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de porta de vidro com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.',
		'keywords' => 'PORTA DE VIDRO',
		),
	'/porta-de-vidro-fosco' => array(
		'title' => 'PORTA DE VIDRO FOSCO',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de porta de vidro fosco e outros serviços.',
		'keywords' => 'PORTA DE VIDRO FOSCO',
		),
	'/porta-de-madeira-personalizada' => array(
		'title' => 'PORTA DE MADEIRA PERSONALIZADA',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo para todos os estados porta de madeira personalizada.',
		'keywords' => 'PORTA DE MADEIRA PERSONALIZADA',
		),
	'/porta-de-madeira-com-boiserie' => array(
		'title' => 'PORTA DE MADEIRA COM BOISERIE',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com porta de madeira com boiserie.',
		'keywords' => 'PORTA DE MADEIRA COM BOISERIE',
		),
	'/manutencao-de-porta-com-roldana' => array(
		'title' => 'MANUTENÇÃO DE PORTA COM ROLDANA',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Manutenção de porta com roldana é com a gente.',
		'keywords' => 'MANUTENÇÃO DE PORTA COM ROLDANA',
		),
	'/manutencao-de-porta-embutida' => array(
		'title' => 'MANUTENÇÃO DE PORTA EMBUTIDA',
		'description' => 'Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de manutenção de porta embutida com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.',
		'keywords' => 'MANUTENÇÃO DE PORTA EMBUTIDA',
		),
	'/manutencao-de-porta-pivotante' => array(
		'title' => 'MANUTENÇÃO DE PORTA PIVOTANTE',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de manutenção de porta pivotante e outros serviços. ',
		'keywords' => 'MANUTENÇÃO DE PORTA PIVOTANTE',
		),
	'/manutencao-de-porta-camarao' => array(
		'title' => 'MANUTENÇÃO DE PORTA CAMARÃO',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo serviços de manutenção de porta camarão em todo os estado..',
		'keywords' => 'MANUTENÇÃO DE PORTA CAMARÃO',
		),
	'/manutencao-de-pora-de-giro' => array(
		'title' => 'MANUTENÇÃO DE PORTA DE GIRO',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com manutenção de porá de giro.',
		'keywords' => 'MANUTENÇÃO DE PORTA DE GIRO',
		),
	'/troca-de-folha-de-porta' => array(
		'title' => 'TROCA DE FOLHA DE PORTA',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Troca de folha de porta é com a gente.',
		'keywords' => 'TROCA DE FOLHA DE PORTA',
		),
	'/troca-de-ferragens-de-porta' => array(
		'title' => 'TROCA DE FERRAGENS DE PORTA',
		'description' => 'Sempre trazendo as novidades do segmento, oferecemos os melhores serviços de troca de ferragens de porta com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.',
		'keywords' => 'TROCA DE FERRAGENS DE PORTA',
		),
	'/loja-de-fabrica-de-porta-laqueada' => array(
		'title' => 'LOJA DE FABRICA DE PORTA LAQUEADA',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de loja de fábrica de porta laqueada e outros serviços.',
		'keywords' => 'LOJA DE FABRICA DE PORTA LAQUEADA',
		),
	'/loja-de-fabrica-de-porta-pivotante' => array(
		'title' => 'LOJA DE FABRICA DE PORTA PIVOTANTE',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo serviços e produtos da nossa loja de fábrica de porta pivotante.',
		'keywords' => 'LOJA DE FABRICA DE PORTA PIVOTANTE',
		),
	'/loja-de-fabrica-de-porta-embutida' => array(
		'title' => 'LOJA DE FABRICA DE PORTA EMBUTIDA',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta é uma loja de fábrica de porta embutida.',
		'keywords' => 'LOJA DE FABRICA DE PORTA EMBUTIDA',
		),
	'/loja-de-fabrica-de-porta-celeiro' => array(
		'title' => 'LOJA DE FABRICA DE PORTA CELEIRO',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Loja de fábrica de porta celeiro é com a gente.',
		'keywords' => 'LOJA DE FABRICA DE PORTA CELEIRO',
		),
	'/loja-de-fabrica-de-porta-roldana-aparente' => array(
		'title' => 'LOJA DE FABRICA DE PORTA ROLDANA APARENTE',
		'description' => 'Sempre trazendo as novidades do segmento, somos uma loja de fábrica de porta roldana aparente com profissionais competentes e atualizados que buscam a máxima qualidade dos nossos produtos.',
		'keywords' => 'LOJA DE FABRICA DE PORTA ROLDANA APARENTE',
		),
	'/porta-de-embutir-sistema-duplo-linear-ou-45-graus' => array(
		'title' => 'PORTA DE EMBUTIR SISTEMA DUPLO LINEAR OU 45 GRAUS',
		'description' => 'Nossos sistemas são extremamente confiáveis. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de porta de embutir sistema duplo linear ou 45 graus e outros serviços. ',
		'keywords' => 'PORTA DE EMBUTIR SISTEMA DUPLO LINEAR OU 45 GRAUS',
		),
	'/porta-de-madeira-com-aplique-de-veneziana-superior-ou-inferior' => array(
		'title' => 'PORTA DE MADEIRA COM APLIQUE DE VENEZIANA SUPERIOR OU INFERIOR',
		'description' => 'Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com porta de madeira com aplique de veneziana superior ou inferior.',
		'keywords' => 'PORTA DE MADEIRA COM APLIQUE DE VENEZIANA SUPERIOR OU INFERIOR',
		),
	'/porta-de-madeira-com-passagem-pet' => array(
		'title' => 'PORTA DE MADEIRA COM PASSAGEM PARA PET',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Porta de madeira com passagem pet é com a gente.',
		'keywords' => 'PORTA DE MADEIRA COM PASSAGEM PARA PET',
		),
	'/porta-de-madeira-bando' => array(
		'title' => 'PORTA DE MADEIRA BANDÔ',
		'description' => 'A Interporta é uma loja especializada em diversos tipos de portas, com garantia de quinze anos do sistema deslizante. Porta de madeira bando é com a gente.',
		'keywords' => 'PORTA DE MADEIRA BANDÔ',
		),
	'/porta-de-correr-trilho-simples' => array(
		'title' => 'PORTA DE CORRER TRILHO SIMPLES',
		'description' => 'A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo serviços e produtos da nossa loja de porta de correr trilho simples.',
		'keywords' => 'PORTA DE CORRER TRILHO SIMPLES',
		),
	);


/*
 * Head
 * -------------------------------------- */
function seo($pagina) {

	global $seo;
	global $config;

	if(isset($seo['paginasSeo'][$pagina])) {
		$arr = $seo['paginasSeo'][$pagina];
		$imgDestaque = ltrim(rtrim($pagina, '/'), '/').'.jpg';
	} else if(isset($seo['paginasSite'][$pagina])) {
		$arr = $seo['paginasSite'][$pagina];
		$imgDestaque = ltrim(rtrim($pagina, '/'), '/').'.jpg';
	} else {
		$arr = array(
			'title' => 'Página não encontrada...',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam nisi odit, officia iste eos neque.',
			'keywords' => ''
			);
		$imgDestaque = '';
	}

	$title       = $arr['title'];
	$description = $arr['description'];
	$keywords    = $arr['keywords'];
	$urlPagina = ltrim($pagina, '/');

	if(!defined('TITULO')) {
		define('TITULO', $title);
	}

	$retorno = '<!DOCTYPE html>
	<html lang="pt-BR">
	<head>
	<meta charset="UTF-8" />
	<title>'.TITULO.' - '.$config['site']['nomeSite'].'</title>

	<!--
	'.$config['creditos']['topo'].'
	-->

	<link rel="canonical" href="'.URL.$urlPagina.'" />
	<meta name="description" content="'.$description.'" />
	<meta name="keywords" content="'.TITULO.', '.$keywords.'" />
	<meta name="googlebot" content="index, follow" />
	<meta name="robots" content="index, follow" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="'.$config['urls']['imagens'].'favicon.png" type="image/x-icon" />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:region" content="Brasil" />
	<meta property="og:title" content="'.TITULO.' - '.$config['site']['nomeSite'].'" />
	<meta property="og:author" content="'.TITULO.'" />
	<meta property="og:image" content="'.$config['urls']['imagens'].'palavras-chave/'.$imgDestaque.'" />
	<meta property="og:url" content="'.URL.$urlPagina.'" />
	<meta property="og:description" content="'.$description.'" />
	<meta property="og:site_name" content="'.$config['site']['nomeSite'].'" />
	<meta name="twitter:card" content="summary">
	<meta name="twitter:title" content="'.TITULO.'">
	<meta name="twitter:description" content="'.$description.'">
	<meta name="twitter:image" content="'.$config['urls']['imagens'].'palavras-chave/'.$imgDestaque.'">
	<meta name="geo.position" content="'.$config['site']['endereco']['latitude'].';'.$config['site']['endereco']['longitude'].'">
	<meta name="ICBM" content="'.$config['site']['endereco']['latitude'].','.$config['site']['endereco']['longitude'].'" />
	<meta name="geo.placename" content="'.$config['site']['endereco']['placename'].'">
	<meta name="geo.region" content="'.$config['site']['endereco']['region'].'">'."\n";

	return str_replace("\t", '', $retorno);
}
