<?php
$config = array();

$pastaProjeto = '/'; // Barra no começo e no final
$protocolo = 'https';

$config['site'] = array(
	'nomeSite' => 'Interporta',
	'slogan' => 'Slogan da Empresa',
	'telefone' => array(
		'(11) 3427.8000',
		'(11) 3926-2900',
		'(11) 97701-0329',	
		),
	'emailContato' => array(
		'vendas@interporta.com.br',
		),
	'endereco' => array(
		'rua' => 'R. Visc. de Itaboraí, 75',
		'bairro' => 'Vila Azevedo',
		'cidade' => 'São Paulo',
		'uf' => 'SP',
		'cep' => '03308-050',
		'latitude' => '-23.539661',
		'longitude' => '-46.573110',
		'placename' => '',
		'region' => '',
		),
	);

$config['smtp'] = array(
	'Username' => 'admin@mybossdobrasil.com.br',
	'Host' => 'h36.servidorhh.com',
	'Password' => 'g)9Lc&2%HTH4',
'SMTPSecure' => 'tls',
'Port' => 587,
'isSMTP' => true,
'SMTPAuth' => true,
'SMTPDebug' => false,
'Timeout' => 10
);

$config['formulario'] = array(
	'nomeRemetente' => 'Formulário do Site', // Caso não tenha nome do cliente
	'emailRemetente' => 'admin@mybossdobrasil.com.br', // Caso não tenha email do cliente
	'emails' => array(
		'receberEnvios' => array(
			'vendas@interporta.com.br',
			),
		'receberEnviosCopia' => array(
			''
			),
		'receberEnviosCopiaOculta' => array(
			'octavio.augusto.pp@gmail.com'
			),
		),
	);


$config['creditos'] = array(
	'topo' => '
	Desenvolvido por Absolut SBO
	Site: www.absolutsbo.com.br
	Contato: comercial@absolutsbo.com.br
	',
	'nome' => 'Absolut SBO',
	'link' => 'http://www.absolutsbo.com.br',
	);

$config['outros'] = array(
	'cssInline' => true,
	'jsInline' => true,
	);

/*
 * Configurações de URL
 * -------------------------------------- */

$url = $protocolo.'://'.$_SERVER['SERVER_NAME'];
$url = rtrim($url, '/');
$url = $url.$pastaProjeto;
define('URL', $url);

/*
 * Configurações necessárias, não remover
 * -------------------------------------- */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)).DS);

$config['urls'] = array(
	'template' => $url.'template/',
	'css' => $url.'template/css/',
	'js' => $url.'template/js/',
	'imagens' => $url.'template/imagens/',
	);

$config['pastas'] = array(
	'configuracoes' => ROOT.'configuracoes'.DS,
	'template' => ROOT.'template'.DS,
	'css' => ROOT.'template'.DS.'css'.DS,
	'imagens' => ROOT.'template'.DS.'imagens'.DS,
	'js' => ROOT.'template'.DS.'js'.DS,
	'paginas' => ROOT.'template'.DS.'paginas'.DS,
	'partes' => ROOT.'template'.DS.'partes'.DS,
	'seo' => ROOT.'template'.DS.'paginas'.DS.'seo'.DS,
	'site' => ROOT.'template'.DS.'paginas'.DS.'site'.DS,
	);

$pagina = '/'.str_replace($pastaProjeto, '', $_SERVER['REQUEST_URI']);

if(substr($pagina, -1) === '/') {
	$pagina = rtrim($pagina, '/');
}

$pagina = strtok($pagina, '?');
$pagina = strtok($pagina, '#');

if(strlen($pagina) === 0) {
	$pagina .= '/';
}

define('PAGINA', $pagina);
define('PARTE', $config['pastas']['partes']);
