<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('America/Sao_Paulo');
define('ARQUIVO_SEO', true);

if(isset($_POST['acessoExterno'])) {
	$autoload = dirname(__FILE__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

	if(file_exists($autoload)) require $autoload;
	require dirname(__FILE__).DIRECTORY_SEPARATOR.'configuracoes'.DIRECTORY_SEPARATOR.'configuracoes.php';
	require dirname(__FILE__).DS.'configuracoes'.DS.'funcoes.php';

	if(isset($_POST['checarArquivo'])) {
		exit(json_encode(array('success' => true, 'msg' => 'Arquivo verificado com sucesso')));
	}

	if(isset($_POST['acao'])) {

		if($_POST['acao'] == 'inserir-conteudo') {
			if(file_exists($config['pastas']['configuracoes'].'seo.php')) {
				try {
					$arquivo = fopen($config['pastas']['configuracoes'].'seo.php', 'w');
					fwrite($arquivo, $_POST['conteudoArquivo']);
					fclose($arquivo);
					exit(json_encode(array('success' => true, 'msg' => 'Arquivo alterado com sucesso!')));
				} catch (Exception $e) {
					exit(json_encode(array('success' => false, 'msg' => '<strong>Erro:</strong> '.$e)));
				}
			} else {
				exit(json_encode(array('success' => false, 'msg' => 'O arquivo <strong>'.$arquivo.'</strong> não existe.')));
			}
		}

		if($_POST['acao'] == 'apagar-paginas-seo' || $_POST['acao'] == 'apagar-paginas-site' || $_POST['acao'] == 'apagar-paginas-seo-e-site') {
			$mensagem = '';
			if($_POST['acao'] == 'apagar-paginas-seo' || $_POST['acao'] == 'apagar-paginas-seo-e-site') {
				try {
					array_map('unlink', glob($config['pastas']['seo'].'*.*'));
					if(is_dir($config['pastas']['seo'])) {
						rmdir($config['pastas']['seo']);
						$mensagem .= '<div><strong>Apagado:</strong> '.$config['pastas']['seo'].'</div>';
					} else {
						$mensagem .= '<div><strong>Diretório não existe:</strong> '.$config['pastas']['seo'].'</div>';
					}
				} catch (Exception $e) {
					$mensagem .= '<div><strong>Erro:</strong> '.$config['pastas']['seo'].' ('.$e.')</div>';
				}
			}
			if($_POST['acao'] == 'apagar-paginas-site' || $_POST['acao'] == 'apagar-paginas-seo-e-site') {
				try {
					array_map('unlink', glob($config['pastas']['site'].'*.*'));
					if(is_dir($config['pastas']['site'])) {
						rmdir($config['pastas']['site']);
						$mensagem .= '<div><strong>Apagado:</strong> '.$config['pastas']['site'].'</div>';
					} else {
						$mensagem .= '<div><strong>Diretório não existe:</strong> '.$config['pastas']['seo'].'</div>';
					}
				} catch (Exception $e) {
					$mensagem .= '<div><strong>Erro:</strong> '.$config['pastas']['site'].' ('.$e.')</div>';
				}
			}
			exit(json_encode(array('success' => true, 'msg' => $mensagem)));
		}

	}

	exit(json_encode(array('success' => true, 'msg' => 'Nenhuma ação informada...')));
}
