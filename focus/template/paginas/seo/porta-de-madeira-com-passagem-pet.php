<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-madeira-com-passagem-pet.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-madeira-com-passagem-pet.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por porta de madeira com passagem pet para sua residência?</h2>
							<p>A Interporta é a melhor opção quando falamos em <strong>porta de madeira com passagem pet</strong>!</p>
							<p>A <strong>porta de madeira com passagem pet</strong> é uma porta de madeira com uma pequena porta vai e vem embaixo que permite que seu pet entre e saia do ambiente quando ele quiser.</p>
							<p>A <strong>porta de madeira com passagem pet</strong> permite que o pet circule sem impedimento pelos ambientes internos ou externos da casa. Essa <strong>porta de madeira com passagem pet</strong> deixa o seu pet mais independente, dando liberdade pra ele pode sair ou voltar do quintal para brincar, usar caixa sanitária ou tapete higiênico em outros cômodos, além de várias outras vantagens.</p>
							<p>A porta de madeira promove um ótimo isolamento térmico e acústico, deixando do lado de fora o barulho e o frio. Além disso, a porta de madeira é bem resistente, protegendo mais contra arrombamentos e contra fatores climáticos.</p>
							<p>Com mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços, nossos sistemas são extremamente confiáveis. Além disso, temos garantia de 15 anos do sistema deslizante.</p>
							<p>A <strong>porta de madeira com passagem pet</strong> pode ser usada tanto por gatos quanto por cães de pequeno porte.</p>
							<p>Existem quatro formas de acesso que você pode escolher para o seu pet utilizando a <strong>porta de madeira com passagem pet</strong>, que seria quando a porta esta totalmente liberada, totalmente fechada, liberada somente para entrada ou liberada somente para saída.</p>
							<p>Outro ponto positivo da <strong>porta de madeira com passagem pet</strong> é que o seu pet não vai arranhar ou danificar a porta, já que o animal não precisa fazer força para entrar ou sair pela passagem pet.</p>
							<p>A Interporta possui um parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.</p>
							<p>Entre em contato com a gente por telefone ou e-mail e faça um orçamento.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>