<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-madeira-com-friso.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-madeira-com-friso.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por porta de madeira com friso para sua residência?</h2>
							<p>A Interporta é a melhor empresa quando falamos em <strong>porta de madeira com friso</strong>!</p>
							<p>Com garantia de 15 anos do sistema deslizante, a Interporta é uma loja especializada em <strong>porta de madeira com friso</strong>. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. </p>
							<p>A <strong>porta de madeira com friso</strong> tem um design único, podendo ser encontrada em diferentes tipos de madeiras. Elas contam com frisos horizontais e verticais que dividem sua porta de forma decorativa.</p>
							<p>Você pode utilizar a <strong>porta de madeira com friso</strong> em apartamentos, banheiros, quartos, entre outros cômodos.</p>
							<p>Essa <strong>porta de madeira com friso</strong> é totalmente sólida e contam com os frisos para fazer um desenho peculiar a cada objeto, podendo mudar de geometria de acordo com o cliente.</p>
							<p>A Interporta permite que você escolha a madeira que será utilizada na sua <strong>porta de madeira com friso</strong>. Independente do tipo de madeira que você escolher, nossa empresa preza pela qualidade, oferecendo produtos que são resistentes, com alta durabilidade e que deixam a estética positiva do ambiente.</p>
							<h3>Porta de madeira com friso para todo o Brasil.</h3>
							<p>A porta de madeira promove um ótimo isolamento térmico e acústico, deixando do lado de fora o barulho e o frio. Além disso, a porta de madeira é bem resistente, protegendo mais contra arrombamentos e contra fatores climáticos.</p>
							<p>Com mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços, nossos sistemas são extremamente confiáveis.</p>
							<p>Produzimos sistemas únicos e exclusivos, totalmente feitos sob medidas, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais, com alturas e larguras especificas, e para portas com medidas e formatos especiais, inclusive com Madeira de Demolição.</p>
							<p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente, portanto qualquer semelhança não será mera coincidência, mas sim cópia falsificada.</p>
							<p>Não perca mais tempo, entre agora em contato com a nossa empresa por telefone ou e-mail e faça um orçamento.</p>
							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>