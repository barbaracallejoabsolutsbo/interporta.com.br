<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/troca-de-ferragens-de-porta.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/troca-de-ferragens-de-porta.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por troca de ferragens de porta para sua residência ou comércio?</h2>
							<p>A Interporta é a melhor empresa quando falamos em <strong>troca de ferragens de porta</strong>.</p>
							<p>As ferragens das portas têm como objetivo garantir a proteção da família e aumentar a vida útil das portas. Com os trincos, você garante o fechamento das portas com mais segurança. </p>
							<p><strong>Troca de ferragens de porta</strong> em São Paulo.</p>
							<p>A instalação do olho mágico é indispensável para portas de entradas e para as pessoas mais prevenidas, já que ele permite identificar quem está do outro lado da porta.</p>
							<p><strong>Troca de ferragens de porta</strong> em Minas Gerais.</p>
							<p>As molas e os amortecedores tem a missão de evitar que as portas fechem rapidamente, evitando fortes impactos e possíveis danos.</p>
							<p><strong>Troca de ferragens de porta</strong> é com a Interporta.</p>
							<p>A porta de correr pode ser de dois tipos: a porta externa à parede e a porta embutida. Os dois modelos têm a vantagem de economizar espaço.</p>
							<p>Esses modelos trazem a vantagem de economizar espaço porque ao ser aberta a porta, ela não usa o espaço da na sua frente, excluindo a necessidade de deixar o espaço próximo a ela desocupado. Além disso, ela tem enorme facilidade em abrir e fechar mesmo se tiver pessoas ou objetos perto dela.</p>
							<p>Situada na cidade de São Paulo, uma das zonas com maior rendimento per capita e desenvolvimento econômico do Brasil, a Interporta opera em todo o território brasileiro. Com garantia de 15 anos do sistema deslizante.</p>
							<p>Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez.</p>
							<p><strong>Troca de ferragens de porta</strong> em todo o Brasil.</p>
							<p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente, portanto qualquer semelhança não será mera coincidência, mas sim cópia falsificada.</p>
							<p>Melhore seu ambiente, valorizando o ambiente com exclusividade e personalidade.</p>
							<p><strong>Troca de ferragens de porta</strong> é com a gente!</p>
							<p>Entre agora em contato com a nossa empresa por telefone ou e-mail e faça um orçamento de <strong>troca de ferragens de porta</strong>.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>