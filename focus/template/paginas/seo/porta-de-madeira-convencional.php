<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-madeira-convencional.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-madeira-convencional.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta precisando de porta de madeira convencional para o seu imóvel?</h2>
							<p>A Interporta é a melhor solução para você que procura por <strong>porta de madeira convencional</strong>.</p>
							<p>A <strong>porta de madeira convencional</strong> é a porta mais utilizada para colocar na entrada da residência. A madeira esta disponível em vários tipos, podendo ser trabalhada e desenhada para diferentes propostas de fachadas, além de possuir um valor estético elevado.</p>
							<p>Para a entrada do imóvel, são recomendadas portas maciças por serem mais seguras e resistentes a impactos e as mudanças climáticas.</p>
							<p>Com o passar do tempo, a <strong>porta de madeira convencional</strong> precisa de manutenção. Por isso, é fundamental escolher uma boa madeira, ou madeiras maciças, que tem boa durabilidade e resistência aos ambientes externos.</p>
							<p>É possível colocar uma <strong>porta de madeira convencional</strong> com desenhos e detalhes, diferenciando o projeto do imóvel. Em um projeto especial o custo dessa porta pode ser elevado.</p>
							<p>A porta de madeira promove um ótimo isolamento térmico e acústico, deixando do lado de fora o barulho e o frio. Além disso, a porta de madeira é bem resistente, protegendo mais contra arrombamentos e contra fatores climáticos.</p>
							<p>Com mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de <strong>porta de madeira convencional</strong> e outros serviços, nossos sistemas são extremamente confiáveis. Além disso, temos garantia de 15 anos do sistema deslizante.</p>
							<p>Produzimos sistemas únicos e exclusivos, totalmente feitos sob medidas, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais para <strong>porta de madeira convencional</strong>, com alturas e larguras especificas, e para portas com medidas e formatos especiais, inclusive com Madeira de Demolição.</p>
							<p><strong>Porta de madeira convencional</strong> para casas, prédios, comércios, escritórios, entre outros.</p>
							<h3>Entregamos nossos produtos para todo o Brasil.</h3>
							<p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente, portanto qualquer semelhança não será mera coincidência, mas sim cópia falsificada.</p>
							<p>Otimize seu espaço, valorizando o ambiente com exclusividade e personalidade.</p>
							Não perca mais tempo, entre agora em contato com a nossa empresa por telefone ou e-mail e faça um orçamento.


							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>