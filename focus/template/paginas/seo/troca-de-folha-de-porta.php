<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/troca-de-folha-de-porta.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/troca-de-folha-de-porta.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por <strong>troca de folha de porta</strong> para sua residência ou comércio?</h2>
							<p>A Interporta é a melhor empresa quando falamos em <strong>troca de folha de porta</strong>.</p>
							<p>A folha de porta se trata da peça individual da porta que vai compor no final a peça por completo. Ela é a folha de madeira, ou apenas a porta de madeira em si.</p>
							<p>Quando vamos comprar uma porta, na maioria das vezes as compramos completas. Isso inclui o batente, guarnição, dobradiça, fechadura e a folha de porta.</p>
							<p>Dificilmente uma pessoa vai comprar apenas a folha de porta e depois todas as demais peças para montar a porta. Por isso, podemos dizer que a compra da folha de porta a parte é necessária quando a <strong>troca de folha de porta</strong> se faz necessária, já que a estragou, mas o resto da porta continua funcionando.</p>
							<ul>
								<li><strong>Troca de folha de porta</strong> para diversos tipos de portas.</li>
								<li><strong>Troca de folha de porta</strong> para São Paulo.</li>
								<li><strong>Troca de folha de porta</strong> para Minas Gerais.</li>
								<li><strong>Troca de folha de porta</strong> para todo o Brasil.</li>
							</ul>
							<p>O sistema de porta embutida permite que você ganhe espaço em ambos os lados da porta, podendo aproveitar o espaço da melhor maneira possível, sem se preocupar em abrir a porta para dentro ou fora, sem se preocupar se a porta vai bater em algum móvel.</p>
							<p>Com mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de <strong>troca de folha de porta</strong> e outros serviços, nossos sistemas são extremamente confiáveis.</p>
							<p>Situada na cidade de São Paulo, uma das zonas com maior rendimento per capita e desenvolvimento econômico do Brasil, a Interporta opera em todo o território brasileiro.</p>
							<p>Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.</p>
							<p>Entre em contato com a gente por e-mail ou telefone e faça um pedido.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>