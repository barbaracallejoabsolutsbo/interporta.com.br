<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-madeira-celeiro.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-madeira-celeiro.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta pesquisando por porta de madeira celeiro?</h2>
							<p>A Interporta é a melhor empresa quando falamos em <strong>porta de madeira celeiro</strong>!</p>
							<p>A cada dia que passa surge novos modelos de portas, com materiais diferenciados, mais modernos e duráveis. A <strong>porta de madeira celeiro</strong> não é uma novidade, mas sempre será uma boa escolha para a sua residência.</p>
							<p>A <strong>porta de madeira celeiro</strong> foi inspirada na rusticidade das portas de celeiro tradicionais encontradas nos campos e sítios, em meio às plantações. Vindas dos celeiros, essas portas entraram nas casas para se tornarem peças de decoração de interiores, podendo ser usadas também como portas de entradas de residência.</p>
							<p>A <strong>porta de madeira celeiro</strong> pode ser instalada em quartos, banheiros, parte externa, além de aperfeiçoar o espaço. A <strong>porta de madeira celeiro</strong> proporciona uma estética peculiar para a composição do ambiente como um todo.</p>
							<p>Situada na cidade de São Paulo, uma das zonas com maior rendimento per capita e desenvolvimento econômico do Brasil, a Interporta opera em todo o território brasileiro.</p>
							<p>Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.</p>
							<p>A <strong>porta de madeira celeiro</strong> deve ser tratada com camadas de verniz para afastar fungos e cupins. Você pode escolher a porta celeiro que mais lhe agrada, sendo de madeira escura ou clara, combinando com o ambiente que ela será instalada.</p>
							<p>A <strong>porta de madeira celeiro</strong> também é uma porta de correr, sendo presa por duas entradas na parte superior ou uma encima e outra embaixo, para que ela possa correr e ter a fixação necessária.</p>
							<p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente, portanto qualquer semelhança não será mera coincidência, mas sim cópia falsificada.</p>
							<p>Não perca mais tempo, entre agora em contato com a nossa empresa por telefone ou e-mail e faça um orçamento com a gente.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>