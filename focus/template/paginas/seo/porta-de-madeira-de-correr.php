<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-madeira-de-correr.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-madeira-de-correr.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por porta de madeira de correr para sua residência ou comércio?</h2>
							<p>A Interporta é a melhor empresa quando falamos em <strong>porta de madeira de correr</strong>!</p>
							<p>A <strong>porta de madeira de correr</strong>, como o próprio nome já diz, é uma porta feita com a função de deslizar para ser aberta, além de ser bonita e funcional.</p>
							<p>Enquanto uma porta tradicional pode ocupar cerca de um metro quadrado, a <strong>porta de madeira de correr</strong> pode ser adaptada a espaços menores.</p>
							<p>Com garantia de 15 anos do sistema deslizante, a Interporta é uma loja especializada em <strong>porta de madeira de correr</strong>. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. O sistema de porta embutida permite que você ganhe espaço em ambos os lados da porta, podendo aproveitar o espaço da melhor maneira possível, sem se preocupar em abrir a porta para dentro ou fora, sem se preocupar se a porta vai bater em algum móvel.</p>
							<p>A <strong>porta de madeira de correr</strong> externa fica para fora da parede, precisando de um espaço para pode correr rente a parede quando ela for aberta, sem móveis ou objetos em pelo menos um lado da parede.</p>
							<p>A <strong>porta de madeira de correr</strong> é uma ótima opção para destacar a porta e deixar o ambiente mais bonito, já que a <strong>porta de madeira de correr</strong> tem um visual robusto valorizando o ambiente e tornando-se parte da decoração do ambiente.</p>
							<h3>A Interporta é a única loja especializada em portas de correr embutidas na parede do Brasil!</h3>
							<p>Como a instalação desses modelos de portas de correr e embutidas são complexas, é fundamental a escolha de bons profissionais para realizar um bom serviço.</p>
							<p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente, portanto qualquer semelhança não será mera coincidência, mas sim cópia falsificada.</p>
							<p>Aperfeiçoe seu espaço, valorizando o ambiente com exclusividade e personalidade.</p>
							<p>Não perca mais tempo, entre agora em contato com a nossa empresa por telefone ou e-mail e faça um orçamento de <strong>porta de madeira de correr</strong>.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>