<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-vidro.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-vidro.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por porta de vidro para sua residência ou comércio?</h2>
							<p>A Interporta é a melhor empresa quando falamos em <strong>porta de vidro</strong>!</p>
							<p>Produzimos sistemas únicos e exclusivos, totalmente feitos sob medidas, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais.</p>
							<p>A <strong>porta de vidro</strong> nasceu a partir boxes dos banheiros, lavanderias, jardim, entre outros ambientes que poderiam ser molhados ou enfrentar adversidades do tempo. Hoje, com o passar dos anos e com o avanço da tecnologia, existe <strong>porta de vidro</strong> para todos os tipos de ambientes com diversos modelos e soluções.</p>
							<p>Você também pode utilizar a <strong>porta de vidro</strong> como porta de correr, ocupando mais espaço e dando um charme a mais para o ambiente.</p>
							<p>Com mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com todos nossos serviços, nossos sistemas são extremamente confiáveis. Além disso, temos garantia de 15 anos do sistema deslizante.</p>
							<p>Situada na cidade de São Paulo, uma das zonas com maior rendimento per capita e desenvolvimento econômico do Brasil, a Interporta opera em todo o território brasileiro.</p>
							<p>Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.</p>
							<p>A <strong>porta de vidro</strong> é uma ótima opção para ambientes integrados, já que elas garantem a continuidade visual do espaço, mas sem deixar de separar o espaço quando for necessário.</p>
							<p>Já nas cozinhas, a <strong>porta de vidro</strong> é mais utilizada. Ela ajuda na higienização do ambiente, na luminosidade e na integração desse espaço com outros cômodos da casa.</p>
							<p>A <strong>porta de vidro</strong> pode ser utilizada em cômodos mais reservados também, como o quarto. Porem, nesses casos, é recomendado adotar modelos de portas com tipos de vidro escuro ou fumê.</p>
							<p>Valorizando o ambiente com exclusividade e personalidade, melhore seu espaço.</p>
							<p>Entre em contato com a gente por telefone ou e-mail e faça um orçamento.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>