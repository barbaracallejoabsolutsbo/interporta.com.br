<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-madeira-telescopica.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-madeira-telescopica.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por porta de madeira telescópica?</h2>
							<p>A Interporta é a melhor opção quando falamos em <strong>porta de madeira telescópica</strong>!</p>
							<p>A <strong>porta de madeira telescópica</strong> é utilizada em grandes vãos, com dois, três ou até quatro folhas móveis, permitindo uma abertura ampla e proporcionando a integração de ambientes.</p>
							<p>A <strong>porta de madeira telescópica</strong> é ideal para lugares com limitação de espaço nas entradas, para separar corredores ou parar ampliar a passagem em algumas situações. Além disso, a <strong>porta de madeira telescópica</strong> isola a acústica e circulação entre dois ambientes, ou então, podem interagir dois ambientes como, por exemplo, a sala de jantar com a copa.</p>
							<p>Para melhorar o manuseio, a <strong>porta de madeira telescópica</strong> pode ter abertura lateral ou central, dependendo do lugar que ela for instalada. A escolha do manuseio deve ser feita previamente, já que a <strong>porta de madeira telescópica</strong> é personalizada em tamanhos, acabamentos, cores entre outros.</p>
							<p>Situada na cidade de São Paulo, uma das zonas com maior rendimento per capita e desenvolvimento econômico do Brasil, a Interporta opera em todo o território brasileiro.</p>
							<p>Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.</p>
							<h3>Porta de madeira telescópica é com a gente!</h3>
							<p>Otimize seu espaço, valorizando o ambiente com exclusividade e personalidade.</p>
							<p>Produzimos sistemas únicos e exclusivos, totalmente feitos sob medidas, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais para <strong>porta de madeira telescópica</strong>, com alturas e larguras especificas, e para portas com medidas e formatos especiais, inclusive com Madeira de Demolição.</p>
							<p>Não perca mais tempo, entre agora em contato com a nossa empresa por telefone ou e-mail e faça um orçamento.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>