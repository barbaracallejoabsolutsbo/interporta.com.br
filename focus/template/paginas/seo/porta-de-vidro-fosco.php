<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="conteudo-full-text">
				<div class="container">
					<div class="conteudo-palavras">
						<div class="texto">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/palavras-chave/porta-de-vidro-fosco.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/palavras-chave/thumb/porta-de-vidro-fosco.jpg" alt="" class="img-right">
								</a>
							</div>
							<h2>Você esta procurando por porta de vidro fosco para sua residência ou comércio?</h2>
							<p>A Interporta é a melhor empresa quando falamos em <strong>porta de vidro fosco</strong>!</p>
							<p>Produzimos sistemas únicos e exclusivos, totalmente feitos sob medidas, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais.</p>
							<p>A porta de vidro nasceu a partir boxes dos banheiros, lavanderias, jardim, entre outros ambientes que poderiam ser molhados ou enfrentar adversidades do tempo. Hoje, com o passar dos anos e com o avanço da tecnologia, existe <strong>porta de vidro fosco</strong> para todos os tipos de ambientes com diversos modelos e soluções.</p>
							<p>A <strong>porta de vidro fosco</strong>, ou jateados, vem com materiais muito populares em projetos de arquitetura por permitirem aliar um grau de privacidade à passagem de luz. Se no passado esse material era restrito a alguns cômodos da casa, hoje ele pode ser encontrado em diversos outros lugares.</p>
							<p>Você também pode utilizar a <strong>porta de vidro fosco</strong> como porta de correr, ocupando mais espaço e dando um charme a mais para o ambiente.</p>
							<p>Com mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com todos nossos serviços, nossos sistemas são extremamente confiáveis. </p>
							<p>Em espaços corporativos, os vidros jateados vêm sendo utilizados em divisórias entre ambientes, oferecendo privacidade entre as salas mas com leveza e sofisticação.</p>
							<p>Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.</p>
							<p>A <strong>porta de vidro fosco</strong> é uma ótima opção para ambientes integrados, já que elas garantem a continuidade visual do espaço, mas sem deixar de separar o espaço quando for necessário.</p>
							<p>Já nas cozinhas, a <strong>porta de vidro fosco</strong> também é utilizada. Ela ajuda na higienização do ambiente, na luminosidade e na integração desse espaço com outros cômodos da casa.</p>
							<p>A <strong>porta de vidro fosco</strong> pode ser utilizada em cômodos mais reservados também, como o quarto, já que o vidro fosco traz privacidade para o cômodo.</p>
							<p>Valorizando o ambiente com exclusividade e personalidade, melhore seu espaço.</p>
							<p>Entre em contato com a gente por telefone ou e-mail e faça um orçamento.</p>

							<?php require PARTE.'abas.php'; ?>

							<?php require PARTE.'mais-visitados.php'; ?>

							<?php require PARTE.'texto-direitos-autorais.php'; ?>

						</div>

						<?php require PARTE.'sidebar.php'; ?>

					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>