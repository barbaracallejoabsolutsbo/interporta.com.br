<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira as imagens de nossas Portas Bandô.</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-58.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-58.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-59.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-59.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-60.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-60.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-61.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-61.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-62.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-62.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-63.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-63.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-64.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-64.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-65.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-65.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-66.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-66.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-67.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-67.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-68.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-68.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-69.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-69.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-70.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-70.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-71.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-71.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-72.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-72.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>