<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira abaixo as fotos de nossos painéis</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/1.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/1.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/2.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/2.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/3.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/3.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/4.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/4.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/5.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/5.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/6.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/6.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/7.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/7.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/8.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/8.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/9.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/9.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/10.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/10.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/11.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/11.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/12.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/12.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/13.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/13.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/14.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/14.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/15.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/15.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/16.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/16.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/paineis/17.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/paineis/thumb/17.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>