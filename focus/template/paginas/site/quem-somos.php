<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-empresa">
				<div class="container">
					<p>A <strong>Interporta</strong> é uma loja especializada em portas de correr embutidas na parede. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. Com garantia de 15 anos do sistema deslizante.</p>
					<p>A <strong>INTERPORTA</strong> é fabricante, que oferece uma gama alargada de sistemas de portas de correr embutidas, trilhos em alumínio e acessórios, batentes de madeira para portas interiores..</p>
					<p>Situada na cidade de São Paulo - uma das zonas com maior rendimento per capita e desenvolvimento econômico e industrial é servida por uma das principais vias de comunicação do Brasil, entre a cidade de São Paulo . A <strong>INTERPORTA</strong>, opera em todo o território brasileiro.</p>
					<h3>A <strong>INTERPORTA</strong> é a única loja especializada em Portas de Correr Embutidas na Parede do Brasil.</h3>
					<p>Produzimos Sistemas únicos e exclusivos, totalmente feitos sob medida, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais, com alturas e larguras específicas, e para portas com medidas e formatos especiais, inclusive Madeira de Demolição.</p>
					<p>Nosso parque fabril no estado de Minas Gerais possui área de 3.000m² com equipe técnica e engenharia capaz de desenvolver seu projeto com a máxima segurança e rapidez. Também possuímos fábrica em São Paulo, assim podemos atender com grande agilidade e eficiência a qualquer tipo de projeto e em todos os estados do Brasil.</p>
					<p>Com mais de 10 anos de experiência e mais de 8mil clientes satisfeitos, nossos sistemas são extremamente confiáveis, por isso agora oferecemos aos nossos clientes 15 anos de Garantia, oque completa a qualidade dos produtos que produzimos.</p>
					<p>Somente a <strong>INTERPORTA</strong> possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente, portanto qualquer semelhança não será mera coincidência, mas sim cópia falsificada.</p>
					<table>
						<tbody><tr>
							<td>PIRATARIA É CRIME</td>
						</tr>
					</tbody></table>
					<br>
					<div class="centerr text-center">
						<img style="max-width: 300px" src="<?php echo $config['urls']['imagens']; ?>inpi.jpg" alt="INPI" />
						<br>
						<p>Otimize seu espaço, valorizando o ambiente com exclusividade e personalidade.</p>
						<p>Assista aos vídeos e conheça um pouco mais sobre a <strong>INTERPORTA</strong>!</p>
						<br>
						<iframe width="803" height="315" src="https://www.youtube.com/embed/kxAYrpUJc58" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						<br>
						<iframe width="803" height="315" src="https://www.youtube.com/embed/LdVKUUWheNI" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						<br>
						<iframe width="803" height="315" src="https://www.youtube.com/embed/lUseVsnohVY" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>