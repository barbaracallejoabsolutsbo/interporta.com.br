<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira nossos produtos: Puxadores</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/15.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/15.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/16.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/16.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/17.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/17.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/18.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/18.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/19.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/19.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>