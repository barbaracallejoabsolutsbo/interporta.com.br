<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira as imagens de nossas Portas de Correr Embutidas.</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-1.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-1.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-2.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-2.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-3.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-3.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-4.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-4.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-5.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-5.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-6.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-6.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-7.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-7.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-8.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-8.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-9.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-9.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-10.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-10.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-11.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-11.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-12.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-12.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-13.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-13.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>