<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="faixa-pg">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 text-left">
						<h2><?php echo TITULO; ?></h2>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 text-right">
						<?php require PARTE.'breadcrumb.php'; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="base">
			<div class="base-content">
				<div class="conteudo">
					<div class="texto">
						<div class="informacoes-container">
							<?php
							foreach ($seo['paginasSeo'] as $key => $value) {
								// $imagem = $config['urls']['imagens'].'palavras-chave/thumb/'.$key.'.jpg';
								$imagem = $config['urls']['imagens'].'sem-imagem.jpg';
								echo '
								<div class="informacoes-box">
								<a href="'.URL.ltrim($key, '/').'" class="informacoes-link">
								<div>
								<img src="'.$imagem.'" alt="'.$value['title'].'" />
								</div>
								<h4>'.$value['title'].'</h4>
								</a>
								</div>
								';
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>