<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="base">
			<div class="base-content">

				<div class="conteudo">

					<div class="texto">
						<h1><?php echo TITULO; ?></h1>

						<?php require PARTE.'compartilhar.php'; ?>

						<form>

							<div class="formulario-box">
								<label>Nome</label>
								<div>
									<input type="text" name="Nome" />
								</div>
							</div>

							<div class="formulario-box">
								<label>Telefone</label>
								<div>
									<input type="text" name="Telefone" />
								</div>
							</div>

							<div class="formulario-box">
								<label>Email</label>
								<div>
									<input type="text" name="Email" />
								</div>
							</div>

							<div class="formulario-box">
								<label>Produto</label>
								<div>
									<select name="Produto">
										<option value="Produto 1">Nome do Produto 1</option>
										<option value="Produto 2">Nome do Produto 2</option>
										<option value="Produto 3">Nome do Produto 3</option>
										<option value="Produto 4">Nome do Produto 4</option>
									</select>
								</div>
							</div>

							<div class="formulario-box">
								<label>
									<input type="radio" checked name="Tipo" value="Carro" />
									<span>Carro</span>
								</label>
								<label>
									<input type="radio" name="Tipo" value="Moto" />
									<span>Moto</span>
								</label>
							</div>

							<div class="formulario-box">
								<label>
									<input type="checkbox" checked name="Ar condicionado" value="Selecionado" />
									<span>Ar condicionado</span>
								</label>
								<label>
									<input type="checkbox" name="Direção Hidráulica" value="Selecionado" />
									<span>Direção Hidráulica</span>
								</label>
							</div>

							<div class="formulario-box">
								<label>Fotos / Arquivos</label>
								<div>
									<input type="file" name="Arquivo" />
								</div>
							</div>

							<div class="formulario-box">
								<label>Mensagem</label>
								<div>
									<textarea name="Mensagem"></textarea>
								</div>
							</div>

							<div class="formulario-box align-center">
								<button type="submit" class="btn" data-ajax="solicitarOrcamento">Solicitar orçamento</button>
							</div>

						</form>

					</div>

				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>