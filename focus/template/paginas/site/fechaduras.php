<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira nossos produtos: Fechaduras</p>
					<br>
					<div class="row">
						<div class="col-md-4">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/9.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/9.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/10.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/10.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-4">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/11.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/11.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/12.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/12.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/13.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/13.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/14.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/14.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>