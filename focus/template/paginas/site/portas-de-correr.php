<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira as imagens de nossas Portas de Correr.</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-14.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-14.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-15.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-15.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-16.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-16.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-17.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-17.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-18.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-18.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-19.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-19.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-20.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-20.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-21.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-21.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-22.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-22.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-23.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-23.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-24.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-24.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-25.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-25.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>