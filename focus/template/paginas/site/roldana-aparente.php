<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira as imagens de nossas Roldana Aparente.</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-73.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-73.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-83.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-83.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-75.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-75.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-76.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-76.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-77.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-77.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-78.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-78.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-79.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-79.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-80.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-80.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-81.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-81.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-82.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-82.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-74.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-74.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>