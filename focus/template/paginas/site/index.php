<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-full">
				<div class="container">
					<h2>Interporta</h2>
					<p>Especialização em portas de correr embutidas na parede!</p>
					<div class="orcamento">
						<a href="<?php echo URL; ?>portas-embutidas">Produtos</a>
						<a href="<?php echo URL; ?>contato">Orçamento</a>
					</div>
				</div>
			</div>
			<div class="empresa">
				<div class="container">
					<h2>Quem Somos</h2>
					<div class="row">
						<div class="col-md-8">
							<p>A Interporta é uma loja especializada em portas de correr embutidas na parede. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. Com garantia de 15 anos do sistema deslizante.</p>
							<p>A INTERPORTA é fabricante, que oferece uma gama alargada de sistemas de portas de correr embutidas, trilhos em alumínio e acessórios, batentes de madeira para portas interiores..</p>
							<p>Situada na cidade de São Paulo - uma das zonas com maior rendimento per capita e desenvolvimento econômico e industrial é servida por uma das principais vias de comunicação do Brasil, entre a cidade de São Paulo . A INTERPORTA, opera em todo o território brasileiro.</p>
							<p>A INTERPORTA é a única loja especializada em Portas de Correr Embutidas na Parede do Brasil. Produzimos Sistemas únicos e exclusivos, totalmente feitos sob medida, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais, com alturas e larguras específicas, e para portas com medidas e formatos especiais, inclusive Madeira de Demolição.</p>

						</div>
						<div class="col-md-4">
							<iframe src="https://www.youtube.com/embed/kxAYrpUJc58" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
			<div class="produtos">
				<div class="container">
					<h2>Nossos Produtos</h2>
					<div class="row">
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/produtos-destaque/1.jpg" alt="Produtos em Destaque" />
							<h3>Portas de Correr Embutidas</h3>
						</div>
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/produtos-destaque/3.jpg" alt="Produtos em Destaque" />
							<h3>Portas Pivotantes</h3>
						</div>
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/produtos-destaque/2.jpg" alt="Produtos em Destaque" />
							<h3>Portas de Correr</h3>
						</div>
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/produtos-destaque/4.jpg" alt="Produtos em Destaque" />
							<h3>Painéis Decorativos</h3>
						</div>
					</div>
					<div class="orcamento">
						<a href="<?php echo URL; ?>informacoes" title="Produtos">Saiba Mais</a>
					</div>
				</div>
			</div>
			<div class="reformas">
				<div class="container">
					<h2>Nossos Projetos</h2>
					<div class="row">
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/projetos/1.jpg" alt="Produtos em Destaque" />
						</div>
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/projetos/3.jpg" alt="Produtos em Destaque" />
						</div>
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/projetos/2.jpg" alt="Produtos em Destaque" />
						</div>
						<div class="col-md-3">
							<img src="<?php echo $config['urls']['imagens']; ?>/projetos/4.jpg" alt="Produtos em Destaque" />
						</div>
					</div>
					<div class="orcamento">
						<a href="<?php echo URL; ?>informacoes" title="Nossos Projetos">Saiba Mais</a>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>
	
</body>
</html>