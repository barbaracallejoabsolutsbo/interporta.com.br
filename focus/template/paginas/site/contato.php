<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="faixa-pg">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 text-left">
						<h2><?php echo TITULO; ?></h2>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 text-right">
						<?php require PARTE.'breadcrumb.php'; ?>
					</div>
				</div>
			</div>
		</div>		
		<div class="main-content">
			<div class="contato-full">
				<div class="container">
					<div class="contato text-center">
						<div class="contato-formulario">
							<p>Preencha o formulário abaixo para enviar uma mensagem para nós.</p>
							<?php require PARTE.'formulario.php'; ?>
						</div>
					</div>
				</div>
				<div class="contato-pessoal">
					<p><strong>Endereço:</strong>R. Visc. de Itaboraí, 75 - Tatuapé, São Paulo - SP CEP: 03308-050</p>
					<p><strong>Email:</strong> vendas@interporta.com.br</p>
					<p><strong>Tel</strong>:  (11) 3427.8000 /  3926-2900</p>
					<p><strong>WhatsApp:</strong><a href="https://wa.me/5511977010329?text=Ola%20tudo%20bem?">
						(11) 97701-0329
					</a>
				</p>
			</div>
		</div>
		<div class="iframe">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7315.575067807578!2d-46.573089!3d-23.540143!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5ee9ec42b8eb%3A0x3483a04e69c8bc09!2sR.%20Visc.%20de%20Itabora%C3%AD%2C%2075%20-%20Vila%20Azevedo%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2003308-050!5e0!3m2!1spt-BR!2sbr!4v1606153225945!5m2!1spt-BR!2sbr"></iframe>	
		</div>
	</div>
</main>

<?php require PARTE.'footer.php'; ?>

</body>
</html>