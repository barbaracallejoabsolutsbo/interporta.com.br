<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira as imagens de nossas Portas de Vidro.</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-26.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-26.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-27.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-27.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-28.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-28.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-29.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-29.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-30.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-30.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-31.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-31.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-32.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-32.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-33.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-33.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/portas/porta-34.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/portas/thumb/porta-34.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>