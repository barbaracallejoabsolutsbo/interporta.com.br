<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="faixa-pg">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 text-left">
						<h2><?php echo TITULO; ?></h2>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 text-right">
						<?php require PARTE.'breadcrumb.php'; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="mapa-full">
			<div class="container">
				<div class="conteudo">
					<div class="texto">
						<ul>
							<li><a href="<?php echo URL; ?>">Home</a></li>
							<li><a href="<?php echo URL; ?>quem-somos">Quem Somos</a></li>
							<li><a href="<?php echo URL; ?>produtos">Produtos</a></li>
							<li><a href="<?php echo URL; ?>informacoes">Informações</a></li>
						</ul>
						<ul class="mapa-do-site">
							<?php
							foreach ($seo['paginasSeo'] as $key => $value) {
								echo '<li><a href="'.URL.ltrim($key, '/').'">'.$value['title'].'</a></li>';
							}
							?>
						</ul>
						<h3>Outras páginas</h3>
						<ul class="mapa-do-site">
							<?php
							foreach ($seo['paginasSite'] as $key => $value) {
								echo '<li><a href="'.URL.ltrim($key, '/').'">'.$value['title'].'</a></li>';
							}
							?>
						</ul>
						<div class="adjust-li">
							<li><a href="<?php echo URL; ?>#contato">Contato</a></li>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>