<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="faixa-pg">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 text-left">
							<h2><?php echo TITULO; ?></h2>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-portas">
				<div class="container">
					<p>Confira nossos produtos: Vedação</p>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/1.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/1.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/2.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/2.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/3.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/3.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/4.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/4.jpg" alt="" class="img-right">
								</a>
							</div>					
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/5.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/5.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/6.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/6.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/7.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/7.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img-pc">
								<a href="<?php echo URL; ?>template/imagens/acessorios/8.jpg" data-lightbox="">
									<img src="<?php echo URL; ?>template/imagens/acessorios/thumb/8.jpg" alt="" class="img-right">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>