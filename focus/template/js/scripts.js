/* Abas */
$('[data-abas]').children().click(function() {
	$(this).addClass('ativo').siblings().removeClass('ativo').parents('[data-abas-container]').find('[data-abas-conteudo]').children().eq($(this).index()).fadeIn().siblings().hide();
});

/* Accordion */
$('[data-accordion-titulo]').click(function() {
	$(this).next().slideToggle();
});

/* Banner Home */
$('[data-banner-principal]').slick({
	arrows: true,
	dots: true,
	infinite: true,
	speed: 500,
	autoplay: true,
	autoplaySpeed: 3000,
	slidesToShow: 1,
	slidesToScroll: 1,
	fade: false,
});

/* Slider */
$('[data-slider]').slick({
	arrows: true,
	dots: false,
	infinite: true,
	speed: 500,
	autoplay: true,
	autoplaySpeed: 30000,
	slidesToShow: 4,
	slidesToScroll: 1,
	fade: false,
	responsive: [
	{
		breakpoint: 640,
		settings: {
			slidesToShow: 3,
		}
	},
	{
		breakpoint: 520,
		settings: {
			slidesToShow: 2,
		}
	},
	{
		breakpoint: 420,
		settings: {
			slidesToShow: 1,
		}
	}
	]
});

/* Imagem Lightbox */
$('[data-lightbox]').magnificPopup({
	type: 'image'
});

/* Página ativa */
$.each(['.menu', '.sidebar'], function(key, val) {
	$(val+' [href="'+window.location+'"]').addClass('ativo');
});

/* Menu Responsivo */
$('[data-menu-principal]').clone().appendTo('[data-menu-lateral-conteudo]').find('[data-btn-menu-responsivo]').remove();

$('body').on('click', '[data-menu]', function(){
	$('[data-menu-lateral]').fadeToggle().toggleClass('menu-lateral-visivel');
	$('body').toggleClass('overflow');
});

/* Formulario */
$('body').on('click', '[data-ajax]', function(){
	var _this = $(this);
	var form = _this.parents('form');
	var dados = new FormData(form[0]);
	var id = Math.random().toString(36).substring(2, 10);
	dados.append('ajax', _this.attr('data-ajax'));
	if(_this.find('[data-enviando]').length == 0) {
		_this.prepend('<span data-enviando="'+id+'" class="enviando"></span>');
	} else {
		return false;
	}
	$.ajax({
		type: 'POST',
		data: dados,
		url: URL+'ajax.php',
		processData: false,
		contentType: false,
	}).done(function(xhrRet){
		try {
			var xhr = JSON.parse(xhrRet);
			var tipoMsg = 'erro';
			if(xhr.success) {
				tipoMsg = 'sucesso';
			}
			if(xhr.inputs) {
				$.each(xhr.inputs, function(index, val){
					form.find($('[name="'+val+'"]').addClass('campo-invalido'));
				});
			}
			$('body').append('<div data-msg="'+id+'" class="msg-box"><div class="msg msg-'+tipoMsg+'">'+xhr.msg+'</div></div>');
			setTimeout(function(){
				$('[data-msg="'+id+'"]').remove();
			}, 5000);
			if(xhr.success) {
				form[0].reset();
				form.find('.campo-invalido').removeClass('campo-invalido');
			}
		} catch(e) {
		}
	}).always(function(xhrRet){
		_this.find('[data-enviando]').remove();
	});
	return false;
});

$('body').on('click focus active', '.campo-invalido', function(){
	$(this).removeClass('campo-invalido');
});

$('body').on('click', '[data-msg]', function(){
	$(this).remove();
});