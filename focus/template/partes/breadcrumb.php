<div class="breadcrumb">
	<?php
	$paginasBreadcrumb = explode('/', PAGINA);
	$breadcrumb = '';

	$paginaBreadcrumb = URL;

	foreach ($paginasBreadcrumb as $key => $value) {
		if(isset($seo['paginasSite']['/'.$value])) {
			$tituloBreadcrumb = $seo['paginasSite']['/'.$value]['keywords'];
		} else if(isset($seo['paginasSeo']['/'.$value])) {
			$tituloBreadcrumb = $seo['paginasSeo']['/'.$value]['title'];
		} else {
			$tituloBreadcrumb = $value;
		}
		$paginaBreadcrumb .= $value.($key != 0 ? '/' : '');
		$breadcrumb .= '<a href="'.rtrim($paginaBreadcrumb, '/').'">'.$tituloBreadcrumb.'</a>'.(($key + 1) < count($paginasBreadcrumb) ? '<span class="breadcrumb-separador"></span>' : '');
	}

	echo $breadcrumb;
	?>
</div>