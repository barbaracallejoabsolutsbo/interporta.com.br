<div class="formulario-container">
	<form>
		<div class="formulario-box">
			<div>
				<input type="text" placeholder="Nome" name="Nome" />
			</div>
		</div>
		<div class="formulario-box">
			<div>
				<input type="email" placeholder="E=mail" name="Email" />
			</div>
		</div>

		<div class="formulario-box">
			<div>
				<input type="text" placeholder="Telefone" name="Telefone" />
			</div>
		</div>
		<div class="formulario-box">
			<div>
				<input type="text" placeholder="Assunto" name="Assunto" />
			</div>
		</div>
		<div class="formulario-box">
			<div>
				<textarea name="Mensagem" placeholder="Mensagem"></textarea>
			</div>
		</div>
		<div class="formulario-box align-center">
			<button type="submit" class="btn btn-default" data-ajax="entrarEmContato">Enviar Mensagem</button>
		</div>
	</form>
</div>