<div class="abas-container" data-abas-container>
	<div class="regioes">
		<h3>Regiões</h3>
		<div class="abas" data-abas>
			<div class="aba ativo">
				<span class="aba-texto">Selecione</span>
			</div>				
			<div class="aba ativo">
				<span class="aba-texto">RJ</span>
			</div>
			<div class="aba ativo">
				<span class="aba-texto">MG</span>
			</div>
			<div class="aba ativo">
				<span class="aba-texto">ES</span>
			</div>	
			<div class="aba ativo">
				<span class="aba-texto">SP</span>
			</div>		
			<div class="aba ativo">
				<span class="aba-texto">PR</span>
			</div>	
			<div class="aba ativo">
				<span class="aba-texto">SC</span>
			</div>
			<div class="aba ativo">
				<span class="aba-texto">RS</span>
			</div>
			<div class="aba ativo">
				<span class="aba-texto">PE</span>
			</div>	
			<div class="aba ativo">
				<span class="aba-texto">BA</span>
			</div>	
			<div class="aba ativo">
				<span class="aba-texto">CE</span>
			</div>		
			<div class="aba ativo">
				<span class="aba-texto">MA</span>
			</div>		
			<div class="aba ativo">
				<span class="aba-texto">PI</span>
			</div>
			<div class="aba ativo">
				<span class="aba-texto">GO</span>
			</div>
			<div class="aba ativo">
				<span class="aba-texto">MS</span>
			</div>	
			<div class="aba ativo">
				<span class="aba-texto">MT</span>
			</div>	
			<div class="aba ativo">
				<span class="aba-texto">AM</span>
			</div>	
			<div class="aba ativo">
				<span class="aba-texto">PA</span>
			</div>																	
		</div>

		<div class="abas-conteudo" data-abas-conteudo>
			<div> 
				<ul>
					<li><strong>Selecione uma região</strong></li>			
				</ul>			
			</div>				
			<div class="row"> 
				<div class="col-md-3">
					<ul>
						<li><strong>Rio de Janeiro&nbsp;</strong></li>
						<li><strong>São Gonçalo</strong></li>
						<li><strong>Duque de Caxias</strong></li>
						<li><strong>Nova Iguaçu</strong></li>
						<li><strong>Niterói</strong></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul>
						<li><strong>São João de Meriti</strong></li>
						<li><strong>Campos dos Goytacazes</strong></li>
						<li><strong>Petrópolis</strong></li>
						<li><strong>Volta Redonda</strong></li>
						<li><strong>Magé</strong></li>

					</ul>
				</div>
				<div class="col-md-3">
					<ul>				
						<li><strong>Mesquita</strong></li>
						<li><strong>Nova Friburgo</strong></li>
						<li><strong>Barra Mansa</strong></li>
						<li><strong>Macaé</strong></li>
						<li><strong>Cabo Frio</strong></li>

					</ul>
				</div>
				<div class="col-md-3">
					<ul>			
						<li><strong>Itaboraí</strong></li>
						<li><strong>Resende</strong></li>	
						<li><strong>Nilópolis</strong></li>
						<li><strong>Teresópolis</strong></li>		
						<li><strong>Belford Roxo</strong></li>		
					</ul>												
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Belo Horizonte</strong></li>
						<li><strong>Uberlândia</strong></li>
						<li><strong>Contagem</strong></li>
						<li><strong>Juiz de Fora</strong></li>
						<li><strong>Betim</strong></li>
						<li><strong>Montes Claros</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Ribeirão das Neves</strong></li>
						<li><strong>Uberaba</strong></li>
						<li><strong>Governador Valadares</strong></li>
						<li><strong>Ipatinga</strong></li>
						<li><strong>Santa Luzia</strong></li>
						<li><strong>Sete Lagoas</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Divinópolis</strong></li>
						<li><strong>Ibirité</strong></li>
						<li><strong>Poços de Caldas</strong></li>
						<li><strong>Patos de Minas</strong></li>
						<li><strong>Teófilo Otoni</strong></li>
						<li><strong>Sabará</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Pouso Alegre</strong></li>
						<li><strong>Barbacena</strong></li>
						<li><strong>Varginha</strong></li>
						<li><strong>Conselheiro Lafeiete</strong></li>
						<li><strong>Araguari</strong></li>
						<li><strong>Itabira</strong></li>
						<li><strong>Passos</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Serra</strong></li>
						<li><strong>Vila Velha</strong></li>
						<li><strong>Cariacica</strong></li>
						<li><strong>Vitória</strong></li>
						<li><strong>Cachoeiro de Itapemirim</strong></li>
						<li><strong>Linhares</strong></li>
						<li><strong>Jaguaré</strong></li>
						<li><strong>Mimoso do Sul</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>São Mateus</strong></li>
						<li><strong>Colatina</strong></li>
						<li><strong>Guarapari</strong></li>
						<li><strong>Aracruz</strong></li>
						<li><strong>Viana</strong></li>
						<li><strong>Nova Venécia</strong></li>
						<li><strong>Sooretama</strong></li>
						<li><strong>Anchieta</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Barra de São Francisco</strong></li>
						<li><strong>Santa Maria de Jetibá</strong></li>
						<li><strong>Castelo</strong></li>
						<li><strong>Marataízes</strong></li>	
						<li><strong>São Gabriel da Palha</strong></li>
						<li><strong>Domingos Martins</strong></li>
						<li><strong>Pinheiros</strong></li>
						<li><strong>Pedro Canário</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Itapemirim</strong></li>
						<li><strong>Afonso Cláudio</strong></li>
						<li><strong>Alegre</strong></li>
						<li><strong>Baixo Guandu</strong></li>
						<li><strong>Conceição da Barra</strong></li>
						<li><strong>Guaçuí</strong></li>
						<li><strong>Iúna</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Bertioga</strong></li>
						<li><strong>Caraguatatuba</strong></li>
						<li><strong>Cubatão</strong></li>
						<li><strong>Guarujá</strong></li>
						<li><strong>Ilhabela</strong></li>
						<li><strong>Itanhaém</strong></li>
						<li><strong>Mongaguá</strong></li>
						<li><strong>Riviera de São Lourenço</strong></li>
						<li><strong>Santos</strong></li>
						<li><strong>São Vicente</strong></li>
						<li><strong>Praia Grande</strong></li>
						<li><strong>Ubatuba</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>				
						<li><strong>São Sebastião</strong></li>
						<li><strong>Peruíbe</strong></li>
						<li><strong>São José dos campos</strong></li>
						<li><strong>Campinas</strong></li>
						<li><strong>Jundiaí</strong></li>
						<li><strong>Sorocaba</strong></li>
						<li><strong>Indaiatuba&nbsp;</strong></li>
						<li><strong>São José do Rio Preto&nbsp;</strong></li>
						<li><strong>Itatiba&nbsp;</strong></li>
						<li><strong>Amparo&nbsp;</strong></li>
						<li><strong>Barueri&nbsp;</strong></li>
						<li><strong>Ribeirão Preto</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Marília&nbsp;</strong></li>
						<li><strong>Louveira&nbsp;</strong></li>
						<li><strong>Paulínia&nbsp;</strong></li>
						<li><strong>Bauru&nbsp;</strong></li>
						<li><strong>Valinhos&nbsp;</strong></li>
						<li><strong>Bragança Paulista&nbsp;</strong></li>
						<li><strong>Araraquara</strong></li>
						<li><strong>Americana</strong></li>
						<li><strong>Atibaia&nbsp;</strong></li>
						<li><strong>Taubaté&nbsp;</strong></li>
						<li><strong>Araras&nbsp;</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>São Carlos&nbsp;</strong></li>
						<li><strong>Itupeva&nbsp;</strong></li>
						<li><strong>Mendonça&nbsp;</strong></li>
						<li><strong>Itu&nbsp;</strong></li>
						<li><strong>Vinhedo&nbsp;</strong></li>
						<li><strong>Marapoama&nbsp;</strong></li>
						<li><strong>Votuporanga&nbsp;</strong></li>
						<li><strong>Hortolândia&nbsp;</strong></li>
						<li><strong>Araçatuba&nbsp;</strong></li>
						<li><strong>Jaboticabal&nbsp;</strong></li>
						<li><strong>Sertãozinho</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Curitiba</strong></li>
						<li><strong>Londrina</strong></li>
						<li><strong>Maringá</strong></li>
						<li><strong>Ponta Grossa</strong></li>
						<li><strong>Cascavel</strong></li>
						<li><strong>Castro</strong></li>
						<li><strong>Rolândia</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>São José dos Pinhais</strong></li>
						<li><strong>Foz do Iguaçu</strong></li>
						<li><strong>Colombo</strong></li>
						<li><strong>Guarapuava</strong></li>
						<li><strong>Paranaguá</strong></li>
						<li><strong>Araucária</strong></li>
						<li><strong>Toledo</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Apucarana</strong></li>
						<li><strong>Pinhais</strong></li>
						<li><strong>Campo Largo</strong></li>
						<li><strong>Almirante Tamandaré</strong></li>
						<li><strong>Umuarama</strong></li>
						<li><strong>Paranavaí</strong></li>
						<li><strong>Piraquara</strong></li>
						<li><strong>Cambé</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Sarandi</strong></li>
						<li><strong>Fazenda Rio Grande</strong></li>
						<li><strong>Paranavaí</strong></li>
						<li><strong>Francisco Beltrão</strong></li>
						<li><strong>Pato Branco</strong></li>
						<li><strong>Cianorte</strong></li>
						<li><strong>Telêmaco Borba</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>	
						<li><strong>Joinville</strong></li>
						<li><strong>Florianópolis</strong></li>
						<li><strong>Blumenau</strong></li>
						<li><strong>Itajaí</strong></li>
						<li><strong>São José</strong></li>
						<li><strong>Navegantes</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Chapecó</strong></li>
						<li><strong>Criciúma</strong></li>
						<li><strong>Jaraguá do sul</strong></li>
						<li><strong>Lages</strong></li>
						<li><strong>Palhoça</strong></li>
						<li><strong>Itapema</strong></li>	
						<li><strong>Camboriú</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Balneário Camboriú</strong></li>
						<li><strong>Brusque</strong></li>
						<li><strong>Tubarão</strong></li>
						<li><strong>São Bento do Sul</strong></li>
						<li><strong>Caçador</strong></li>
						<li><strong>Concórdia</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>	
						<li><strong>Rio do Sul</strong></li>
						<li><strong>Araranguá</strong></li>
						<li><strong>Gaspar</strong></li>
						<li><strong>Biguaçu</strong></li>
						<li><strong>Indaial</strong></li>
						<li><strong>Mafra</strong></li>
						<li><strong>Canoinhas</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>	
						<li><strong>Porto Alegre</strong></li>
						<li><strong>Caxias do Sul</strong></li>
						<li><strong>Pelotas</strong></li>
						<li><strong>Canoas</strong></li>
						<li><strong>Santa Maria</strong></li>
						<li><strong>Alegrete</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>	
						<li><strong>Gravataí</strong></li>
						<li><strong>Viamão</strong></li>
						<li><strong>Novo Hamburgo</strong></li>
						<li><strong>São Leopoldo</strong></li>
						<li><strong>Rio Grande</strong></li>
						<li><strong>Alvorada</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Passo Fundo</strong></li>
						<li><strong>Sapucaia do Sul</strong></li>
						<li><strong>Uruguaiana</strong></li>
						<li><strong>Santa Cruz do Sul</strong></li>
						<li><strong>Cachoeirinha</strong></li>
						<li><strong>Bagé</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Bento Gonçalves</strong></li>
						<li><strong>Erechim</strong></li>
						<li><strong>Guaíba</strong></li>
						<li><strong>Cachoeira do Sul</strong></li>
						<li><strong>Santana do Livramento</strong></li>
						<li><strong>Esteio</strong></li>
						<li><strong>Ijuí</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>	
						<li><strong>Recife</strong></li>
						<li><strong>Jaboatão dos Guararapes</strong></li>
						<li><strong>Olinda</strong></li>
						<li><strong>Caruaru</strong></li>
						<li><strong>Petrolina</strong></li>
						<li><strong>Paulista</strong></li>
						<li><strong>Surubim</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Cabo de Santo Agostinho</strong></li>
						<li><strong>Camaragibe</strong></li>
						<li><strong>Garanhuns</strong></li>
						<li><strong>Vitória de Santo Antão</strong></li>
						<li><strong>Igarassu</strong></li>
						<li><strong>São Lourenço da Mata</strong></li>
						<li><strong>Abreu e Lima</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>	
						<li><strong>Santa Cruz do Capibaribe</strong></li>
						<li><strong>Ipojuca</strong></li>
						<li><strong>Serra Talhada</strong></li>
						<li><strong>Araripina</strong></li>
						<li><strong>Gravatá</strong></li>
						<li><strong>Carpina</strong></li>
						<li><strong>Goiana</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>	
						<li><strong>Belo Jardim</strong></li>
						<li><strong>Arcoverde</strong></li>
						<li><strong>Ouricuri</strong></li>
						<li><strong>Escada</strong></li>
						<li><strong>Pesqueira</strong></li>
						<li><strong>Palmares</strong></li>
						<li><strong>Bezerros</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Salvador</strong></li>
						<li><strong>Feira de Santana</strong></li>
						<li><strong>Vitória da Conquista</strong></li>
						<li><strong>Camaçari</strong></li>
						<li><strong>Itabuna</strong></li>
						<li><strong>Juazeiro</strong></li>
						<li><strong>Lauro de Freitas</strong></li>
						<li><strong>Ilhéus</strong></li>
						<li><strong>Jequié</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Teixeira de Freitas</strong></li>
						<li><strong>Alagoinhas</strong></li>
						<li><strong>Barreiras</strong></li>
						<li><strong>Porto Seguro</strong></li>
						<li><strong>Simões Filho</strong></li>
						<li><strong>Paulo Afonso</strong></li>
						<li><strong>Santo Antônio de Jesus</strong></li>
						<li><strong>Valença</strong></li>
						<li><strong>Candeias</strong></li>
						<li><strong>Guanambi</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Jacobina</strong></li>
						<li><strong>Serrinha</strong></li>
						<li><strong>Senhor do Bonfim</strong></li>
						<li><strong>Dias d'Ávila</strong></li>
						<li><strong>Luís Eduardo Magalhães</strong></li>
						<li><strong>Itapetinga</strong></li>
						<li><strong>Irecê</strong></li>
						<li><strong>Campo Formoso</strong></li>
						<li><strong>Eunápolis</strong></li>					
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Casa Nova</strong></li>
						<li><strong>Brumado</strong></li>
						<li><strong>Bom Jesus da Lapa</strong></li>
						<li><strong>Conceição do Coité</strong></li>
						<li><strong>Itamaraju</strong></li>
						<li><strong>Itaberaba</strong></li>
						<li><strong>Cruz das Almas</strong></li>
						<li><strong>Ipirá</strong></li>
						<li><strong>Santo Amaro</strong></li>
						<li><strong>Euclides da Cunha</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Fortaleza</strong></li>
						<li><strong>caucacia</strong></li>
						<li><strong>Juazeiro do Norte</strong></li>
						<li><strong>Maracanaú</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Sobral</strong></li>
						<li><strong>Crato</strong></li>
						<li><strong>Itapipoca</strong></li>
						<li><strong>Maranguape</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Iguatu</strong></li>
						<li><strong>Quixadá</strong></li>
						<li><strong>Pacajus</strong></li>
						<li><strong>Crateús</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Aquiraz</strong></li>
						<li><strong>Pacatuba</strong></li>
						<li><strong>Canindé</strong></li>					
						<li><strong>Quixeramobim</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>São Luís</strong></li>
						<li><strong>Imperatriz</strong></li>
						<li><strong>São José de Ribamar</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Timon</strong></li>
						<li><strong>Caxias</strong></li>
						<li><strong>Codó</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Paço do Lumiar</strong></li>
						<li><strong>Açailândia</strong></li>
						<li><strong>Bacabal</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Balsas</strong></li>
						<li><strong>Barra do Corda</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Teresina</strong></li>
						<li><strong>São Raimundo Nonato</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Parnaíba</strong></li>
						<li><strong>Picos</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Uruçuí</strong></li>
						<li><strong>Floriano</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Piripiri</strong></li>
						<li><strong>Campo Maior</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Goiânia</strong></li>
						<li><strong>Aparecida de Goiânia</strong></li>
						<li><strong>Anápolis</strong></li>
						<li><strong>Rio Verde</strong></li>
						<li><strong>Luziânia</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>		
						<li><strong>Águas Lindas de Goiás</strong></li>
						<li><strong>Valparaíso de Goiás</strong></li>
						<li><strong>Trindade</strong></li>
						<li><strong>Formosa</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Novo Gama</strong></li>
						<li><strong>Itumbiara</strong></li>
						<li><strong>Senador Canedo</strong></li>
						<li><strong>Catalão</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Jataí</strong></li>
						<li><strong>Anápolis</strong></li>					
						<li><strong>Planaltina</strong></li>
						<li><strong>Caldas Novas</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Campo Grande</strong></li>
						<li><strong>Dourados</strong></li>			
						<li><strong>Corumbá</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Ponta Porã</strong></li>
						<li><strong>Maracaju</strong></li>
						<li><strong>Sidrolândia</strong></li>
						<li><strong>Naviraí</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Nova Andradina</strong></li>
						<li><strong>Aquidauana</strong></li>
						<li><strong>Paranaíba</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Três Lagoas</strong></li>					
						<li><strong>Amambai</strong></li>
						<li><strong>Rio Brilhante</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>		
						<li><strong>Cuiabá</strong></li>
						<li><strong>Várzea Grande</strong></li>
						<li><strong>Sinop</strong></li>
						<li><strong>Tangará da Serra</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Cáceres</strong></li>
						<li><strong>Sorriso</strong></li>
						<li><strong>Lucas do Rio Verde</strong></li>
						<li><strong>Primavera do Leste</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Barra do Garças</strong></li>
						<li><strong>Alta Floresta</strong></li>
						<li><strong>Pontes e Lacerda</strong></li>
						<li><strong>Nova Mutum</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Campo Verde</strong></li>
						<li><strong>Juína</strong></li>
						<li><strong>Rondonópolis</strong></li>					
						<li><strong>Colniza</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Manaus</strong></li>
						<li><strong>Parintins</strong></li>
						<li><strong>Itacoatiara</strong></li>
						<li><strong>Manacapuru</strong></li>
						<li><strong>Coari</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Centro Amazonense</strong></li>
						<li><strong>Alvaraes</strong></li>
						<li><strong>Amatura</strong></li>
						<li><strong>Anama</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Anori</strong></li>
						<li><strong>Apui</strong></li>
						<li><strong>Atalaia do Norte</strong></li>
						<li><strong>Codajas</strong></li>
						<li><strong>Eirunepe</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>			
						<li><strong>Envira</strong></li>
						<li><strong>Fonte Boa</strong></li>
						<li><strong>Guajara</strong></li>
						<li><strong>Humaita</strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">			
					<ul>
						<li><strong>Belém</strong></li>
						<li><strong>Ananindeua</strong></li>
						<li><strong>Santarém</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Marabá</strong></li>
						<li><strong>Castanhal</strong></li>
						<li><strong>Parauapebas</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Itaituba</strong></li>
						<li><strong>Cametá</strong></li>
						<li><strong>Bragança&nbsp;</strong></li>
					</ul>
				</div>
				<div class="col-md-3">			
					<ul>
						<li><strong>Abaetetuba</strong></li>
						<li><strong>Bragança</strong></li>
						<li><strong>Marituba</strong></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>