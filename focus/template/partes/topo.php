<div class="faixa-home">
	<div class="container">
		<p><img src="<?php echo $config['urls']['imagens']; ?>telefone.png" alt="Telefone" /> 11 3427.8000 | 11 3926-2900 | <img src="<?php echo $config['urls']['imagens']; ?>whatsapp.png" alt="Email" /> <a href="https://wa.me/5511977010329?text=Ola%20tudo%20bem?" target="_blank"> (11) 97701-0329</a> | <img src="<?php echo $config['urls']['imagens']; ?>arroba.png" alt="Email" /> vendas@interporta.com.br</p>
	</div>
</div>
<header class="topo">
	<div class="container">
		<div class="topo-box">
			<div class="logo">
				<a href="<?php echo URL; ?>"><img src="<?php echo $config['urls']['imagens']; ?>logo.png" alt="logo" /></a>
			</div>
			<nav class="menu" data-menu-principal>
				<div data-btn-menu-responsivo class="btn-menu-responsivo">
					<div class="btn-menu" data-menu>MENU</div>
				</div>
				<div class="menu-box">
					<ul>
						<li><a href="<?php echo URL; ?>">Home</a></li>
						<li><a href="<?php echo URL; ?>quem-somos">Quem Somos</a></li>
						<li><a href="#">Portas</a>
							<ul>
								<?php foreach ($seo['paginasPort'] as $key => $value) {
									echo '<li><a href="'.rtrim(URL, '/').$key.'">'.$value['title'].'</a></li>';
								} ?>
							</ul> 
						</li>
						<li><a href="#">Acessórios</a>
							<ul>
								<?php foreach ($seo['paginasAcess'] as $key => $value) {
									echo '<li><a href="'.rtrim(URL, '/').$key.'">'.$value['title'].'</a></li>';
								} ?>
							</ul> 
						</li>
						<li><a href="<?php echo URL; ?>paineis-decorativos">Painéis Decorativos</a></li>
						<li>
							<a href="<?php echo URL; ?>informacoes">Informações</a>
							<!-- <ul>
								<?php foreach ($seo['paginasSeo'] as $key => $value) {
									echo '<li><a href="'.rtrim(URL, '/').$key.'">'.$value['title'].'</a></li>';
								} ?>
							</ul> -->
						</li>
						<li><a href="<?php echo URL; ?>contato">Contato</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>