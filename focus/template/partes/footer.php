<footer class="footer">
	<div class="container">
		<h2><span>Interporta</span></h2>
		<div class="row">
			<div class="col-md-4">
				<h3>Sobre Nós</h3>
				<p>A Interporta é uma loja especializada em portas de correr embutidas na parede. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. Com garantia de 15 anos do sistema deslizante.</p>
			</div>
			<div class="col-md-2">
				<h3>Institucional</h3>
				<ul>
					<li><a href="<?php echo URL; ?>quem-somos" title="Empresa">Quem Somos</a></li>
					<li><a href="<?php echo URL; ?>informacoes" title="Informações">Informações</a></li>
					<li><a href="<?php echo URL; ?>mapa-site" title="Mapa do Site">Mapa do Site</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h3>Entre em Contato</h3>
				<ul>
					<li><img src="<?php echo $config['urls']['imagens']; ?>arroba.png" alt="email" /> <a href="mailto:vendas@interporta.com.br"> 	vendas@interporta.com.br</a></li>
					<li><img src="<?php echo $config['urls']['imagens']; ?>telefone.png" alt="Telefone 1" /> 11 3427.8000 / 11 3926-2900</li>
					<li><img src="<?php echo $config['urls']['imagens']; ?>telefone.png" alt="Telefone 2" /> 11 97701-0329</li>
				</ul>

			</div>
			<div class="col-md-2">
				<h3>Siga-nos</h3>
				<a href="https://www.facebook.com/interporta.portasembutidas" title="Facebook" target="_blank">
					<img src="<?php echo $config['urls']['imagens']; ?>facebook.png" alt="Facebook" /> - @iterporta
				</a>
			</div>
		</div>
		<div class="creditos">
			<div class="creditos-box">
				<div class="creditos-logo">
					<img src="<?php echo $config['urls']['imagens']; ?>logo.png" alt="logo" />
				</div>
				<div class="creditos-link">
					<a href="<?php echo $config['creditos']['link']; ?>" target="_blank"><img src="<?php echo $config['urls']['imagens']; ?>creditos/logo.png" alt="logo"></a>
					<a href="http://validator.w3.org/check?uri=<?php echo rtrim(URL, '/').PAGINA; ?>" target="_blank"><img src="<?php echo $config['urls']['imagens']; ?>creditos/logo-html5.png" alt="html5" style="width: 20px;"></a>
					<a href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo rtrim(URL, '/').PAGINA; ?>" target="_blank"><img src="<?php echo $config['urls']['imagens']; ?>creditos/logo-css3.png" alt="css3" style="width: 20px;"></a>
				</div>
			</div>
		</div>
	</div>
</footer>

<div class="menu-lateral" data-menu-lateral>
	<div class="menu-lateral-conteudo" data-menu-lateral-conteudo></div>
	<div data-menu class="menu-lateral-overlay"></div>
</div>

<div class="barra-footer">
	<a href="tel:113427-8000"><i class="fa fa-phone"></i></a>	
	<a href="https://wa.me/5511977010329?text=Ola%20tudo%20bem?"><i class="fa fa-whatsapp"></i></a>	
	<a href="mailto:vendas@interporta.com.br"><i class="fa fa-envelope"></i></a>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172848124-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-172848124-5');
</script>

<?php
echo jsInline(array(
	'jquery-3.3.1.min.js' => false,
	'slick.min.js' => false,
	'jquery.magnific-popup.min.js' => false,
	'functions.js' => true,
	'scripts.js' => true,
	'jquery.nivo.js' => true,
	'jquery.slick.js' => true,	
));
