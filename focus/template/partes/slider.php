<div data-slider class="slider">

	<?php
	foreach ($seo['paginasSeo'] as $key => $value) {
		// $imagem = $config['urls']['imagens'].'palavras-chave/thumb/'.$key.'.jpg';
		$imagem = $config['urls']['imagens'].'sem-imagem.jpg';
		echo '
		<div>
			<a href="'.URL.ltrim($key, '/').'" class="slider-box">
				<div>
					<img src="'.$imagem.'" alt="'.$value['title'].'" />
				</div>
				<h4>'.$value['title'].'</h4>
			</a>
		</div>

		';
	}
	?>

</div>