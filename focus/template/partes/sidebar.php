<aside class="sidebar">

	<div style="float: left; width: 100%; background: #fff; padding: 5px 0px;">
		<a style="text-align: center; text-decoration: none; float: left; border: none; width: 50%;" href="http://api.whatsapp.com/send?phone=5511977010329" class="whats-buttom" target="_blank">
			<i style="color: #fff; font-size: 40px; background: #4EC95A; padding: 10px 14px; border-radius: 10px;" class="fa fa-whatsapp" aria-hidden="true"></i>
		</a>
		<a style="text-align: center; text-decoration: none; float: left; border: none; width: 50%;" href="tel:551139262900" class="whats-buttom" target="_blank">

			<i style="color: #fff; font-size: 40px; background: #319b41; padding: 10px 15px; border-radius: 10px;" class="fa fa-phone" aria-hidden="true"></i>
		</a>

		<div class="col-md-12" style="background: #ffa500; color: #fff; margin-top: 20px; padding: 10px;">
			<a style="text-transform: none; text-decoration: none; color: #fff;" href="tel:551139262900">Telefone: (11) 39262900</a>
			<br><br>
			<a style="text-transform: none; text-decoration: none; color: #fff;" href="http://api.whatsapp.com/send?phone=5511977010329">Whatsapp: (11) 97701-0329</a>
			<br><br>
			<a style="text-transform: none; text-decoration: none; color: #fff;" href="mailto:vendas@interporta.com.br">vendas@interporta.com.br</a>
		</div>
	</div>

	<div class="sidebar-box">
		<h3>Informações</h3>
		<ul class="sidebar-conteudo">
		<?php
		foreach ($seo['paginasSeo'] as $key => $value) {
			echo '
				<li>
					<a href="'.URL.ltrim($key, '/').'">
						'.$value['title'].'
					</a>
				</li>
			';
		}
		?>
		</ul>
	</div>

</aside>