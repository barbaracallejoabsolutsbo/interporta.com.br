<?php

    $title       = "Portas Pivotantes";
    $description = "Bem vindo a Interporta, nosso objetivo consiste em oferecer o que tem de melhor em portas de correr embutidas na parede. Para saber mais, entre em contato conosco."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "galeria-fotos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="faixa-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <h1><?php echo $h1; ?></h1>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <div class="text-right">
                            <?php echo $padrao->breadcrumb(array($title)); ?>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo">
            <div class="container">
                <p>Confira as imagens de nossas Portas Pivotantes:</p>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-35.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-35-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-36.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-36-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-37.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-37-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-38.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-38-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-39.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-39-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-40.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-40-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-41.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-41-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-42.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-42-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-43.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-43-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-44.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-44-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-45.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-45-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-46.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-46-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-47.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-47-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-48.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-48-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-49.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-49-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-50.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-50-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-51.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-51-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="<?php echo $url; ?>imagens/portas-pivotantes/porta-52.jpg" title="<?php echo $h1; ?>" data-fancybox-group="item">
                            <img src="<?php echo $url; ?>imagens/portas-pivotantes/porta-52-thumb.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox"
    )); ?>
    
</body>
</html>