<?php
$title       = "Porta de madeira camarão em Higienópolis";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Porta de madeira camarão em Higienópolis deixa as folhas dobradas uma sobre a outra quando é aberta. Esse movimento é possível porque tem um trilho no piso ou no forro que permite esse movimento. Quando elas são fechadas, funciona normalmente como uma porta deslizante. A porta de madeira promove um ótimo isolamento térmico e acústico, deixando do lado de fora o barulho e o frio.</p>
<p>A empresa Interporta é destaque entre as principais empresas do ramo de Fabricante de Porta, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Porta de madeira camarão em Higienópolis do mercado. Ainda, possui facilidade com Porta de madeira embutida na parede, Manutenção de porta com roldana, Porta de madeira camarão, Porta de madeira convencional e Porta de madeira com passagem para pet mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>