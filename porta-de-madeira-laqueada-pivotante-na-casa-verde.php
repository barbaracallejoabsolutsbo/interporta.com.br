<?php
$title       = "Porta de madeira laqueada pivotante na Casa Verde";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A porta de madeira laqueada embutida promove um visual diferenciado nos cômodos, agregando sofisticação, luxo e qualidade ao ambiente. Esses modelos de portas embutidas trazem a vantagem de economizar espaço, já que ao ser aberta a porta, ela não usa o espaço da na sua frente, excluindo a necessidade de deixar o espaço próximo desocupado. Entre em contato com a nossa empresa e saiba mais.</p>
<p>Especialista no segmento de Fabricante de Porta, a Interporta é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Porta de madeira laqueada pivotante na Casa Verde. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Porta de madeira telescópica, Porta de madeira laqueada roldana aparente, Loja de fabrica de porta celeiro, Loja de fabrica de porta embutida e Porta de madeira bandô e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>