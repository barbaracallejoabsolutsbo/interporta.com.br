<?php
$title       = "Manutenção de porta de giro no Pacaembú";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sempre que vamos escolher uma porta, é importante se atentar ao tamanho da porta de giro, já que elas são vendidas em tamanhos maiores que as comuns e precisam de um vão de alguns centímetros para que a instalação seja feita de maneira correta. Se não for instalada da forma correta, a porta de giro logo precisará de uma Manutenção de porta de giro no Pacaembú para que funcione corretamente.</p>
<p>Especialista no mercado, a Interporta é uma empresa que ganha visibilidade quando se trata de Manutenção de porta de giro no Pacaembú, já que possui mão de obra especializada em Porta de madeira com boiserie, Porta de vidro, Manutenção de porta de giro, Porta de madeira celeiro e Porta de embutir sistema duplo linear ou 45 graus. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de Fabricante de Porta, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>