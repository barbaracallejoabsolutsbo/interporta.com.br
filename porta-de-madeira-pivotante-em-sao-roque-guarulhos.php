<?php
$title       = "Porta de madeira pivotante em São Roque - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Utilizada há mais de vinte anos no Brasil, as portas pivotantes adicionam estética e elegância a qualquer projeto. Escolher o modelo de porta para a entrada ou para qualquer outro cômodo da residência ou comércio é uma tarefa difícil. Temos mais de 10 anos de experiência e mais de oito mil clientes satisfeitos com nossos serviços de Porta de madeira pivotante em São Roque - Guarulhos e outros serviços.  </p>
<p>A empresa Interporta é destaque entre as principais empresas do ramo de Fabricante de Porta, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Porta de madeira pivotante em São Roque - Guarulhos do mercado. Ainda, possui facilidade com Porta de madeira laqueada, Porta de madeira de parede de drywall, Porta de madeira laqueada roldana aparente, Porta de madeira de roldana aparente e Porta de madeira embutida na lateral da parede mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>