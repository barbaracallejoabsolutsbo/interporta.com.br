<?php
$title       = "Porta de madeira com boiserie no Jardim São Luiz";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Somente a Interporta possui patentes de todos os produtos que produz, muitos dos quais com mais de uma patente. A Interporta trabalha com Porta de madeira com boiserie no Jardim São Luiz. O século XVII e o século XVIII marcaram o auge das boiseries na decoração. A boiserie surgiu anos atrás na França influenciada pelo movimento artístico que ficou conhecido como Rococó. 
</p>
<p>Nós da Interporta trabalhamos dia a dia para garantir o melhor em Porta de madeira com boiserie no Jardim São Luiz e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de Fabricante de Porta com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Porta de madeira laqueada embutida, Porta de madeira laqueada roldana aparente, Porta de madeira de correr, Porta de madeira com espelho e Porta de madeira celeiro laqueada e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>