<?php
$title       = "Porta de madeira com aplique de veneziana superior ou inferior no Tatuapé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Independente se você vai construir ou reformar, a Porta de madeira com aplique de veneziana superior ou inferior no Tatuapé é uma das melhores opções. Essa porta oferece design moderno sem perder a segurança que a porta deve oferecer para o ambiente. Com profissionais experientes e capacitados para fazer os melhores produtos e realizar os melhores serviços, nossa empresa é referência no segmento!</p>
<p>Sendo uma das principais empresas do segmento de Fabricante de Porta, a Interporta possui os melhores recursos do mercado com o objetivo de disponibilizar Porta de madeira laqueada embutida, Porta de madeira com passagem para pet, Porta de madeira celeiro laqueada, Porta de madeira com boiserie e Porta de embutir sistema duplo linear ou 45 graus com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Porta de madeira com aplique de veneziana superior ou inferior no Tatuapé com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>