<?php
$title       = "Porta de madeira celeiro em Pari";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo para todos os estados do Brasil Porta de madeira celeiro em Pari. A Porta de madeira celeiro em Pari não é uma novidade no mercado, mas sempre será uma boa escolha para a sua residência. A cada dia que passa surge novos modelos de portas, com materiais diferenciados e tecnologia avançada.</p>
<p>Sendo uma das empresas mais confiáveis no ramo de Fabricante de Porta, a Interporta ganha destaque por ser confiável e idônea quando falamos não só de Porta de madeira celeiro em Pari, mas também quando o assim é Loja de fabrica de porta pivotante, Porta de madeira laqueada roldana aparente, Porta de vidro, Porta de madeira com aplique de veneziana superior ou inferior e Porta de madeira de parede de alvenaria. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>