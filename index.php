<?php

    $h1      	 = "Home";
    $title    	 = "Portas de Correr na Parede";
    $description = "Portas de Correr na Parede"; // Manter entre 130 a 160 caracteres
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "tools/nivo-slider"
    ));
    
    ?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="banner-home">
            <h1>INTERPORTA</h1>
            <p>Especialização em portas de correr embutidas na parede!</p>
            <div class="btn-banner">
                <a href="<?php echo $url; ?>portas-embutidas">Produtos</a>
                <a href="<?php echo $url; ?>contato">Orçamento</a>
            </div>
        </div> 
        <div class="sobre-nos">
            <div class="container">
                <h1>Quem Somos</h1>
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <p>A Interporta é uma loja especializada em portas de correr embutidas na parede. Com sistema mecânico e estrutural diferenciado, otimizando espaço em ambos os lados, possibilitando a introdução total da porta na parede. Com garantia de 15 anos do sistema deslizante.</p>
                        <p>A INTERPORTA é fabricante, que oferece uma gama alargada de sistemas de portas de correr embutidas, trilhos em alumínio e acessórios, batentes de madeira para portas interiores.</p>
                        <p>Situada na cidade de São Paulo - uma das zonas com maior rendimento per capita e desenvolvimento econômico e industrial é servida por uma das principais vias de comunicação do Brasil, entre a cidade de São Paulo . A INTERPORTA, opera em todo o território brasileiro.</p>
                        <p>A INTERPORTA é a única loja especializada em Portas de Correr Embutidas na Parede do Brasil. Produzimos Sistemas únicos e exclusivos, totalmente feitos sob medida, e, por sermos fabricantes, temos condições de produzir sistemas com medidas especiais, com alturas e larguras específicas, e para portas com medidas e formatos especiais, inclusive Madeira de Demolição.</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <iframe src="https://www.youtube.com/embed/kxAYrpUJc58" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>            
        <div class="servicos">
            <div class="container">
                <h1>Nossos Produtos</h1>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/nossos-produtos/1.jpg" alt="Portas de Correr Embutidas" class="img-responsive">
                        <h3>Portas de Correr Embutidas</h3>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/nossos-produtos/2.jpg" alt="Portas Pivotantes" class="img-responsive">
                        <h3>Portas Pivotantes</h3>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/nossos-produtos/3.jpg" alt="Portas de Correr" class="img-responsive">
                        <h3>Portas de Correr</h3>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/nossos-produtos/4.jpg" alt="Painéis Decorativos" class="img-responsive">
                        <h3>Painéis Decorativos</h3>
                    </div>
                </div>
                <div class="orcamento">
                    <a href="<?php echo $url; ?>informacoes" title="Saiba Mais">Saiba Mais</a>
                </div>
            </div>
        </div>  
        <div class="projetos">
            <div class="container">
                <h3>Nossos Projetos</h3>
                <div class="row">
                   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 zoom">
                       <img src="<?php echo $url; ?>imagens/nossos-projetos/nossos-projetos-1.jpg" class="img-responsive" alt="Nossos Projetos" title="Nossos Projetos">
                   </div>
                   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 zoom">
                       <img src="<?php echo $url; ?>imagens/nossos-projetos/nossos-projetos-2.jpg" class="img-responsive" alt="Nossos Projetos" title="Nossos Projetos">
                   </div>
                   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 zoom">
                       <img src="<?php echo $url; ?>imagens/nossos-projetos/nossos-projetos-3.jpg" class="img-responsive" alt="Nossos Projetos" title="Nossos Projetos">
                   </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 zoom">
                       <img src="<?php echo $url; ?>imagens/nossos-projetos/nossos-projetos-4.jpg" class="img-responsive" alt="Nossos Projetos" title="Nossos Projetos">
                    </div>
                </div>
                <div class="orcamento">
                    <a href="<?php echo $url; ?>informacoes" title="Saiba Mais">Saiba Mais</a>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        "tools/contador-clientes",
        "tools/jquery.nivo"
    )); ?>

    <script>
        $(function(){
            $("#slider").nivoSlider();
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //animSpeed: 500,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });
    </script>

    <script>
        $(function(){
            $(".counter").counterUp({
                delay: 2,
                time:9000
            });
        });
    </script>
     <script>
        $(function(){
           function scrollTopMenu(a){
               var id_element = $(a).attr("data-id");
               var top = $("#"+id_element).offset().top -58;
               if(id_element === "home")
               {
                   var top = 0;
               }
               $("html, body").animate({
                   scrollTop: top
               }, 700);
           }
           $("ul li a").click(function(){
               scrollTopMenu($(this));
           });
           $("a.click-second").click(function(){
               scrollTopMenu($(this));
           });
        });
    </script>
    
</body>
</html>