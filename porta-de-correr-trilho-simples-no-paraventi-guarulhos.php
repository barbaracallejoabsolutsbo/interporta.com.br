<?php
$title       = "Porta de correr trilho simples no Paraventi - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Interporta esta situada na cidade de SP, uma das cidades com maior rendimento e desenvolvimento econômico do Brasil, operando e fornecendo serviços e produtos da nossa Porta de correr trilho simples no Paraventi - Guarulhos. As portas de correr produzidas pela nossa empresa são uma ótima solução para otimização de espaço da sua casa ou comércio, agregando também com uma ótima opção de decoração.</p>
<p>Desempenhando uma das melhores assessorias do segmento de Fabricante de Porta, a Interporta se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Porta de correr trilho simples no Paraventi - Guarulhos com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Porta de madeira de correr, Porta de correr trilho simples, Manutenção de porta embutida, Porta de madeira personalizada e Loja de fabrica de porta laqueada, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>