<?php

    $title       = "Informações";
    $description = "Em nossa página de informações você encontra diversos produtos e serviços, confira nossas categorias e informações de nossos produtos e serviços."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "informacoes"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="faixa-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <h1><?php echo $h1; ?></h1>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <div class="text-right">
                            <?php echo $padrao->breadcrumb(array($title)); ?>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo">
            <div class="container">
                <p>Confira abaixo informações relacionadas a <strong><?php echo $nome_empresa; ?></strong>:</p>
                <div class="row lista-thumbs">
                    <?php echo $padrao->listaThumbs($palavras_chave, array(
                        "class_div" => "col-xs-4 col-sm-3 col-md-2 col-lg-2",
                    )); ?>
                </div>
            </div>
        </div>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>