<?php
$title       = "Porta de madeira celeiro em Morro Grande - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Porta de madeira celeiro em Morro Grande - Guarulhos deve ser tratada com camadas de verniz para evitar que fungos e cupins danifiquem as portas. Essa porta também é uma porta de correr, sendo presa por duas entradas na parte superior ou uma encima e outra embaixo, para que ela possa correr e ter a fixação necessária. Você pode escolher a porta celeiro que mais lhe agrada, combinando com o ambiente.</p>
<p>Procurando uma empresa de confiança onde você possa garantir o melhor em Fabricante de Porta, a Interporta é a companhia certa. Aqui desde Porta de madeira celeiro em Morro Grande - Guarulhos até Porta de madeira laqueada roldana aparente, Porta de madeira de correr, Porta de embutir sistema duplo linear ou 45 graus, Loja de fabrica de porta embutida e Manutenção de porta com roldana é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções em energia sustentável. Entre em contato conosco e saiba mais!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>