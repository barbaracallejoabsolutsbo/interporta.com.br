<?php
$title       = "Porta de madeira telescópica em Francisco Morato";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Porta de madeira telescópica em Francisco Morato é uma porta ideal para lugares com limitação de espaço nas entradas, para separar corredores ou parar ampliar a passagem em algumas situações. Além disso, a Porta de madeira telescópica em Francisco Morato tem um ótimo isolamento acústico entre dois ambientes. Ela ajuda também a interagir dois ambientes, como por exemplo, a sala de jantar com a copa. </p>
<p>Empresa pioneira no mercado de Fabricante de Porta, a Interporta é qualificada e experiente quando o assunto são Loja de fabrica de porta roldana aparente, Porta de madeira com espelho, Porta de madeira com aplicação de vidro, Porta de embutir sistema duplo linear ou 45 graus e Loja de fabrica de porta embutida. Aqui você encontra soluções personalizadas para o que necessita e tem acesso ao que há de melhor e mais moderno para Porta de madeira telescópica em Francisco Morato sempre com muita eficiência e qualidade para garantir a sua satisfação e sua fidelidade conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>