<?php
$title       = "Troca de ferragens de porta no Parque São Rafael";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você esta procurando por Troca de ferragens de porta no Parque São Rafael, conte com a Interporta. Nossa empresa possui experiência no segmento a muitos anos, oferecendo diversos produtos e serviços. As ferragens das portas e das janelas são as peças fundamentais para que essas esquadrias sejam encaixadas de forma fixa, funcionando de forma correta. Entre em contato com a nossa empresa e saiba mais sobre nossos serviços.</p>
<p>Com sua credibilidade no mercado de Fabricante de Porta, proporcionando com qualidade, viabilidade e custo x benefício seja em Troca de ferragens de porta no Parque São Rafael quanto em Porta de vidro fosco, Troca de folha de porta, Troca de ferragens de porta, Porta de madeira com aplique de veneziana superior ou inferior e Porta de madeira laqueada embutida, podendo dessa forma garantir seu sucesso no segmento em que atua de forma idônea e com altíssimo nível de qualidade a Interporta é a opção número um para você garantir o melhor no que busca!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>