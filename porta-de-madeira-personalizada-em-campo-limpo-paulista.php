<?php
$title       = "Porta de madeira personalizada em Campo Limpo Paulista";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O sistema de porta embutida permite que você ganhe espaço em ambos os lados da porta, podendo aproveitar o espaço da melhor maneira possível, sem se preocupar em abrir a porta para dentro ou fora, sem se preocupar se a porta vai bater em algum móvel. A Porta de madeira personalizada em Campo Limpo Paulista pode, além de ocupar menos espaço, ser feita com materiais e idéias do seu gosto.</p>
<p>Com a Interporta proporcionando o que se tem de melhor e mais moderno no segmento de Fabricante de Porta consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Manutenção de porta embutida, Manutenção de porta camarão, Loja de fabrica de porta embutida, Porta de madeira laqueada roldana aparente e Porta de madeira laqueada pivotante nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Porta de madeira personalizada em Campo Limpo Paulista.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>